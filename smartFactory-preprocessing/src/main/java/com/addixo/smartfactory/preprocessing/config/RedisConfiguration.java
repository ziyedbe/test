
package com.addixo.smartfactory.preprocessing.config;

import com.addixo.smartfactory.preprocessing.broker.RedisMessageSubscriber;
import com.addixo.smartfactory.preprocessing.service.SwitchDataService;
import com.addixo.smartfactory.preprocessing.service.SwitchDataServiceImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

@Configuration
public class RedisConfiguration {

  @Value("${spring.redis.host}")
  private String redisHostName;

  @Value("${spring.redis.password}")
  private String redisPassword;

  @Value("${spring.redis.port}")
  private int redisPort;

  @Bean
  JedisConnectionFactory jedisConnectionFactory() throws Exception {

    final JedisConnectionFactory factory = new JedisConnectionFactory();
    factory.setHostName(this.redisHostName);
    factory.setPort(this.redisPort);
    factory.setPassword(this.redisPassword);
    factory.setUsePool(true);
    return factory;

  }

  @Bean
  MessageListenerAdapter messageListener() {

    return new MessageListenerAdapter(new RedisMessageSubscriber(this.switchDataService()));
  }

  @Bean
  RedisMessageListenerContainer redisContainer() throws Exception {

    final RedisMessageListenerContainer container = new RedisMessageListenerContainer();
    container.setConnectionFactory(this.jedisConnectionFactory());
    container.addMessageListener(this.messageListener(), this.topicConsumer());
    return container;
  }

  @Bean
  public StringRedisTemplate stringRedisTemplate() throws Exception {

    StringRedisTemplate stringRedisTemplate = new StringRedisTemplate(
        this.jedisConnectionFactory());
    stringRedisTemplate.setEnableTransactionSupport(true);
    return stringRedisTemplate;
  }

  @Bean
  ChannelTopic topicConsumer() {

    return new ChannelTopic(Environment.SHOP_FLOOR_TOPIC);
  }

  @Bean
  SwitchDataService switchDataService() {

    return new SwitchDataServiceImpl();
  }

}
