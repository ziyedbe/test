
package com.addixo.smartfactory.preprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.addixo.smartfactory.preprocessing")

public class Preprocessing {

  public static void main(final String[] args) {

    SpringApplication.run(Preprocessing.class, args);
  }
}
