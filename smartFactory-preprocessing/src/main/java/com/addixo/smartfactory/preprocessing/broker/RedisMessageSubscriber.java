
package com.addixo.smartfactory.preprocessing.broker;

import com.addixo.smartfactory.preprocessing.model.ShopFloorData;
import com.addixo.smartfactory.preprocessing.service.SwitchDataService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

@Service
public class RedisMessageSubscriber implements MessageListener {

  private static final Logger logger = LoggerFactory.getLogger(RedisMessageSubscriber.class);

  private final Gson gson;

  private final SwitchDataService switchDataService;

  public RedisMessageSubscriber(final SwitchDataService switchDataService) {

    this.switchDataService = switchDataService;
    this.gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

  }

  @Override

  public void onMessage(final Message message, final byte[] pattern) {

    try {
      logger.info("start {} ", "onMessage");
      logger.debug("message received: {}", message);
      final ShopFloorData shopFloorData = this.gson.fromJson(message.toString(), ShopFloorData.class);

      this.switchDataService.switchShopFloorData(shopFloorData);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);

    }
    logger.info("Finsh {}", "onMessage");
  }

}