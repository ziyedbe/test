
package com.addixo.smartfactory.preprocessing.controller;

import com.addixo.smartfactory.preprocessing.model.ShopFloorData;
import com.addixo.smartfactory.preprocessing.service.ShopFloorDataService;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ShopFloorDataController {

  private final Logger logger = LoggerFactory.getLogger(ShopFloorDataController.class);

  @Autowired
  private ShopFloorDataService shopFloorDataService;

  @GetMapping("/frame")
  @ResponseBody
  public List<ShopFloorData> getFrameBetweenStartDataAndFinshData(
      @Valid @RequestHeader("start") final Date start, @RequestHeader("finish") final Date finish) {

    this.logger.debug("get Frame Between StartData {} And FinshData : {}", start, finish);
    return this.shopFloorDataService.getShopFloorDataByStartdataAndFinishData(start, finish);
  }

}
