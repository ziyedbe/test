
package com.addixo.smartfactory.preprocessing.service;

import com.addixo.smartfactory.preprocessing.model.ShopFloorData;

public interface SwitchDataService {

  void switchShopFloorData(ShopFloorData shopFloorData);

}
