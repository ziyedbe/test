
package com.addixo.smartfactory.preprocessing.service;

import com.addixo.smartfactory.preprocessing.model.ShopFloorData;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ShopFloorDataService {

  Optional<ShopFloorData> saveShopFloorData(ShopFloorData shopFloorData);

  List<ShopFloorData> getShopFloorDataByStartdataAndFinishData(Date start, Date finish);
}
