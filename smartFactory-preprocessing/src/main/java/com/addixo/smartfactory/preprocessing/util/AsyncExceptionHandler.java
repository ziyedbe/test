
package com.addixo.smartfactory.preprocessing.util;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

public class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(AsyncExceptionHandler.class);

  @Override
  public void handleUncaughtException(final Throwable ex, final Method method,
      final Object... params) {

    logger.debug("Exception Cause - {}", ex.getMessage());
    logger.debug("Method name - {}", method.getName());
    for (Object param : params) {
      logger.debug("Parameter value - {} ", param);
    }

  }
}
