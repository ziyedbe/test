
package com.addixo.smartfactory.preprocessing.service;

import com.addixo.smartfactory.preprocessing.model.ShopFloorData;
import com.addixo.smartfactory.preprocessing.repository.ShopFloorDataRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
class ShopFloorDataServiceImpl implements ShopFloorDataService {

  @Autowired
  private ShopFloorDataRepository shopFloorDataRepository;

  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  public Optional<ShopFloorData> saveShopFloorData(final ShopFloorData shopFloorData) {

    return Optional.of(this.shopFloorDataRepository.save(shopFloorData));
  }

  @Override
  public List<ShopFloorData> getShopFloorDataByStartdataAndFinishData(final Date start,
      final Date finsh) {

   final Query query = new Query();
    query.addCriteria(Criteria.where("eventDate").gte(start).lte(finsh));
    return this.mongoTemplate.find(query, ShopFloorData.class);
  }

}
