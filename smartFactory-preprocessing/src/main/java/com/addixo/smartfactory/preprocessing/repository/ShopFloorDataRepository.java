
package com.addixo.smartfactory.preprocessing.repository;

import com.addixo.smartfactory.preprocessing.model.ShopFloorData;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopFloorDataRepository extends MongoRepository<ShopFloorData, String> {

}
