
package com.addixo.smartfactory.preprocessing.model;

import com.addixo.smartfactory.preprocessing.enumeration.ShopFloorType;

import java.util.Date;
import java.util.Map;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ShopFloorData {

  @Id
  private String id;

  @NotBlank
  private String ofId;

  private String opId;

  @NotBlank
  private String shopFloorPostReference;

  private String shopFloorLineLabel;

  private String shopFloorMachineReference;

  @Enumerated(EnumType.STRING)
  private ShopFloorType shopFloorType;

  @NotNull

  private Date eventDate;

  @NotNull
  private Map<String, String> data;

  public String getId() {

    return this.id;
  }

  public void setId(final String id) {

    this.id = id;
  }

  public String getShopFloorPostReference() {

    return this.shopFloorPostReference;
  }

  public void setShopFloorPostReference(final String shopFloorPostReference) {

    this.shopFloorPostReference = shopFloorPostReference;
  }

  public String getShopFloorLineLabel() {

    return this.shopFloorLineLabel;
  }

  public void setShopFloorLineLabel(final String shopFloorLineLabel) {

    this.shopFloorLineLabel = shopFloorLineLabel;
  }

  public ShopFloorType getShopFloorType() {

    return this.shopFloorType;
  }

  public void setShopFloorType(final ShopFloorType shopFloorType) {

    this.shopFloorType = shopFloorType;
  }

  public Date getEventDate() {

    return this.eventDate;
  }

  public void setEventDate(final Date eventDate) {

    this.eventDate = eventDate;
  }

  public Map<String, String> getData() {

    return this.data;
  }

  public void setData(final Map<String, String> data) {

    this.data = data;
  }

  public String getShopFloorMachineReference() {

    return this.shopFloorMachineReference;
  }

  public void setShopFloorMachineReference(final String shopFloorMachineReference) {

    this.shopFloorMachineReference = shopFloorMachineReference;
  }

  public String getOfId() {

    return this.ofId;
  }

  public void setOfId(final String ofId) {

    this.ofId = ofId;
  }

  public String getOpId() {

    return this.opId;
  }

  public void setOpId(final String opId) {

    this.opId = opId;
  }

  @Override
  public String toString() {

    return "ShopFloorData [id=" + this.id + ", ofId=" + this.ofId + ", opId=" + this.opId
        + ", shopFloorPostReference=" + this.shopFloorPostReference + ", shopFloorLineLabel="
        + this.shopFloorLineLabel + ", shopFloorMachineReference=" + this.shopFloorMachineReference
        + ", shopFloorType=" + this.shopFloorType + ", eventDate=" + this.eventDate + ", data="
        + this.data + "]";
  }

}
