
package com.addixo.smartfactory.preprocessing.config;

public class Environment {

  public static final String SHOP_FLOOR_TOPIC = "shopfloordata->preprocessing";

  public static final String PRE_POST_INFORMATION_TOPIC = "preprocessing->postprocessing->information";

  public static final String PRE_POST_STATUS_TOPIC = "preprocessing->postprocessing->status";

  public static final String PRE_POST_PRODUCTION_TOPIC = "preprocessing->postprocessing->production";
}
