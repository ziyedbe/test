
package com.addixo.smartfactory.preprocessing.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.addixo.smartfactory.preprocessing.repository")
public class MongoConfig extends AbstractMongoConfiguration {

  @Value("${spring.data.mongodb.database}")
  private String databaseName;

  @Value("${spring.data.mongodb.port}")
  private int mongoPort;

  @Value("${spring.data.mongodb.host}")
  private String mongoHost;

  @Override
  protected String getDatabaseName() {

    return this.databaseName;
  }

  @Override
  public Mongo mongo() throws Exception {

    return new MongoClient(this.mongoHost, this.mongoPort);
  }

  @Override
  protected String getMappingBasePackage() {

    return "com.addixo.smartfactory.preprocessing";
  }
}
