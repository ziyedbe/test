
package com.addixo.smartfactory.preprocessing.service;

import com.addixo.smartfactory.preprocessing.config.Environment;
import com.addixo.smartfactory.preprocessing.model.ShopFloorData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class SwitchDataServiceImpl implements SwitchDataService {

  private static final Logger logger = LoggerFactory.getLogger(SwitchDataServiceImpl.class);

  @Autowired
  private StringRedisTemplate redisTemplate;

  private final Gson gson;

  @Autowired
  private ShopFloorDataService shopFloorDataService;

  public SwitchDataServiceImpl() {

    this.gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
  }

  @Override
  @Async
  public void switchShopFloorData(final ShopFloorData shopFloorData) {

    logger.info("shopFloorData {}", shopFloorData);
    this.shopFloorDataService.saveShopFloorData(shopFloorData).ifPresent(shopFloorDataSaved -> {
      logger.debug("shopFloorData Saved: {}", shopFloorDataSaved);

      final String shopFloorDataSavedString = this.gson.toJson(shopFloorData);
      logger.debug("shopFloorData ShopFloorType: {}", shopFloorData.getShopFloorType());
      switch (shopFloorData.getShopFloorType()) {
      case production:
        this.sendToProstProcessing(shopFloorDataSavedString, Environment.PRE_POST_PRODUCTION_TOPIC);
        break;
      case status:
        this.sendToProstProcessing(shopFloorDataSavedString, Environment.PRE_POST_STATUS_TOPIC);
        break;
      case information:
        this.sendToProstProcessing(shopFloorDataSavedString,
            Environment.PRE_POST_INFORMATION_TOPIC);
        break;

      default:
        logger.warn("case :{} ", "default");
        break;

      }
    });
  }

  private void sendToProstProcessing(final String shopFloorDataSavedString, final String topic) {

    logger.debug("Start sendToProstProcessing case :{} ", topic);
    this.redisTemplate.convertAndSend(topic, shopFloorDataSavedString);
    logger.info("Finish  {}", "sendToProstProcessing");

  }

}
