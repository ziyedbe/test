
package com.addixo.smartfactory.postprocessing.config;

public class Environment {

  public static final String SHOP_FLOOR_TOPIC = "shopfloordata->preprocessing";

  public static final String PRE_POST_INFORMATION_TOPIC = "preprocessing->postprocessing->information";

  public static final String PRE_POST_STATUS_TOPIC = "preprocessing->postprocessing->status";

  public static final String PRE_POST_PRODUCTION_TOPIC = "preprocessing->postprocessing->production";

  public static final String findOrganizationByWorkshop = "SELECT o.organization_name As organizationName,\r\n"
      + "u.work_shop_id As workShopId,\r\n" + "u.work_shop_label As workShopLabel,\r\n"
      + "u.work_shop_description As workShopDescription,      \r\n"
      + "s.shift_start As shiftStart ,s.shift_end As shiftEnd\r\n"
      + "FROM postprocessing.work_shop u,organization o,shift s\r\n"
      + "where o.organization_id=work_shop_organization_id\r\n"
      + "and u.work_shop_shift_id=s.shift_id;";
}
