
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorProduction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopFloorProductionRepository extends JpaRepository<ShopFloorProduction, String>,
    PagingAndSortingRepository<ShopFloorProduction, String>,
    QueryDslPredicateExecutor<ShopFloorProduction> {

  // List<ShopFloorProduction>
  // findByShopFloorProdPostReferenceAndShopFloorProdLineLabelAndShopFloorProdMachineReferenceAndShopFloorProdEventDateBetween(
  // String shopFloorProdPostReference, String shopFloorProdLineLabel,
  // String shopFloorProdMachineReference, Date startDate, Date endtDate);
  //
  // Page<ShopFloorProduction> findByShopFloorProdOfIdAndShopFloorProdEventDateBetween(
  // String shopFloorProdOfId, Date startDate, Date endtDate, Pageable pageable);

}
