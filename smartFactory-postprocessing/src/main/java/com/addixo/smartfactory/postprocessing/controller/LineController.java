
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Line;
import com.addixo.smartfactory.postprocessing.service.LineService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class LineController {

  private static final Logger logger = LoggerFactory.getLogger(LineController.class);

  @Autowired
  private LineService lineService;

  @PostMapping("/line")
  public Line createLigne(@Valid @RequestBody final Line line) {

    logger.debug("create line : {}", line);
    return this.lineService.saveOrUpdateLine(line);
  }

  @GetMapping("/line")
  public List<Line> listAllLigne() {

    logger.debug("get all {}", "line ");
    return this.lineService.findAllLine();
  }

  @GetMapping("/line/byWorkShopId/{lineWorkshopId}")
  public List<Line> getlistAllLigneByWorkShopId(
      @PathVariable("lineWorkshopId") final Long lineWorkshopId) {

    logger.debug("get list all line by workShop id {} ", lineWorkshopId);

    return this.lineService.findLineByLineWorkshopId(lineWorkshopId);
  }

  @DeleteMapping("/line/{id}")
  public String deleteLigne(@PathVariable("id") final Long lineId) {

    logger.debug("delete line by id {}", lineId);
    final Line line = this.lineService.findLineById(lineId)
        .orElseThrow(() -> new ResourceNotFoundException(lineId.toString(), "lineId not found"));
    this.lineService.deleteLine(line.getLineId());
    return "deleted";
  }

  @GetMapping("/line/{id}")
  public Line getLigneById(@PathVariable("id") final Long lineId) {

    logger.debug("get line by id {}", lineId);
    return this.lineService.findLineById(lineId)
        .orElseThrow(() -> new ResourceNotFoundException(lineId.toString(), "lineId not found"));
  }

  @PutMapping("/line/{id}")
  public Line updateLigne(@PathVariable("id") final Long id,
      @Valid @RequestBody final Line newLigne) {

    logger.debug("update line by id {}", id);
    final Line oldLigne = this.lineService.findLineById(id)
        .orElseThrow(() -> new ResourceNotFoundException(id, "lineId not found"));
    newLigne.setLineId(oldLigne.getLineId());
    return this.lineService.saveOrUpdateLine(newLigne);
  }
}