
package com.addixo.smartfactory.postprocessing.domain;

import com.addixo.smartfactory.postprocessing.enumeration.AutoManuallyType;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_ShopFloorProduction", columnList = "shopFloorProdOfId,shopFloorProdOpId,shopFloorProdEventDate"))

public class ShopFloorProduction implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private String shopFloorProdId;

  private String shopFloorProdOfId;

  private String shopFloorProdOpId;

  @NotBlank
  private String shopFloorProdPostReference;

  @NotBlank
  private String shopFloorProdLineLabel;

  private String shopFloorProdMachineReference;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date shopFloorProdEventDate;

  private Long shopFloorProdQuantity;

  private Long shopFloorProdQuantityNotOk;

  @NotNull
  @Enumerated(EnumType.STRING)
  private AutoManuallyType shopFloorProdInsertionType;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public String getShopFloorProdId() {

    return this.shopFloorProdId;
  }

  public void setShopFloorProdId(final String shopFloorProdId) {

    this.shopFloorProdId = shopFloorProdId;
  }

  public String getShopFloorProdOfId() {

    return this.shopFloorProdOfId;
  }

  public void setShopFloorProdOfId(final String shopFloorProdOfId) {

    this.shopFloorProdOfId = shopFloorProdOfId;
  }

  public String getShopFloorProdOpId() {

    return this.shopFloorProdOpId;
  }

  public void setShopFloorProdOpId(final String shopFloorProdOpId) {

    this.shopFloorProdOpId = shopFloorProdOpId;
  }

  public String getShopFloorProdPostReference() {

    return this.shopFloorProdPostReference;
  }

  public void setShopFloorProdPostReference(final String shopFloorProdPostReference) {

    this.shopFloorProdPostReference = shopFloorProdPostReference;
  }

  public String getShopFloorProdLineLabel() {

    return this.shopFloorProdLineLabel;
  }

  public void setShopFloorProdLineLabel(final String shopFloorProdLineLabel) {

    this.shopFloorProdLineLabel = shopFloorProdLineLabel;
  }

  public String getShopFloorProdMachineReference() {

    return this.shopFloorProdMachineReference;
  }

  public void setShopFloorProdMachineReference(final String shopFloorProdMachineReference) {

    this.shopFloorProdMachineReference = shopFloorProdMachineReference;
  }

  public Date getShopFloorProdEventDate() {

    return this.shopFloorProdEventDate;
  }

  public void setShopFloorProdEventDate(final Date shopFloorProdEventDate) {

    this.shopFloorProdEventDate = shopFloorProdEventDate;
  }

  public Long getShopFloorProdQuantity() {

    return this.shopFloorProdQuantity;
  }

  public void setShopFloorProdQuantity(final Long shopFloorProdQuantity) {

    this.shopFloorProdQuantity = shopFloorProdQuantity;
  }

  public Long getShopFloorProdQuantityNotOk() {

    return this.shopFloorProdQuantityNotOk;
  }

  public void setShopFloorProdQuantityNotOk(final Long shopFloorProdQuantityNotOk) {

    this.shopFloorProdQuantityNotOk = shopFloorProdQuantityNotOk;
  }

  public AutoManuallyType getShopFloorProdInsertionType() {

    return this.shopFloorProdInsertionType;
  }

  public void setShopFloorProdInsertionType(final AutoManuallyType shopFloorProdInsertionType) {

    this.shopFloorProdInsertionType = shopFloorProdInsertionType;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "ShopFloorProduction [shopFloorProdId=" + this.shopFloorProdId + ", shopFloorProdOfId="
        + this.shopFloorProdOfId + ", shopFloorProdOpId=" + this.shopFloorProdOpId
        + ", shopFloorProdPostReference=" + this.shopFloorProdPostReference
        + ", shopFloorProdLineLabel=" + this.shopFloorProdLineLabel
        + ", shopFloorProdMachineReference=" + this.shopFloorProdMachineReference
        + ", shopFloorProdEventDate=" + this.shopFloorProdEventDate + ", shopFloorProdQuantity="
        + this.shopFloorProdQuantity + ", shopFloorProdQuantityNotOk="
        + this.shopFloorProdQuantityNotOk + ", shopFloorProdInsertionType="
        + this.shopFloorProdInsertionType + ", createdAt=" + this.createdAt + ", updatedAt="
        + this.updatedAt + "]";
  }

}
