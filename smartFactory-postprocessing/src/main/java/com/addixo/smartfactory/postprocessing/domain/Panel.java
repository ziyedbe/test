
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Panel", columnList = "panelExternalId,panelReference"))
public class Panel implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long panelId;

  private String panelExternalId;

  @Column(unique = true)
  private String panelReference;

  private String panelType;

  private String panelAdrmac;

  private String panelEStor;

  private String panelESusb;

  private String panelESserial;

  private String panelOutputTor;

  private String panelESrj45;

  private String panelWorkShopLabel;

  private String panelLineLabel;

  private String panelPostReference;

  private String panelMachineReference;

  private String panelDescription;

  @Lob
  private String panelAffectPins;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getPanelId() {

    return this.panelId;
  }

  public void setPanelId(final Long panelId) {

    this.panelId = panelId;
  }

  public String getPanelExternalId() {

    return this.panelExternalId;
  }

  public void setPanelExternalId(final String panelExternalId) {

    this.panelExternalId = panelExternalId;
  }

  public String getPanelReference() {

    return this.panelReference;
  }

  public void setPanelReference(final String panelReference) {

    this.panelReference = panelReference;
  }

  public String getPanelType() {

    return this.panelType;
  }

  public void setPanelType(final String panelType) {

    this.panelType = panelType;
  }

  public String getPanelAdrmac() {

    return this.panelAdrmac;
  }

  public void setPanelAdrmac(final String panelAdrmac) {

    this.panelAdrmac = panelAdrmac;
  }

  public String getPanelEStor() {

    return this.panelEStor;
  }

  public void setPanelEStor(final String panelEStor) {

    this.panelEStor = panelEStor;
  }

  public String getPanelESusb() {

    return this.panelESusb;
  }

  public void setPanelESusb(final String panelESusb) {

    this.panelESusb = panelESusb;
  }

  public String getPanelESserial() {

    return this.panelESserial;
  }

  public void setPanelESserial(final String panelESserial) {

    this.panelESserial = panelESserial;
  }

  public String getPanelOutputTor() {

    return this.panelOutputTor;
  }

  public void setPanelOutputTor(final String panelOutputTor) {

    this.panelOutputTor = panelOutputTor;
  }

  public String getPanelESrj45() {

    return this.panelESrj45;
  }

  public void setPanelESrj45(final String panelESrj45) {

    this.panelESrj45 = panelESrj45;
  }

  public String getPanelWorkShopLabel() {

    return this.panelWorkShopLabel;
  }

  public void setPanelWorkShopLabel(final String panelWorkShopLabel) {

    this.panelWorkShopLabel = panelWorkShopLabel;
  }

  public String getPanelLineLabel() {

    return this.panelLineLabel;
  }

  public void setPanelLineLabel(final String panelLineLabel) {

    this.panelLineLabel = panelLineLabel;
  }

  public String getPanelPostReference() {

    return this.panelPostReference;
  }

  public void setPanelPostReference(final String panelPostReference) {

    this.panelPostReference = panelPostReference;
  }

  public String getPanelMachineReference() {

    return this.panelMachineReference;
  }

  public void setPanelMachineReference(final String panelMachineReference) {

    this.panelMachineReference = panelMachineReference;
  }

  public String getPanelDescription() {

    return this.panelDescription;
  }

  public void setPanelDescription(final String panelDescription) {

    this.panelDescription = panelDescription;
  }

  public String getPanelAffectPins() {

    return this.panelAffectPins;
  }

  public void setPanelAffectPins(final String panelAffectPins) {

    this.panelAffectPins = panelAffectPins;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Panel [panelId=" + this.panelId + ", panelExternalId=" + this.panelExternalId
        + ", panelReference=" + this.panelReference + ", panelType=" + this.panelType
        + ", panelAdrmac=" + this.panelAdrmac + ", panelEStor=" + this.panelEStor + ", panelESusb="
        + this.panelESusb + ", panelESserial=" + this.panelESserial + ", panelOutputTor="
        + this.panelOutputTor + ", panelESrj45=" + this.panelESrj45 + ", panelWorkShopLabel="
        + this.panelWorkShopLabel + ", panelLineLabel=" + this.panelLineLabel
        + ", panelPostReference=" + this.panelPostReference + ", panelMachineReference="
        + this.panelMachineReference + ", panelDescription=" + this.panelDescription
        + ", panelAffectPins=" + this.panelAffectPins + ", createdAt=" + this.createdAt
        + ", updatedAt=" + this.updatedAt + "]";
  }

}
