
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.WorkShop;
import com.addixo.smartfactory.postprocessing.domain.dto.OrganizationWorkShop;
import com.addixo.smartfactory.postprocessing.repository.WorkShopRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class WorkShopServiceImpl implements WorkShopService {

  @Autowired
  private WorkShopRepository workShopRepository;

  @Autowired
  private LineService lineService;

  @Autowired
  private PostService postService;

  @Autowired
  private MachineService machineService;

  @Override
  @Transactional(readOnly = true)
  public List<WorkShop> findAllWorkShop() {

    return this.workShopRepository.findAll();
  }

  @Override
  public WorkShop saveOrUpdateWorkShop(final WorkShop workShop) {

    return this.workShopRepository.save(workShop);
  }

  @Override
  public void deleteWorkShop(final Long workShopId) {

    lineService.findLineByLineWorkshopId(workShopId).forEach(line -> {

      lineService.deleteLine(line.getLineId());
    });

    this.findWorkShopById(workShopId)
        .ifPresent(workshop -> this.workShopRepository.delete(workshop));
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<WorkShop> findWorkShopById(final Long id) {

    return Optional.ofNullable(this.workShopRepository.findOne(id));
  }

  @Override
  @Transactional(readOnly = true)
  public List<OrganizationWorkShop> findOrganizationByWorkshop() {

    return this.workShopRepository.findOrganizationByWorkshop();
  }

}
