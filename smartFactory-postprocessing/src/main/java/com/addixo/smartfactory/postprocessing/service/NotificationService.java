
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Notification;

import java.util.List;
import java.util.Optional;

public interface NotificationService {

  List<Notification> findAllNotification();

  Notification saveOrUpdateNotification(Notification notification);

  void deleteNotification(Long id);

  Optional<Notification> findNotificationById(Long id);
}
