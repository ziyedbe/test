
package com.addixo.smartfactory.postprocessing.util;

public class ExceptionResponse {

  private String errorCode;

  private String errorMessage;

  public ExceptionResponse() {

  }

  public ExceptionResponse(final String errorCode, final String errorMessage) {

    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
  }

  public String getErrorCode() {

    return this.errorCode;
  }

  public void setErrorCode(final String errorCode) {

    this.errorCode = errorCode;
  }

  public String getErrorMessage() {

    return this.errorMessage;
  }

  public void setErrorMessage(final String errorMessage) {

    this.errorMessage = errorMessage;
  }
}
