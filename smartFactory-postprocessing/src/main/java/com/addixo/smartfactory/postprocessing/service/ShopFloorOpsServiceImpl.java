
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorOps;
import com.addixo.smartfactory.postprocessing.repository.ShopFloorOpsRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class ShopFloorOpsServiceImpl implements ShopFloorOpsService {

  @Autowired
  private ShopFloorOpsRepository shopFloorOpsRepository;

  @Override
  @Transactional(readOnly = true)
  public List<ShopFloorOps> findAllShopFloorOps() {

    return this.shopFloorOpsRepository.findAll();
  }

  @Override
  public ShopFloorOps saveOrUpdateShopFloorOps(final ShopFloorOps monitoringInformation) {

    return this.shopFloorOpsRepository.save(monitoringInformation);
  }

  @Override
  public void deleteShopFloorOps(final String id) {

    this.shopFloorOpsRepository.delete(id);
  }

  @Override
  public ShopFloorOps updateShopFloorOps(final ShopFloorOps monitoringInformation) {

    return this.saveOrUpdateShopFloorOps(monitoringInformation);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ShopFloorOps> findShopFloorOpsById(final String id) {

    return Optional.ofNullable(this.shopFloorOpsRepository.findOne(id));
  }

  @Override
  @Transactional(readOnly = true)
  public List<ShopFloorOps> findByShopFloorOpsPostReferenceAndShopFloorOpsLineLabelAndShopFloorOpsMachineReferenceAndShopFloorOpsEventDateBetween(
      final String shopFloorOpsPostReference, final String shopFloorOpsLineLabel,
      final String shopFloorOpsMachineReference, final Date startDate, final Date endtDate) {

    return this.shopFloorOpsRepository
        .findByShopFloorOpsPostReferenceAndShopFloorOpsLineLabelAndShopFloorOpsMachineReferenceAndShopFloorOpsEventDateBetween(
            shopFloorOpsPostReference, shopFloorOpsLineLabel, shopFloorOpsMachineReference,
            startDate, endtDate);
  }

  @Override
  @Transactional(readOnly = true)
  public List<ShopFloorOps> findByShopFloorOpsOfIdAndShopFloorOpsEventDateBetween(
      final String shopFloorOpsOfId, final Date startDate, final Date endtDate) {

    return this.shopFloorOpsRepository.findByShopFloorOpsOfIdAndShopFloorOpsEventDateBetween(
        shopFloorOpsOfId, startDate, endtDate);
  }
}
