
package com.addixo.smartfactory.postprocessing.domain.dto;

import com.addixo.smartfactory.postprocessing.enumeration.ShopFloorType;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.Map;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ShopFloorData {

  private String id;

  private String ofId;

  private String opId;

  private String shopFloorPostReference;

  private String shopFloorMachineReference;

  private String shopFloorLineLabel;

  @Enumerated(EnumType.STRING)
  private ShopFloorType shopFloorType;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.DATE)
  private Date eventDate;

  private Map<String, String> data;

  public String getId() {

    return this.id;
  }

  public void setId(final String id) {

    this.id = id;
  }

  public String getOfId() {

    return this.ofId;
  }

  public void setOfId(final String ofId) {

    this.ofId = ofId;
  }

  public String getOpId() {

    return this.opId;
  }

  public void setOpId(final String opId) {

    this.opId = opId;
  }

  public String getShopFloorPostReference() {

    return this.shopFloorPostReference;
  }

  public void setShopFloorPostReference(final String shopFloorPostReference) {

    this.shopFloorPostReference = shopFloorPostReference;
  }

  public String getShopFloorMachineReference() {

    return this.shopFloorMachineReference;
  }

  public void setShopFloorMachineReference(final String shopFloorMachineReference) {

    this.shopFloorMachineReference = shopFloorMachineReference;
  }

  public String getShopFloorLineLabel() {

    return this.shopFloorLineLabel;
  }

  public void setShopFloorLineLabel(final String shopFloorLineLabel) {

    this.shopFloorLineLabel = shopFloorLineLabel;
  }

  public ShopFloorType getShopFloorType() {

    return this.shopFloorType;
  }

  public void setShopFloorType(final ShopFloorType shopFloorType) {

    this.shopFloorType = shopFloorType;
  }

  public Date getEventDate() {

    return this.eventDate;
  }

  public void setEventDate(final Date eventDate) {

    this.eventDate = eventDate;
  }

  public Map<String, String> getData() {

    return this.data;
  }

  public void setData(final Map<String, String> data) {

    this.data = data;
  }

}
