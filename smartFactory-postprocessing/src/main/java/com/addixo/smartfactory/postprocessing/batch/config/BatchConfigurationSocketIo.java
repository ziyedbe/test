
package com.addixo.smartfactory.postprocessing.batch.config;

import com.addixo.smartfactory.postprocessing.batch.step.BatchSocketIoReader;
import com.addixo.smartfactory.postprocessing.batch.step.BatchSocketIoWriter;
import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.service.DashboardMonitoringService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableBatchProcessing
public class BatchConfigurationSocketIo {

  private static final Logger logger = LoggerFactory.getLogger(BatchConfigurationSocketIo.class);

  @Autowired
  private SimpMessageSendingOperations messagingTemplate;

  @Autowired
  private StepBuilderFactory stepBuilderFactory;

  @Autowired
  private JobBuilderFactory jobBuilderFactory;

  @Autowired
  private DashboardMonitoringService dashboardMonitoringService;

  @Autowired
  private SimpleJobLauncher jobLauncher;

  @Autowired
  private JobExecutionListener listener;

  @Scheduled(cron = "${batch.cron.socketIo}")
  public void dashbordMonitoringSendToSocketIo() {

    try {
      JobParameters jobParameters = new JobParametersBuilder()
          .addLong("time", System.currentTimeMillis()).toJobParameters();
      this.jobLauncher.run(this.sendSocketIo(), jobParameters);
      logger.info("Launcher {}", " dashbordMonitoringSendToSocketIo");
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  public Job sendSocketIo() {

    return this.jobBuilderFactory.get("sendSocketIo").preventRestart()
        .incrementer(new RunIdIncrementer()).listener(this.listener).flow(this.sendSocketIoStep1())
        .end().build();
  }

  public Step sendSocketIoStep1() {

    return this.stepBuilderFactory.get("sendSocketIoStep1")
        .<DashboardMonitoring, DashboardMonitoring>chunk(10).reader(this.batchSocketIoReader())
        .writer(new BatchSocketIoWriter(this.messagingTemplate)).faultTolerant().build();
  }

  public BatchSocketIoReader batchSocketIoReader() {

    return new BatchSocketIoReader(this.getListDashboardMonitoring());
  }

  public List<DashboardMonitoring> getListDashboardMonitoring() {

    logger.debug("*** BATCH RED {} ***", "Read List DashboardMonitoring");
    return this.dashboardMonitoringService.findAllDashboardMonitoring();
  }

}
