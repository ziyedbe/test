
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Organization;
import com.addixo.smartfactory.postprocessing.service.OrganizationService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OrganizationController {

  private static final Logger logger = LoggerFactory.getLogger(OrganizationController.class);

  @Autowired
  private OrganizationService organizationService;

  @PostMapping("/organization")
  public Organization createOrganization(@Valid @RequestBody final Organization organization) {

    logger.debug("create organization : {}", organization);
    return this.organizationService.saveOrUpdateOrganization(organization);
  }

  @GetMapping("/organization")
  public List<Organization> listAllOrganization() {

    logger.debug("get all organization ");
    return this.organizationService.findAllOrganization();
  }

  @DeleteMapping("/organization/{id}")

  public String deleteOrganization(@PathVariable("id") final Long organizationId) {

    logger.debug("delete organization by id {}", organizationId);
    final Organization organization = this.organizationService.findOrganizationById(organizationId)
        .orElseThrow(() -> new ResourceNotFoundException(organizationId.toString(), "not found"));
    this.organizationService.deleteOrganization(organization.getOrganizationId());
    return "deleted";
  }

  @PutMapping("/organization/{id}")

  public Organization updateOrganization(@PathVariable("id") final Long id,
      @Valid @RequestBody final Organization newOrganization) {

    logger.debug("update organization by id {}", id);
    final Organization oldOrganization = this.organizationService.findOrganizationById(id)
        .orElseThrow(() -> new ResourceNotFoundException("organization", id));
    newOrganization.setOrganizationId(oldOrganization.getOrganizationId());
    return this.organizationService.saveOrUpdateOrganization(newOrganization);
  }
}
