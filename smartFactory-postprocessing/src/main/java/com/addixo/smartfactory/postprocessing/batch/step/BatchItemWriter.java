
package com.addixo.smartfactory.postprocessing.batch.step;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.service.DashboardMonitoringService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

public class BatchItemWriter implements ItemWriter<DashboardMonitoring> {

  private static final Logger logger = LoggerFactory.getLogger(BatchItemWriter.class);

  private final DashboardMonitoringService dashboardMonitoringService;

  public BatchItemWriter(final DashboardMonitoringService dashboardMonitoringService) {

    this.dashboardMonitoringService = dashboardMonitoringService;
  }

  @Override
  public void write(final List<? extends DashboardMonitoring> items) throws Exception {

    logger.info("start write {}", " BatchItemWriter");
    logger.debug("start process items size {}", items.size());
    this.dashboardMonitoringService
        .saveOrUpdateDashboardMonitoringList((List<DashboardMonitoring>) items);
    logger.debug("end {}", " write");
  }
}
