
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Notification;
import com.addixo.smartfactory.postprocessing.service.NotificationService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class NotificationController {

  private static final Logger logger = LoggerFactory.getLogger(NotificationController.class);

  @Autowired
  private NotificationService notificationService;

  @PostMapping("/notification")
  public Notification createNotification(@Valid @RequestBody final Notification notification) {

    logger.debug("create notification : {}", notification);
    return this.notificationService.saveOrUpdateNotification(notification);
  }

  @GetMapping("/notification")

  public List<Notification> listAllNotification() {

    logger.debug("get all notification ");
    return this.notificationService.findAllNotification();
  }

  @DeleteMapping("/notification/{id}")

  public String deleteNotification(@PathVariable("id") final Long notificationId) {

    logger.debug("delete notification by id {}", notificationId);
    final Notification notification = this.notificationService.findNotificationById(notificationId)
        .orElseThrow(() -> new ResourceNotFoundException(notificationId.toString(), "not found"));
    this.notificationService.deleteNotification(notification.getNotificationId());
    return "deleted";
  }

  @PutMapping("/notification/{id}")

  public Notification updateNotification(@PathVariable("id") final Long id,
      @Valid @RequestBody final Notification newNotification) {

    logger.debug("update notification by id {}", id);
    final Notification oldNotification = this.notificationService.findNotificationById(id)
        .orElseThrow(() -> new ResourceNotFoundException("notification", id));
    newNotification.setNotificationId(oldNotification.getNotificationId());
    return this.notificationService.saveOrUpdateNotification(newNotification);
  }
}