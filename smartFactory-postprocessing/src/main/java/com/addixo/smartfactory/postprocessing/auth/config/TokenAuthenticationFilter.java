
package com.addixo.smartfactory.postprocessing.auth.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

  @Autowired
  private TokenHelper tokenHelper;

  @Autowired
  private UserDetailsService userDetailsService;

  public static final String ROOT_MATCHER = "/";

  public static final String FAVICON_MATCHER = "/favicon.ico";

  public static final String HTML_MATCHER = "/**/*.html";

  public static final String CSS_MATCHER = "/**/*.css";

  public static final String JS_MATCHER = "/**/*.js";

  public static final String IMG_MATCHER = "/images/*";

  public static final String LOGIN_MATCHER = "/auth/login";

  public static final String LOGOUT_MATCHER = "/auth/logout";

  private final List<String> pathsToSkip = Arrays.asList(ROOT_MATCHER, HTML_MATCHER,
      FAVICON_MATCHER, CSS_MATCHER, JS_MATCHER, IMG_MATCHER, LOGIN_MATCHER, LOGOUT_MATCHER);

  @Override
  public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
      final FilterChain chain) throws IOException, ServletException {

    String authToken = this.tokenHelper.getToken(request);
    if ((authToken != null) && !this.skipPathRequest(request, this.pathsToSkip)) {
      try {
        String username = this.tokenHelper.getUsernameFromToken(authToken);
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
        TokenBasedAuthentication authentication = new TokenBasedAuthentication(userDetails);
        authentication.setToken(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
      } catch (Exception e) {
        SecurityContextHolder.getContext().setAuthentication(new AnonAuthentication());
      }
    } else {
      SecurityContextHolder.getContext().setAuthentication(new AnonAuthentication());
    }

    chain.doFilter(request, response);
  }

  private boolean skipPathRequest(final HttpServletRequest request,
      final List<String> pathsToSkip) {

    Assert.notNull(pathsToSkip, "path cannot be null.");
    List<RequestMatcher> m = pathsToSkip.stream().map(path -> new AntPathRequestMatcher(path))
        .collect(Collectors.toList());
    OrRequestMatcher matchers = new OrRequestMatcher(m);
    return matchers.matches(request);
  }

}