
package com.addixo.smartfactory.postprocessing.auth.domain.dto;

import com.addixo.smartfactory.postprocessing.auth.config.TokenHelper;

import org.springframework.beans.factory.annotation.Autowired;

public class UserTokenState {

  private String access_token;

  private Long expires_in;

  @Autowired
  private TokenHelper tokenHelper;

  public UserTokenState() {

    this.access_token = null;
    this.expires_in = null;
  }

  public UserTokenState(final String access_token, final long expires_in) {

    this.access_token = access_token;
    this.expires_in = expires_in;
  }

  public String getAccess_token() {

    return this.access_token;
  }

  public void setAccess_token(final String access_token) {

    this.access_token = access_token;
  }

  public Long getExpires_in() {

    return this.expires_in;
  }

  public void setExpires_in(final Long expires_in) {

    this.expires_in = expires_in;
  }
}
