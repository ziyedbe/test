
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Operation;
import com.addixo.smartfactory.postprocessing.repository.OperationRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class OperationServiceImpl implements OperationService {

  @Autowired
  private OperationRepository operationRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Operation> findAllOperation() {

    return this.operationRepository.findAll();
  }

  @Override
  public Operation saveOrUpdateOperation(final Operation operation) {

    return this.operationRepository.save(operation);
  }

  @Override
  public void deleteOperation(final Long operationId) {

    this.operationRepository.delete(operationId);

  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Operation> findOperationById(final Long id) {

    return Optional.of(this.operationRepository.findOne(id));
  }

  @Override
  @Transactional(readOnly = true)
  public List<Operation> findByOperationOfId(final String operationOfId) {

    return this.operationRepository.findByOperationOfId(operationOfId);
  }

}
