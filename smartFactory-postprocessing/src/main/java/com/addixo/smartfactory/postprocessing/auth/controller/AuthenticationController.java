
package com.addixo.smartfactory.postprocessing.auth.controller;

import com.addixo.smartfactory.postprocessing.auth.config.TokenHelper;
import com.addixo.smartfactory.postprocessing.auth.domain.dto.ResetPassword;
import com.addixo.smartfactory.postprocessing.auth.domain.dto.UserTokenState;
import com.addixo.smartfactory.postprocessing.auth.service.CustomUserDetailsService;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {

  @Autowired
  private CustomUserDetailsService userDetailsService;

  @Autowired
  private TokenHelper tokenHelper;

  @Value("${jwt.expires_in}")
  private int expiresIn;

  @Value("${jwt.cookie}")
  private String tokenCookie;

  @RequestMapping(value = "/refresh", method = RequestMethod.GET)
  public ResponseEntity<?> refreshAuthenticationToken(final HttpServletRequest request,
      final HttpServletResponse response) {

    String authToken = this.tokenHelper.getToken(request);
    if ((authToken != null) && this.tokenHelper.canTokenBeRefreshed(authToken)) {
      String refreshedToken = this.tokenHelper.refreshToken(authToken);
      Cookie authCookie = new Cookie(this.tokenCookie, refreshedToken);
      authCookie.setPath("/");
      authCookie.setHttpOnly(true);
      authCookie.setMaxAge(this.expiresIn);
      response.addCookie(authCookie);
      UserTokenState userTokenState = new UserTokenState(refreshedToken, this.expiresIn);
      return ResponseEntity.ok(userTokenState);
    } else {
      UserTokenState userTokenState = new UserTokenState();
      return ResponseEntity.accepted().body(userTokenState);
    }
  }

  @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
  public ResponseEntity<?> changePassword(@RequestBody final ResetPassword passwordChanger) {

    this.userDetailsService.changePassword(passwordChanger.getOldPassword(),
        passwordChanger.getNewPassword());
    Map<String, String> result = new HashMap<>();
    result.put("result", "success");
    return ResponseEntity.accepted().body(result);
  }
}
