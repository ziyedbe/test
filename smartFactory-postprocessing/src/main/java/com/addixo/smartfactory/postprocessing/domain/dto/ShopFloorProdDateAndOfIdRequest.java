
package com.addixo.smartfactory.postprocessing.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;

public class ShopFloorProdDateAndOfIdRequest {

  @NotBlank
  private String shopFloorProdOfId;

  private String shopFloorProdOpId;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date startDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date endtDate;

  public String getShopFloorProdOfId() {

    return this.shopFloorProdOfId;
  }

  public void setShopFloorProdOfId(final String shopFloorProdOfId) {

    this.shopFloorProdOfId = shopFloorProdOfId;
  }

  public Date getStartDate() {

    return this.startDate;
  }

  public void setStartDate(final Date startDate) {

    this.startDate = startDate;
  }

  public Date getEndtDate() {

    return this.endtDate;
  }

  public void setEndtDate(final Date endtDate) {

    this.endtDate = endtDate;
  }

  public String getShopFloorProdOpId() {

    return this.shopFloorProdOpId;
  }

  public void setShopFloorProdOpId(final String shopFloorProdOpId) {

    this.shopFloorProdOpId = shopFloorProdOpId;
  }

  @Override
  public String toString() {

    return "ShopFloorProdDateAndOfIdRequest [shopFloorProdOfId=" + this.shopFloorProdOfId
        + ", shopFloorProdOpId=" + this.shopFloorProdOpId + ", startDate=" + this.startDate
        + ", endtDate=" + this.endtDate + "]";
  }

}
