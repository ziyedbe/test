
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Settings;
import com.addixo.smartfactory.postprocessing.service.SettingsService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SettingsController {

  private static final Logger logger = LoggerFactory.getLogger(SettingsController.class);

  @Autowired
  private SettingsService settingsService;

  @PostMapping("/settings")
  public Settings createSettings(@Valid @RequestBody final Settings settings) {

    logger.debug("create settings : {}", settings);
    return this.settingsService.saveOrUpdateSettings(settings);
  }

  @GetMapping("/settings")
  public List<Settings> listAllSettings() {

    logger.debug("get all {}", "settings ");
    return this.settingsService.findAllSettings();
  }

  @DeleteMapping("/settings/{id}")
  public String deleteSettings(@PathVariable("id") final Long settingsId) {

    logger.debug("delete settings by id {}", settingsId);
    final Settings settings = this.settingsService.findSettingsById(settingsId)
        .orElseThrow(() -> new ResourceNotFoundException(settingsId.toString(), "not found"));
    this.settingsService.deleteSettings(settings.getSettingsId());
    return "deleted";
  }

  @PutMapping("/settings/{id}")
  public Settings updateSettings(@PathVariable("id") final Long id,
      @Valid @RequestBody final Settings newSettings) {

    logger.debug("update settings by id {}", id);
    final Settings oldSettings = this.settingsService.findSettingsById(id)
        .orElseThrow(() -> new ResourceNotFoundException("settings", id));
    newSettings.setSettingsId(oldSettings.getSettingsId());
    return this.settingsService.saveOrUpdateSettings(newSettings);
  }
}
