
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.SkgIoItem;
import com.addixo.smartfactory.postprocessing.repository.SkgIoItemRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SkgIoItemServiceImpl implements SkgIoItemService {

  @Autowired
  private SkgIoItemRepository skgIoItemRepository;

  @Override
  @Transactional(readOnly = true)
  public List<SkgIoItem> findAllSkgIoItem() {

    return this.skgIoItemRepository.findAll();
  }

  @Override
  public SkgIoItem saveOrUpdateSkgIoItem(final SkgIoItem skgIoItem) {

    return this.skgIoItemRepository.save(skgIoItem);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<SkgIoItem> findBySkgItemId(final Long skgItemId) {

    return Optional.of(this.skgIoItemRepository.findOne(skgItemId));
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<SkgIoItem> findBySkgIoItemEntrer(final String skgIoItemEntrer) {

    return this.skgIoItemRepository.findBySkgIoItemEntrer(skgIoItemEntrer);
  }

  @Override
  public void deleteSkgIoItem(final Long skgIoItemId) {

    this.skgIoItemRepository.delete(skgIoItemId);

  }

}
