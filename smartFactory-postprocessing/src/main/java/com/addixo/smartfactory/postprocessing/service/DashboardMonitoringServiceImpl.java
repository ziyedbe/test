
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.domain.Operation;
import com.addixo.smartfactory.postprocessing.domain.QDashboardMonitoring;
import com.addixo.smartfactory.postprocessing.domain.dto.DashboardMonitoringDto;
import com.addixo.smartfactory.postprocessing.domain.dto.DashboardMonitoringOperation;
import com.addixo.smartfactory.postprocessing.repository.DashboardMonitoringRepository;
import com.google.common.base.Preconditions;
import com.querydsl.core.types.dsl.BooleanExpression;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service

public class DashboardMonitoringServiceImpl implements DashboardMonitoringService {

  @Autowired
  private DashboardMonitoringRepository dashboardMonitoringRepository;

  @Autowired
  private OperationService OperationService;

  @Override
  @Transactional(readOnly = true)
  public List<DashboardMonitoring> findAllDashboardMonitoring() {

    return this.dashboardMonitoringRepository.findAll();
  }

  @Override
  public DashboardMonitoring saveOrUpdateDashboardMonitoring(
      final DashboardMonitoring dashboardMonitoring) {

    return this.dashboardMonitoringRepository.save(dashboardMonitoring);
  }

  @Override
  public List<DashboardMonitoring> saveOrUpdateDashboardMonitoringList(
      final List<DashboardMonitoring> dashboardMonitoringList) {

    return this.dashboardMonitoringRepository.save(dashboardMonitoringList);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<DashboardMonitoring> findDashboardMonitoringById(final String id) {

    return Optional.ofNullable(this.dashboardMonitoringRepository.findOne(id));
  }

  @Override
  public void deleteDashboardMonitoring(final String dashboardMonitoringId) {

    this.dashboardMonitoringRepository.delete(dashboardMonitoringId);
  }

  @Override
  public boolean isDashboardMonitoringExist(final DashboardMonitoring dashboardMonitoring) {

    return this.findDashboardMonitoringById(dashboardMonitoring.getDashboardOfId()).isPresent();
  }

  @Override
  @Transactional(readOnly = true)
  public List<DashboardMonitoring> findDashboardByDashboardMonitoringUserId(
      final Long dashboardMonitoringUserId) {

    return this.dashboardMonitoringRepository.findByDashboardUserId(dashboardMonitoringUserId);
  }

  @Override
  @Transactional(readOnly = true)
  public List<DashboardMonitoring> findDashboardMonitoringByDashboardMonitoringLine(
      final String dashboardMonitoringLine) {

    return this.dashboardMonitoringRepository.findByDashboardLineLabel(dashboardMonitoringLine);
  }

  @Override
  @Transactional(readOnly = true)
  public List<DashboardMonitoring> findDashboardMonitoringByDashboardMonitoringOf(
      final String dashboardMonitoringOf) {

    return this.dashboardMonitoringRepository.findByDashboardOfId(dashboardMonitoringOf);
  }

  @Override
  @Transactional(readOnly = true)
  public List<DashboardMonitoring> findDashboardMonitoringByDashboardMonitoringWorkShop(
      final String dashboardMonitoringWorkShop) {

    return this.dashboardMonitoringRepository
        .findByDashboardWorkShopLabel(dashboardMonitoringWorkShop);
  }

  @Override
  public List<DashboardMonitoringOperation> findDashboardMonitoringWithOperations() {

    List<DashboardMonitoring> listDashboardMonitoring = this.dashboardMonitoringRepository
        .findAll();
    List<DashboardMonitoringOperation> listDashboardMonitoringOperation = new ArrayList<DashboardMonitoringOperation>();
    listDashboardMonitoring.forEach(dash -> {
      List<Operation> listOperation = this.OperationService
          .findByOperationOfId(dash.getDashboardOfId());
      DashboardMonitoringOperation dashboardMonitoringOperation = this
          .mappingDashbordMonitoringMethod(dash, listOperation);
      listDashboardMonitoringOperation.add(dashboardMonitoringOperation);
    });
    return listDashboardMonitoringOperation;
  }

  private DashboardMonitoringOperation mappingDashbordMonitoringMethod(
      final DashboardMonitoring dashboard, final List<Operation> listOperation) {

    final DashboardMonitoringOperation dashboardMonitoringOperation = new DashboardMonitoringOperation();

    dashboardMonitoringOperation
        .setDashboardDateTimeFinishPlanned(dashboard.getDashboardDateTimeFinishPlanned());
    dashboardMonitoringOperation
        .setDashboardDateTimeRealFinished(dashboard.getDashboardDateTimeRealFinished());
    dashboardMonitoringOperation
        .setDashboardDateTimeRealStarted(dashboard.getDashboardDateTimeRealStarted());
    dashboardMonitoringOperation
        .setDashboardDateTimeStartPlanned(dashboard.getDashboardDateTimeStartPlanned());
    dashboardMonitoringOperation.setDashboardDuration(dashboard.getDashboardDuration());
    dashboardMonitoringOperation.setDashboardOfId(dashboard.getDashboardOfId());
    dashboardMonitoringOperation.setDashboardLineLabel(dashboard.getDashboardLineLabel());
    dashboardMonitoringOperation
        .setDashboardMachineReference(dashboard.getDashboardMachineReference());
    dashboardMonitoringOperation.setDashboardNumCommande(dashboard.getDashboardNumCommande());
    dashboardMonitoringOperation.setDashboardPostReference(dashboard.getDashboardPostReference());
    dashboardMonitoringOperation.setDashboardProductCode(dashboard.getDashboardProductCode());
    dashboardMonitoringOperation
        .setDashboardProductCustomerCode(dashboard.getDashboardProductCustomerCode());
    dashboardMonitoringOperation.setDashboardProductLabel(dashboard.getDashboardProductLabel());
    dashboardMonitoringOperation.setDashboardQuantityGlobal(dashboard.getDashboardQuantityGlobal());
    dashboardMonitoringOperation
        .setDashboardQuantityProduced(dashboard.getDashboardQuantityProduced());
    dashboardMonitoringOperation.setDashboardQuantityNotOk(dashboard.getDashboardQuantityNotOk());
    dashboardMonitoringOperation.setDashboardStatus(dashboard.getDashboardStatus());
    dashboardMonitoringOperation.setDashboardUserLogin(dashboard.getDashboardUserLogin());
    dashboardMonitoringOperation.setDashboardUserRole(dashboard.getDashboardUserRole());
    dashboardMonitoringOperation.setDashboardProductDevise(dashboard.getDashboardProductDevise());
    dashboardMonitoringOperation.setDashboardProductPrice(dashboard.getDashboardProductPrice());
    dashboardMonitoringOperation
        .setDashboardProductTheoreticCycle(dashboard.getDashboardProductTheoreticCycle());
    dashboardMonitoringOperation.setDashboardArretUnPlanned(dashboard.getDashboardArretUnPlanned());

    return dashboardMonitoringOperation;
  }

  @Override
  public Optional<DashboardMonitoring> getDashboardMonitoringByDashboardMonitoringList(
      DashboardMonitoringDto dashboardMonitoringDto) {

    // throws IllegalArgumentException if shopFloorProdPostReference is null
    Preconditions.checkNotNull(dashboardMonitoringDto.getDashboardLineLabel(),
        "getDashboardLineLabel must not be null");
    Preconditions.checkNotNull(dashboardMonitoringDto.getDashboardPostReference(),
        "getDashboardPostReference must not be null");

    QDashboardMonitoring qfp = QDashboardMonitoring.dashboardMonitoring;

    BooleanExpression be = qfp.dashboardLineLabel.eq(dashboardMonitoringDto.getDashboardLineLabel())
        .and(qfp.dashboardPostReference.eq(dashboardMonitoringDto.getDashboardPostReference()));
    return Optional.ofNullable(dashboardMonitoringRepository.findOne(be));
  }
}
