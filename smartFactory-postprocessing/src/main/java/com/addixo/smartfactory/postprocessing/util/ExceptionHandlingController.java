
package com.addixo.smartfactory.postprocessing.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlingController {

  @ExceptionHandler(ResourceConflictException.class)
  public ResponseEntity<ExceptionResponse> resourceConflict(final ResourceConflictException ex) {

    final ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Custom Data Conflict");
    response.setErrorMessage(ex.getConflitId() + " " + ex.getMessage());
    return new ResponseEntity<>(response, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(ResourceEntityException.class)
  public ResponseEntity<ExceptionResponse> resourceInternal(final ResourceEntityException ex) {

    final ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Custom Entity Execption");
    response.setErrorMessage(ex.getMessage());
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ResourceFilesException.class)
  public ResponseEntity<ExceptionResponse> resourceFile(final ResourceFilesException ex) {

    final ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Custom File Execption ");
    response.setErrorMessage(ex.getMessage() + " " + ex.getConflitId());
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ResourceBadRequestException.class)
  public ResponseEntity<ExceptionResponse> resourceBadRequest(
      final ResourceBadRequestException ex) {

    final ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Custom Bad Request Execption ");
    response.setErrorMessage(ex.getMessage() + " " + ex.getConflitId());
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<ExceptionResponse> resourceNotFound(final ResourceNotFoundException ex) {

    final ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Custom Not Found Resource ");
    if (ex.getResourceId() != null) {
      response.setErrorMessage(ex.getMessage() + " " + ex.getResourceId());
    } else {
      response.setErrorMessage(ex.getMessage() + " " + ex.getConflitId());
    }
    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(ColorNotFoundException.class)
  public ResponseEntity<ExceptionResponse> colorNotFound(final ResourceNotFoundException ex) {

    final ExceptionResponse response = new ExceptionResponse();
    response.setErrorCode("Custom Not Found Resource ");
    if (ex.getResourceId() != null) {
      response.setErrorMessage(ex.getMessage() + " " + ex.getResourceId());
    } else {
      response.setErrorMessage(ex.getMessage() + " " + ex.getConflitId());
    }
    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
  }
}
