
package com.addixo.smartfactory.postprocessing.aspect;

import com.addixo.smartfactory.postprocessing.auth.service.UserService;
import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.domain.Of;
import com.addixo.smartfactory.postprocessing.enumeration.State;
import com.addixo.smartfactory.postprocessing.service.DashboardMonitoringService;
import com.addixo.smartfactory.postprocessing.service.LineService;
import com.addixo.smartfactory.postprocessing.service.ProductService;
import com.addixo.smartfactory.postprocessing.service.WorkShopService;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class OfServiceAspect {

  private static final Logger logger = LoggerFactory.getLogger(OfServiceAspect.class);

  @Autowired
  private DashboardMonitoringService dashboardMonitoringService;

  @Autowired
  private UserService userService;

  @Autowired
  private ProductService productService;

  @Autowired
  private WorkShopService workShopService;

  @Autowired
  private LineService lineService;

  @AfterReturning(pointcut = "execution(* com.addixo.smartfactory.postprocessing.service.OfServiceImpl.saveOrUpdateOf(..))", returning = "of")
  public void aspectOfStartMethod(final Of of) {

    logger.info("Start {}", "aspect OfStartMethod");
    logger.debug(" of {}", of);

    if (State.inprogress.equals(of.getOfStatus())) {
      logger.debug("OfStatus() {}", of.getOfStatus());
      try {
        final DashboardMonitoring dashboardMonitoring = this.mappingOfToDashbordMonitoring(of);
        this.dashboardMonitoringService.saveOrUpdateDashboardMonitoring(dashboardMonitoring);
      } catch (Exception e) {
        logger.error(e.getMessage(), e);

      }
    } else {
      logger.debug("OfStatus() {}", of.getOfStatus());
    }
    logger.info("End  {}", "aspect OfStartMethod");
  }

  /**
   * @param of
   * @return {@link DashboardMonitoring}
   * 
   */
  private DashboardMonitoring mappingOfToDashbordMonitoring(final Of of) {

    logger.info("start {}", "mappingOfToDashbordMonitoring");
    final DashboardMonitoring dashboardMonitoring = new DashboardMonitoring();
    dashboardMonitoring.setDashboardOfId(of.getOfId());

    dashboardMonitoring.setDashboardDateTimeFinishPlanned(of.getOfDateTimeFinishPlanned());
    dashboardMonitoring.setDashboardDateTimeStartPlanned(of.getOfDateTimeStartPlanned());

    dashboardMonitoring.setDashboardLineLabel(of.getOfLineLabel());

    dashboardMonitoring.setDashboardProductCode(of.getOfProductCode());
    dashboardMonitoring.setDashboardProductCustomerCode(of.getOfCustomer());

    dashboardMonitoring.setDashboardProductLabel(of.getOfProductLabel());
    dashboardMonitoring.setDashboardQuantityGlobal(of.getOfGlobalQuantity());
    dashboardMonitoring.setDashboardStatus(State.inprogress);

    this.lineService.findByLineLabel(of.getOfLineLabel()).ifPresent(line -> {
      logger.debug("line {}", line);
      this.workShopService.findWorkShopById(line.getLineWorkshopId()).ifPresent(workShop -> {
        dashboardMonitoring.setDashboardWorkShopLabel(workShop.getWorkShopLabel());
      });
    });
    this.productService.findProductByProductCode(of.getOfProductCode()).ifPresent(product -> {
      logger.debug("product {}", product);
      dashboardMonitoring.setDashboardProductTheoreticCycle(product.getProductTheoreticCycle());
      dashboardMonitoring.setDashboardProductPrice(product.getProductPrice());
      dashboardMonitoring.setDashboardProductDevise(product.getProductDevise());
    });

    this.userService.findByUsername(of.getOfResponsableTeam()).ifPresent(user -> {
      logger.debug("user {}", user);
      dashboardMonitoring.setDashboardUserRole(user.getRole().getName());
      dashboardMonitoring.setDashboardUserId(user.getId());
    });

    dashboardMonitoring.setDashboardUserLogin(of.getOfResponsableTeam());
    dashboardMonitoring.setDashboardNumCommande(of.getOfNumCommande());
    logger.debug("dashboardMonitoring() {}", dashboardMonitoring);
    logger.info("end {}", "mappingOfToDashbordMonitoring");
    return dashboardMonitoring;
  }
}
