
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Machine", columnList = "machineExternalId"))
public class Machine implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long machineId;

  private String machineExternalId;

  private String machineDescription;

  @Lob
  private String machineInformation;

  private String machineReference;

  private Long machineLineId;

  private String machineLineExternalId;

  private String machineLineLabel;

  private Long machinePostId;

  private String machinePostExternalId;

  private String machinePostReference;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getMachineId() {

    return machineId;
  }

  public void setMachineId(Long machineId) {

    this.machineId = machineId;
  }

  public String getMachineExternalId() {

    return machineExternalId;
  }

  public void setMachineExternalId(String machineExternalId) {

    this.machineExternalId = machineExternalId;
  }

  public String getMachineDescription() {

    return machineDescription;
  }

  public void setMachineDescription(String machineDescription) {

    this.machineDescription = machineDescription;
  }

  public String getMachineInformation() {

    return machineInformation;
  }

  public void setMachineInformation(String machineInformation) {

    this.machineInformation = machineInformation;
  }

  public String getMachineReference() {

    return machineReference;
  }

  public void setMachineReference(String machineReference) {

    this.machineReference = machineReference;
  }

  public Long getMachineLineId() {

    return machineLineId;
  }

  public void setMachineLineId(Long machineLineId) {

    this.machineLineId = machineLineId;
  }

  public String getMachineLineExternalId() {

    return machineLineExternalId;
  }

  public void setMachineLineExternalId(String machineLineExternalId) {

    this.machineLineExternalId = machineLineExternalId;
  }

  public String getMachineLineLabel() {

    return machineLineLabel;
  }

  public void setMachineLineLabel(String machineLineLabel) {

    this.machineLineLabel = machineLineLabel;
  }

  public Long getMachinePostId() {

    return machinePostId;
  }

  public void setMachinePostId(Long machinePostId) {

    this.machinePostId = machinePostId;
  }

  public String getMachinePostExternalId() {

    return machinePostExternalId;
  }

  public void setMachinePostExternalId(String machinePostExternalId) {

    this.machinePostExternalId = machinePostExternalId;
  }

  public String getMachinePostReference() {

    return machinePostReference;
  }

  public void setMachinePostReference(String machinePostReference) {

    this.machinePostReference = machinePostReference;
  }

  public Date getCreatedAt() {

    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Machine [machineId=" + machineId + ", machineExternalId=" + machineExternalId
        + ", machineDescription=" + machineDescription + ", machineInformation="
        + machineInformation + ", machineReference=" + machineReference + ", machineLineId="
        + machineLineId + ", machineLineExternalId=" + machineLineExternalId + ", machineLineLabel="
        + machineLineLabel + ", machinePostId=" + machinePostId + ", machinePostExternalId="
        + machinePostExternalId + ", machinePostReference=" + machinePostReference + ", createdAt="
        + createdAt + ", updatedAt=" + updatedAt + "]";
  }

}
