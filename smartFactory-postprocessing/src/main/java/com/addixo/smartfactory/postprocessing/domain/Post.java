
package com.addixo.smartfactory.postprocessing.domain;

import com.addixo.smartfactory.postprocessing.enumeration.InputOutput;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Post", columnList = "postExternalId,postLineExternalId"))
public class Post implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long postId;

  private String postExternalId;

  private String postReference;

  private Long postLineId;

  private String postLineExternalId;

  private String postLineLabel;

  private int postNbMachine;

  private String postDescription;

  @Lob
  private String postInformation;

  @Enumerated(EnumType.STRING)
  private InputOutput postType;

  @ElementCollection
  private List<String> postConfig;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getPostId() {

    return postId;
  }

  public void setPostId(Long postId) {

    this.postId = postId;
  }

  public String getPostExternalId() {

    return postExternalId;
  }

  public void setPostExternalId(String postExternalId) {

    this.postExternalId = postExternalId;
  }

  public String getPostReference() {

    return postReference;
  }

  public void setPostReference(String postReference) {

    this.postReference = postReference;
  }

  public Long getPostLineId() {

    return postLineId;
  }

  public void setPostLineId(Long postLineId) {

    this.postLineId = postLineId;
  }

  public String getPostLineExternalId() {

    return postLineExternalId;
  }

  public void setPostLineExternalId(String postLineExternalId) {

    this.postLineExternalId = postLineExternalId;
  }

  public String getPostLineLabel() {

    return postLineLabel;
  }

  public void setPostLineLabel(String postLineLabel) {

    this.postLineLabel = postLineLabel;
  }

  public int getPostNbMachine() {

    return postNbMachine;
  }

  public void setPostNbMachine(int postNbMachine) {

    this.postNbMachine = postNbMachine;
  }

  public String getPostDescription() {

    return postDescription;
  }

  public void setPostDescription(String postDescription) {

    this.postDescription = postDescription;
  }

  public String getPostInformation() {

    return postInformation;
  }

  public void setPostInformation(String postInformation) {

    this.postInformation = postInformation;
  }

  public InputOutput getPostType() {

    return postType;
  }

  public void setPostType(InputOutput postType) {

    this.postType = postType;
  }

  public List<String> getPostConfig() {

    return postConfig;
  }

  public void setPostConfig(List<String> postConfig) {

    this.postConfig = postConfig;
  }

  public Date getCreatedAt() {

    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Post [postId=" + postId + ", postExternalId=" + postExternalId + ", postReference="
        + postReference + ", postLineId=" + postLineId + ", postLineExternalId="
        + postLineExternalId + ", postLineLabel=" + postLineLabel + ", postNbMachine="
        + postNbMachine + ", postDescription=" + postDescription + ", postInformation="
        + postInformation + ", postType=" + postType + ", postConfig=" + postConfig + ", createdAt="
        + createdAt + ", updatedAt=" + updatedAt + "]";
  }

}
