
package com.addixo.smartfactory.postprocessing.util;

public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1791564636123821405L;

  private Long resourceId;

  private String conflitId;

  public ResourceNotFoundException(final String conflitId, final String message) {

    super(message);
    this.conflitId = conflitId;
  }

  public ResourceNotFoundException(final String message, final Long resourceId) {

    super(message);
    this.resourceId = resourceId;
  }

  public ResourceNotFoundException(final Long id, final String message) {

  }

  public Long getResourceId() {

    return this.resourceId;
  }

  public void setResourceId(final Long resourceId) {

    this.resourceId = resourceId;
  }

  public String getConflitId() {

    return this.conflitId;
  }

  /**
   * Gets the resource id.
   *
   * @return the resource id
   */

  public void setConflitId(final String conflitId) {

    this.conflitId = conflitId;
  }
}
