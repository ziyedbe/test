
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.Post;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

  Optional<Post> findByPostReference(String postReference);

  List<Post> findByPostLineId(Long postLineId);
}
