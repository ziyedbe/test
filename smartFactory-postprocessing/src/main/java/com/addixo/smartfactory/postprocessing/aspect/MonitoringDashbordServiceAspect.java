
package com.addixo.smartfactory.postprocessing.aspect;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.domain.HistoryDashboardMonitoring;
import com.addixo.smartfactory.postprocessing.enumeration.State;
import com.addixo.smartfactory.postprocessing.service.DashboardMonitoringService;
import com.addixo.smartfactory.postprocessing.service.HistoryDashboardMonitoringService;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MonitoringDashbordServiceAspect {

  private static final Logger logger = LoggerFactory
      .getLogger(MonitoringDashbordServiceAspect.class);

  @Autowired
  private HistoryDashboardMonitoringService historyDashboardMonitoringService;

  @Autowired
  private DashboardMonitoringService dashboardMonitoringService;

  @AfterReturning(pointcut = "execution(* com.addixo.smartfactory.postprocessing.service.DashboardMonitoringServiceImpl.updateDashboardMonitoring(..))", returning = "dashboardMonitoring")
  public void aspectDashboardMonitoringClosedMethod(final DashboardMonitoring dashboardMonitoring) {

    logger.info("Start {}", "aspect DashboardMonitoringClosedMethod");
    logger.debug(" dashboardMonitoring{}", dashboardMonitoring);
    logger.debug("DashboardStatus {}", dashboardMonitoring.getDashboardStatus());
    if (State.closed.equals(dashboardMonitoring.getDashboardStatus())) {
      try {
        HistoryDashboardMonitoring historyDashboardMonitoring = this
            .mappingDashbordMonitoringMethod(dashboardMonitoring);
        this.historyDashboardMonitoringService
            .saveOrUpdateHistoryDashboardMonitoring(historyDashboardMonitoring);

        logger.debug("saved historyDashboardMonitoring {}", historyDashboardMonitoring);
        this.dashboardMonitoringService
            .deleteDashboardMonitoring(dashboardMonitoring.getDashboardOfId());
        logger.debug("delete dashboardMonitoring {}", dashboardMonitoring);
      } catch (Exception e) {
        logger.error(e.getMessage(), e);
      }
    }
    logger.info("End  {}", "aspect OfStartMethod");
  }

  private HistoryDashboardMonitoring mappingDashbordMonitoringMethod(
      final DashboardMonitoring dashboard) {

    final HistoryDashboardMonitoring historyDash = new HistoryDashboardMonitoring();

    historyDash.setDashboardDateTimeFinishPlanned(dashboard.getDashboardDateTimeFinishPlanned());
    historyDash.setDashboardDateTimeRealFinished(dashboard.getDashboardDateTimeRealFinished());
    historyDash.setDashboardDateTimeRealStarted(dashboard.getDashboardDateTimeRealStarted());
    historyDash.setDashboardDateTimeStartPlanned(dashboard.getDashboardDateTimeStartPlanned());
    historyDash.setDashboardDuration(dashboard.getDashboardDuration());
    historyDash.setDashboardOfId(dashboard.getDashboardOfId());
    historyDash.setDashboardLineLabel(dashboard.getDashboardLineLabel());
    historyDash.setDashboardMachineReference(dashboard.getDashboardMachineReference());
    historyDash.setDashboardNumCommande(dashboard.getDashboardNumCommande());
    historyDash.setDashboardPostReference(dashboard.getDashboardPostReference());
    historyDash.setDashboardProductCode(dashboard.getDashboardProductCode());
    historyDash.setDashboardProductCustomerCode(dashboard.getDashboardProductCustomerCode());
    historyDash.setDashboardProductLabel(dashboard.getDashboardProductLabel());
    historyDash.setDashboardQuantityGlobal(dashboard.getDashboardQuantityGlobal());
    historyDash.setDashboardQuantityProduced(dashboard.getDashboardQuantityProduced());
    historyDash.setDashboardQuantityNotOk(dashboard.getDashboardQuantityNotOk());
    historyDash.setDashboardStatus(dashboard.getDashboardStatus());
    historyDash.setDashboardUserLogin(dashboard.getDashboardUserLogin());
    historyDash.setDashboardUserRole(dashboard.getDashboardUserRole());
    historyDash.setDashboardProductDevise(dashboard.getDashboardProductDevise());
    historyDash.setDashboardProductPrice(dashboard.getDashboardProductPrice());
    historyDash.setDashboardProductTheoreticCycle(dashboard.getDashboardProductTheoreticCycle());
    historyDash.setDashboardArretUnPlanned(dashboard.getDashboardArretUnPlanned());

    return historyDash;
  }
}
