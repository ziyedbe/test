
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Of;
import com.addixo.smartfactory.postprocessing.enumeration.State;
import com.addixo.smartfactory.postprocessing.repository.OfRepository;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class OfServiceImpl implements OfService {

  private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

  private static final Logger logger = LoggerFactory.getLogger(OfServiceImpl.class);

  @Autowired
  private OfRepository ofRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Of> findAllOf() {

    return this.ofRepository.findAllByOrderByOfDateTimeStartPlannedDesc();
  }

  @Override
  public Of saveOrUpdateOf(final Of of) {

    return this.ofRepository.save(of);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Of> findOfById(final String id) {

    return Optional.ofNullable(this.ofRepository.findOne(id));
  }

  @Override
  public void deleteOf(final String ofId) {

    this.ofRepository.delete(ofId);
  }

  @Override
  @Transactional(readOnly = true)

  public List<Of> findOfByStatus(final State ofStatus) {

    return this.ofRepository.findByOfStatus(ofStatus);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Of> findOfByLineLabel(final String ofLineLabel) {

    return this.ofRepository.findByOfLineLabel(ofLineLabel);
  }

}
