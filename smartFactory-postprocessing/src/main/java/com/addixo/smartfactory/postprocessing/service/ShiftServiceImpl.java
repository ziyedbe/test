
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Shift;
import com.addixo.smartfactory.postprocessing.repository.ShiftRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ShiftServiceImpl implements ShiftService {

  @Autowired
  private ShiftRepository shiftRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Shift> findAllShift() {

    return this.shiftRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public boolean isShiftExist(final Shift shift) {

    return this.findShiftById(shift.getShiftId()).isPresent();
  }

  @Override
  public Shift saveOrUpdateShift(final Shift shift) {

    return this.shiftRepository.save(shift);
  }

  @Override
  public void deleteShift(final Long id) {

    this.shiftRepository.delete(id);
  }

  @Override
  public Shift updateShift(final Shift shift) {

    return this.saveOrUpdateShift(shift);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Shift> findShiftById(final Long id) {

    return Optional.ofNullable(this.shiftRepository.findOne(id));
  }
}
