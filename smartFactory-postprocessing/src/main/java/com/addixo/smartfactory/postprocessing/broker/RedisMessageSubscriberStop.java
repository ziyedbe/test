
package com.addixo.smartfactory.postprocessing.broker;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorStop;
import com.addixo.smartfactory.postprocessing.domain.dto.ShopFloorData;
import com.addixo.smartfactory.postprocessing.enumeration.AutoManuallyType;
import com.addixo.smartfactory.postprocessing.service.ShopFloorStopService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

@Service
public class RedisMessageSubscriberStop implements MessageListener {

  private static final Logger logger = LoggerFactory.getLogger(RedisMessageSubscriberStop.class);

  private final ShopFloorStopService shopFloorStopService;

  private final Gson gson;

  public RedisMessageSubscriberStop(final ShopFloorStopService shopFloorStopService) {

    this.shopFloorStopService = shopFloorStopService;
    this.gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
  }

  @Override
  public void onMessage(final Message message, final byte[] pattern) {

    try {
      this.saveStatusMessage(message);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    logger.info("Finsh {}", "onMessage");
  }

  private void saveStatusMessage(final Message message) {

    logger.info("start {} ", "onMessage");
    final ShopFloorData shopFloorData = this.gson.fromJson(message.toString(), ShopFloorData.class);
    final ShopFloorStop shopFloorStop = new ShopFloorStop();
    shopFloorStop.setShopFloorStopId(shopFloorData.getId());
    shopFloorStop.setShopFloorStopOfId(shopFloorData.getOfId());
    shopFloorStop.setShopFloorStopMachineReference(shopFloorData.getShopFloorMachineReference());
    shopFloorStop.setShopFloorStopPostReference(shopFloorData.getShopFloorPostReference());
    shopFloorStop.setShopFloorStopLineLabel(shopFloorData.getShopFloorLineLabel());
    shopFloorStop.setShopFloorStopInsertionType(AutoManuallyType.automatic);
    shopFloorStop.setShopFloorStopStatus(shopFloorData.getData().get("status"));
    shopFloorStop.setShopFloorStopEventDate(shopFloorData.getEventDate());
    logger.debug("shopFloorStop : {} ", shopFloorStop);
    this.shopFloorStopService.saveOrUpdateShopFloorStop(shopFloorStop);
  }
}
