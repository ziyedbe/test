
package com.addixo.smartfactory.postprocessing.auth.config;

import com.addixo.smartfactory.postprocessing.auth.service.CustomUserDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
// @EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Value("${jwt.cookie}")
  private String tokenCookie;

  @Bean
  public TokenAuthenticationFilter jwtAuthenticationTokenFilter() throws Exception {

    return new TokenAuthenticationFilter();
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {

    return super.authenticationManagerBean();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {

    return new BCryptPasswordEncoder();
  }

  @Autowired
  private CustomUserDetailsService jwtUserDetailsService;

  @Autowired
  private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

  @Autowired
  private LogoutSuccess logoutSuccess;

  @Autowired
  public void configureGlobal(final AuthenticationManagerBuilder authenticationManagerBuilder)
      throws Exception {

    authenticationManagerBuilder.userDetailsService(this.jwtUserDetailsService)
        .passwordEncoder(this.passwordEncoder());
  }

  @Autowired
  private AuthenticationSuccessHandler authenticationSuccessHandler;

  @Autowired
  private AuthenticationFailureHandler authenticationFailureHandler;

  @Override
  protected void configure(final HttpSecurity http) throws Exception {

    http.cors().configurationSource(this.corsFilter());
    http.csrf().disable().authorizeRequests()
        .antMatchers("/api/login", "/api/signup", "/swagger-ui.html").permitAll().and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        .exceptionHandling().authenticationEntryPoint(this.restAuthenticationEntryPoint).and()
        .addFilterBefore(this.jwtAuthenticationTokenFilter(), BasicAuthenticationFilter.class)
        .authorizeRequests().anyRequest().authenticated().and().formLogin().loginPage("/api/login")
        .successHandler(this.authenticationSuccessHandler)
        .failureHandler(this.authenticationFailureHandler).and().logout()
        .logoutRequestMatcher(new AntPathRequestMatcher("/api/logout"))
        .logoutSuccessHandler(this.logoutSuccess).deleteCookies(this.tokenCookie);
  }

  public UrlBasedCorsConfigurationSource corsFilter() {

    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    final CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    config.addAllowedOrigin("*");
    config.addAllowedHeader("*");
    config.addAllowedMethod("GET");
    config.addAllowedMethod("PUT");
    config.addAllowedMethod("POST");
    config.addAllowedMethod("DELETE");
    config.addAllowedMethod("PATCH");
    source.registerCorsConfiguration("/**", config);
    return source;
  }

}
