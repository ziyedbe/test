
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Chart;
import com.addixo.smartfactory.postprocessing.service.ChartService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ChartController {

  @Autowired
  private ChartService chartService;

  @GetMapping("/chart")
  public List<Chart> getAllChart() {

    return this.chartService.findAllChart();
  }

  @PostMapping("/chat")
  public Chart saveChart(@RequestBody final Chart chart) {

    return this.chartService.saveChart(chart);

  }
}
