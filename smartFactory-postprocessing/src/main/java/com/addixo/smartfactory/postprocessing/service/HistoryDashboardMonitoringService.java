
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.HistoryDashboardMonitoring;

import java.util.List;
import java.util.Optional;

public interface HistoryDashboardMonitoringService {

  List<HistoryDashboardMonitoring> findAllHistoryDashboardMonitoring();

  HistoryDashboardMonitoring saveOrUpdateHistoryDashboardMonitoring(
      HistoryDashboardMonitoring historyDashboardMonitoring);

  boolean isHistoryDashboardMonitoringExist(HistoryDashboardMonitoring historyDashboardMonitoring);

  void deleteHistoryDashboardMonitoring(String id);

  Optional<HistoryDashboardMonitoring> findHistoryDashboardMonitoringById(String id);
}
