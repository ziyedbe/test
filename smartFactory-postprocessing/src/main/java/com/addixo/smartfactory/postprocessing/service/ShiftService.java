
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Shift;

import java.util.List;
import java.util.Optional;

public interface ShiftService {

  List<Shift> findAllShift();

  Shift saveOrUpdateShift(Shift shift);

  boolean isShiftExist(Shift shift);

  void deleteShift(Long id);

  Optional<Shift> findShiftById(Long id);

  Shift updateShift(Shift shift);
}
