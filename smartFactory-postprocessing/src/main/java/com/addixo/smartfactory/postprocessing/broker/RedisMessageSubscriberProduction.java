
package com.addixo.smartfactory.postprocessing.broker;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorProduction;
import com.addixo.smartfactory.postprocessing.domain.dto.ShopFloorData;
import com.addixo.smartfactory.postprocessing.enumeration.AutoManuallyType;
import com.addixo.smartfactory.postprocessing.service.ShopFloorProductionService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

@Service
public class RedisMessageSubscriberProduction implements MessageListener {

  private static final Logger logger = LoggerFactory
      .getLogger(RedisMessageSubscriberProduction.class);

  private final ShopFloorProductionService shopFloorProductionService;

  private final Gson gson;

  public RedisMessageSubscriberProduction(
      final ShopFloorProductionService monitoringProductionService) {

    this.shopFloorProductionService = monitoringProductionService;
    this.gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
  }

  @Override
  public void onMessage(final Message message, final byte[] pattern) {

    logger.info("start {} ", "onMessage");
    try {
      this.saveProductionMessage(message);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    logger.info("Finsh {}", "onMessage");
  }

  private void saveProductionMessage(final Message message) {

    logger.debug("case : {} ", "production");
    final ShopFloorData shopFloorData = this.gson.fromJson(message.toString(), ShopFloorData.class);
    final ShopFloorProduction shopProduction = new ShopFloorProduction();
    shopProduction.setShopFloorProdId(shopFloorData.getId());
    shopProduction.setShopFloorProdOfId(shopFloorData.getOfId());
    shopProduction.setShopFloorProdOpId(shopFloorData.getOpId());
    shopProduction.setShopFloorProdMachineReference(shopFloorData.getShopFloorMachineReference());
    shopProduction.setShopFloorProdPostReference(shopFloorData.getShopFloorPostReference());
    shopProduction.setShopFloorProdLineLabel(shopFloorData.getShopFloorLineLabel());
    shopProduction.setShopFloorProdQuantity(Long.valueOf(shopFloorData.getData().get("quantity")));
    shopProduction.setShopFloorProdInsertionType(AutoManuallyType.automatic);
    shopProduction.setShopFloorProdEventDate(shopFloorData.getEventDate());
    logger.debug("shopFloorProduction : {} ", shopProduction);
    this.shopFloorProductionService.saveOrUpdateShopFloorProduction(shopProduction);
  }
}
