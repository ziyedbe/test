
package com.addixo.smartfactory.postprocessing.auth.controller;

import com.addixo.smartfactory.postprocessing.auth.domain.Role;
import com.addixo.smartfactory.postprocessing.auth.service.RoleService;
import com.addixo.smartfactory.postprocessing.auth.service.UserService;
import com.addixo.smartfactory.postprocessing.util.ResourceConflictException;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RoleController {

  @Autowired
  private RoleService roleService;

  @Autowired
  private UserService userService;

  @GetMapping("/role/{roleId}")
  @PreAuthorize("hasAuthority('getRoleById')")
  public Role getRoleById(@PathVariable final Long roleId) {

    return this.roleService.findById(roleId)
        .orElseThrow(() -> new ResourceNotFoundException(roleId, "not found"));
  }

  @GetMapping("/role")
  @PreAuthorize("hasAuthority('getAllRole')")
  public List<Role> getAllRole() {

    return this.roleService.findAll();
  }

  @PostMapping("/role")
  @PreAuthorize("hasAuthority('saveRole')")
  public Role saveRole(@RequestBody final Role role) {

    this.roleService.findByName(role.getName()).ifPresent(storedRole -> {
      throw new ResourceConflictException(storedRole.getName().toString(), "found");
    });
    return this.roleService.saveOrUpdate(role);
  }

  @PutMapping("/role/{roleId}")
  @PreAuthorize("hasAuthority('updateRole')")
  public Role updateRole(@PathVariable final Long roleId, @RequestBody final Role role) {

    Role existRole = this.roleService.findById(roleId)
        .orElseThrow(() -> new ResourceNotFoundException(roleId.toString(), "not found"));
    existRole.setName(role.getName());
    existRole.setPermissions(role.getPermissions());
    return this.roleService.saveOrUpdate(existRole);
  }

  @DeleteMapping("/role/{roleId}")
  @PreAuthorize("hasAuthority('deleteRole')")
  public String deleteRole(@PathVariable final Long roleId) {

    this.roleService.findById(roleId)
        .orElseThrow(() -> new ResourceNotFoundException(roleId.toString(), "not found"));
    this.roleService.delete(roleId);
    return "deleted";
  }

  @GetMapping("/myroles")
  @PreAuthorize("hasAuthority('getMyRole')")
  public Role getMyRole() {

    String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal()
        .toString();
    return this.userService.findByUsername(username).get().getRole();

  }
}
