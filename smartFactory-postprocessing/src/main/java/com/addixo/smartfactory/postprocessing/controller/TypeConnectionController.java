
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.TypeConnection;
import com.addixo.smartfactory.postprocessing.service.TypeConnectionService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TypeConnectionController {

  /** The logger. */
  private static final Logger logger = LoggerFactory.getLogger(MachineController.class);

  /** The machine service. */
  @Autowired
  private TypeConnectionService typeConnectionService;

  @PostMapping("/typeConnection")

  public TypeConnection createTypeConnection(
      @Valid @RequestBody final TypeConnection typeConnection) {

    logger.debug("create machine : {}", typeConnection);
    return this.typeConnectionService.saveOrUpdateTypeConnection(typeConnection);
  }

  @GetMapping("/typeConnection")

  public List<TypeConnection> listAllTypeConnection() {

    logger.debug("get all typeConnection ");
    return this.typeConnectionService.findAllTypeConnection();
  }

  @DeleteMapping("/typeConnection/{id}")

  public String deleteTypeConnection(@PathVariable("id") final Long typeConnectionId) {

    logger.debug("delete machine by id {}", typeConnectionId);
    final TypeConnection typeConnection = this.typeConnectionService
        .findTypeConnectionById(typeConnectionId)
        .orElseThrow(() -> new ResourceNotFoundException(typeConnectionId.toString(), "not found"));
    this.typeConnectionService.deleteTypeConnection(typeConnection.getTypeConnectionId());
    return "deleted";
  }

  @PutMapping("/typeConnection/{id}")

  public TypeConnection updateTypeConnection(@PathVariable("id") final Long id,
      @Valid @RequestBody final TypeConnection newTypeConnection) {

    logger.debug("update typeConnection by id {}", id);
    final TypeConnection oldTypeConnection = this.typeConnectionService.findTypeConnectionById(id)
        .orElseThrow(() -> new ResourceNotFoundException("typeConnection", id));
    newTypeConnection.setTypeConnectionId(oldTypeConnection.getTypeConnectionId());
    return this.typeConnectionService.saveOrUpdateTypeConnection(newTypeConnection);
  }

  @GetMapping("/typeConnection/{id}")

  public TypeConnection getTypeConnectionById(@PathVariable("id") final Long typeConnectionId) {

    logger.debug("get typeConnection by id {}", typeConnectionId);
    return this.typeConnectionService.findTypeConnectionById(typeConnectionId)
        .orElseThrow(() -> new ResourceNotFoundException(typeConnectionId.toString(), "not found"));

  }

}
