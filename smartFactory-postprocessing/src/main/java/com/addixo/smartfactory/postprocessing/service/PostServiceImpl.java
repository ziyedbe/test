
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Post;
import com.addixo.smartfactory.postprocessing.repository.PostRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class PostServiceImpl implements PostService {

  @Autowired
  private PostRepository postRepository;

  @Autowired
  private MachineService machineService;

  @Override
  @Transactional(readOnly = true)
  public List<Post> findAllPost() {

    return this.postRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public boolean isPostExist(final Post post) {

    return this.findPostById(post.getPostId()).isPresent();
  }

  @Override
  public Post saveOrUpdatePost(final Post post) {

    return this.postRepository.save(post);
  }

  @Override
  public void deletePost(final Long postId) {

    this.findPostById(postId).ifPresent(post -> this.postRepository.delete(post));
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Post> findPostById(final Long postId) {

    return Optional.ofNullable(this.postRepository.findOne(postId));
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Post> findByPostReference(final String postRef) {

    return this.postRepository.findByPostReference(postRef);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Post> findByPostLineId(Long postLineId) {

    return postRepository.findByPostLineId(postLineId);
  }
}
