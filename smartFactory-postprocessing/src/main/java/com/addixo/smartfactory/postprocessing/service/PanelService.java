
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Panel;

import java.util.List;
import java.util.Optional;

public interface PanelService {

  List<Panel> findAllPanel();

  boolean isPanelExist(Panel panel);

  Panel saveOrUpdatePanel(Panel panel);

  void deletePanel(Long panelId);

  Optional<Panel> findPanelById(Long id);

  Optional<Panel> findByPanelReference(String panelReference);
}
