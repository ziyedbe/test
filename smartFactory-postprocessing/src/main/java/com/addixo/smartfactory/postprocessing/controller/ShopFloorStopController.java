
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorStop;
import com.addixo.smartfactory.postprocessing.domain.dto.ShopFloorRequest;
import com.addixo.smartfactory.postprocessing.service.ShopFloorStopService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ShopFloorStopController {

  private static final Logger logger = LoggerFactory.getLogger(ShopFloorStopController.class);

  @Autowired
  private ShopFloorStopService shopFloorStopService;

  @PostMapping("/shopFloorStop")

  public ShopFloorStop createShopFloorStop(@Valid @RequestBody final ShopFloorStop shopFloorStop) {

    logger.debug("create shopFloorStop : {}", shopFloorStop);
    return this.shopFloorStopService.saveOrUpdateShopFloorStop(shopFloorStop);
  }

  @GetMapping("/shopFloorStop")
  public List<ShopFloorStop> listAllShopFloorStop() {

    logger.debug("get all ShopFloorStop ");
    return this.shopFloorStopService.findAllShopFloorStop();
  }

  @DeleteMapping("/shopFloorStop/{id}")

  public String deleteShopFloorStop(@PathVariable("id") final String shopFloorStopId) {

    logger.debug("delete ShopFloorStop by id {}", shopFloorStopId);
    final ShopFloorStop shopFloorStop = this.shopFloorStopService
        .findShopFloorStopById(shopFloorStopId)
        .orElseThrow(() -> new ResourceNotFoundException(shopFloorStopId, "not found"));
    this.shopFloorStopService.deleteShopFloorStop(shopFloorStop.getShopFloorStopId());
    return "deleted";
  }

  @PutMapping("/shopFloorStop/{id}")

  public ShopFloorStop updateShopFloorStop(@PathVariable("id") final String id,
      @Valid @RequestBody final ShopFloorStop newShopFloorStop) {

    logger.debug("update ShopFloorStop by id {}", id);
    final ShopFloorStop oldShopFloorStop = this.shopFloorStopService.findShopFloorStopById(id)
        .orElseThrow(() -> new ResourceNotFoundException("shopFloorStop", id));
    newShopFloorStop.setShopFloorStopId(oldShopFloorStop.getShopFloorStopId());
    return this.shopFloorStopService.saveOrUpdateShopFloorStop(newShopFloorStop);
  }

  @GetMapping("/shopFloorStop/ByDate")

  public List<ShopFloorStop> getListShopFloorStopByDate(
      @Valid @RequestBody final ShopFloorRequest shopFloorRequestuctionRequest) {

    logger.info("get list {}", "ShopFloorStopByDate");
    logger.info("shopFloorRequestuctionRequest {}", shopFloorRequestuctionRequest);
    return this.shopFloorStopService
        .findByShopFloorStopPostReferenceAndShopFloorStopLineLabelAndShopFloorStopMachineReferenceAndShopFloorStopEventDateBetween(
            shopFloorRequestuctionRequest.getShopFloorRequestPostReference(),
            shopFloorRequestuctionRequest.getShopFloorRequestLineLabel(),
            shopFloorRequestuctionRequest.getShopFloorRequestMachineReference(),
            shopFloorRequestuctionRequest.getStartDate(),
            shopFloorRequestuctionRequest.getEndtDate());
  }

}
