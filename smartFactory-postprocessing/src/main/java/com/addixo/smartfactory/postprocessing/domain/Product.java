
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Product", columnList = "productExternalId,productCode"))
public class Product implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long productId;

  private String productExternalId;

  @Column(unique = true)
  private String productCode;

  private String productLabel;

  private String productUnit;

  private String productCustomerCode;

  private String productDevise;

  private String productPrice;

  private double productTheoreticCycle;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getProductId() {

    return this.productId;
  }

  public void setProductId(final Long productId) {

    this.productId = productId;
  }

  public String getProductExternalId() {

    return this.productExternalId;
  }

  public void setProductExternalId(final String productExternalId) {

    this.productExternalId = productExternalId;
  }

  public String getProductCode() {

    return this.productCode;
  }

  public void setProductCode(final String productCode) {

    this.productCode = productCode;
  }

  public String getProductLabel() {

    return this.productLabel;
  }

  public void setProductLabel(final String productLabel) {

    this.productLabel = productLabel;
  }

  public String getProductUnit() {

    return this.productUnit;
  }

  public void setProductUnit(final String productUnit) {

    this.productUnit = productUnit;
  }

  public String getProductCustomerCode() {

    return this.productCustomerCode;
  }

  public void setProductCustomerCode(final String productCustomerCode) {

    this.productCustomerCode = productCustomerCode;
  }

  public String getProductDevise() {

    return this.productDevise;
  }

  public void setProductDevise(final String productDevise) {

    this.productDevise = productDevise;
  }

  public String getProductPrice() {

    return this.productPrice;
  }

  public void setProductPrice(final String productPrice) {

    this.productPrice = productPrice;
  }

  public double getProductTheoreticCycle() {

    return this.productTheoreticCycle;
  }

  public void setProductTheoreticCycle(final double productTheoreticCycle) {

    this.productTheoreticCycle = productTheoreticCycle;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Product [productId=" + this.productId + ", productExternalId=" + this.productExternalId
        + ", productCode=" + this.productCode + ", productLabel=" + this.productLabel
        + ", productUnit=" + this.productUnit + ", productCustomerCode=" + this.productCustomerCode
        + ", productDevise=" + this.productDevise + ", productPrice=" + this.productPrice
        + ", productTheoreticCycle=" + this.productTheoreticCycle + ", createdAt=" + this.createdAt
        + ", updatedAt=" + this.updatedAt + "]";
  }

}
