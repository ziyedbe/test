
package com.addixo.smartfactory.postprocessing.auth.config;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class AnonAuthentication extends AbstractAuthenticationToken {

  private static final long serialVersionUID = 1L;

  public AnonAuthentication() {

    super(null);
  }

  @Override
  public Object getCredentials() {

    return null;
  }

  @Override
  public Object getPrincipal() {

    return null;
  }

  @Override
  public boolean isAuthenticated() {

    return true;
  }

  @Override
  public int hashCode() {

    return 7;
  }

  @Override
  public boolean equals(final Object obj) {

    return (this == obj) || ((obj != null) && (this.getClass() == obj.getClass()));
  }
}
