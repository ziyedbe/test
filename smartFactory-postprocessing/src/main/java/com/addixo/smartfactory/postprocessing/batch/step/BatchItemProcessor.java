
package com.addixo.smartfactory.postprocessing.batch.step;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.enumeration.QualityType;
import com.addixo.smartfactory.postprocessing.service.ShopFloorProductionService;
import com.google.common.base.Strings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class BatchItemProcessor implements ItemProcessor<DashboardMonitoring, DashboardMonitoring> {

  private static final Logger logger = LoggerFactory.getLogger(BatchItemProcessor.class);

  private final ShopFloorProductionService shopFloorProdService;

  private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

  private final DateFormat dateFormat;

  public BatchItemProcessor(final ShopFloorProductionService shopFloorProdService) {

    this.dateFormat = new SimpleDateFormat(DATE_FORMAT);
    this.shopFloorProdService = shopFloorProdService;
  }

  @Override
  public DashboardMonitoring process(final DashboardMonitoring dashboardMonitoring)
      throws Exception {

    logger.debug("Start {}", " process");
    logger.debug("dashboardMonitoringId : {}", dashboardMonitoring.getDashboardOfId());
    final Date currentDate = new Date();

    final Date currentDateTime = this.dateFormat.parse(this.dateFormat.format(currentDate));

    if (!Strings.isNullOrEmpty(dashboardMonitoring.getDashboardLineLabel())
        && !Strings.isNullOrEmpty(dashboardMonitoring.getDashboardPostReference())) {

      Long qteProduced = this.shopFloorProdService.getProducedQuantity(
          dashboardMonitoring.getDashboardOfId(),
          dashboardMonitoring.getDashboardDateTimeRealStarted(), currentDateTime, QualityType.ok);

      Long qteProducedNotOk = this.shopFloorProdService.getProducedQuantity(
          dashboardMonitoring.getDashboardOfId(),
          dashboardMonitoring.getDashboardDateTimeRealStarted(), currentDateTime,
          QualityType.notOk);

      logger.debug("qteProduced {}", qteProduced);
      logger.debug("qteProducedNotOk {}", qteProducedNotOk);
      long millisecond = currentDate.getTime()
          - dashboardMonitoring.getDashboardDateTimeRealStarted().getTime();
      double duration = TimeUnit.MILLISECONDS.toMinutes(millisecond);

      dashboardMonitoring.setDashboardDuration(duration);
      logger.debug("duration {}", duration);
      dashboardMonitoring.setDashboardQuantityProduced(qteProduced);
      dashboardMonitoring.setDashboardQuantityNotOk(qteProducedNotOk);
    } else {
      logger.debug("machine {}", " null");
    }

    logger.debug("end {}", " process");

    return dashboardMonitoring;
  }
}
