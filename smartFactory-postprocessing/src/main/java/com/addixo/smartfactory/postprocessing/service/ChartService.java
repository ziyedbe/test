
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Chart;

import java.util.List;

public interface ChartService {

  List<Chart> findAllChart();

  Chart saveChart(Chart chart);

  Chart findById(Long chartId);

}
