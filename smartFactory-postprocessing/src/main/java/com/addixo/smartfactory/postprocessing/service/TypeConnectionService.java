
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.TypeConnection;

import java.util.List;
import java.util.Optional;

public interface TypeConnectionService {

  List<TypeConnection> findAllTypeConnection();

  TypeConnection saveOrUpdateTypeConnection(TypeConnection typeConnection);

  void deleteTypeConnection(Long typeConnectionId);

  Optional<TypeConnection> findTypeConnectionById(Long id);

}
