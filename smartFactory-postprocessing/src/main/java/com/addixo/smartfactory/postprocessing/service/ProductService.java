
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Product;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

public interface ProductService {

  List<Product> findAllProduct(Pageable pageable);

  Product saveOrUpdateProduct(Product produit);

  void deleteProduct(Long produitId);

  Optional<Product> findProductById(Long id);

  Optional<Product> findProductByProductCode(String productCode);
}
