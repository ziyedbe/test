
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorStop;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ShopFloorStopService {

  List<ShopFloorStop> findAllShopFloorStop();

  ShopFloorStop saveOrUpdateShopFloorStop(ShopFloorStop shopFloorStop);

  void deleteShopFloorStop(String shopFloorStopId);

  Optional<ShopFloorStop> findShopFloorStopById(String shopFloorStopId);

  List<ShopFloorStop> findByShopFloorStopPostReferenceAndShopFloorStopLineLabelAndShopFloorStopMachineReferenceAndShopFloorStopEventDateBetween(
      String shopFloorStopPostReference, String shopFloorStopLineLabel,
      String shopFloorStopMachineReference, Date startDate, Date endtDate);

  List<ShopFloorStop> findByShopFloorStopOfIdAndShopFloorStopEventDateBetween(
      String shopFloorStopOfId, Date startDate, Date endtDate);
}
