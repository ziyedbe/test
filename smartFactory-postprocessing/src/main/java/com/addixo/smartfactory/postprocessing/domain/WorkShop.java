
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_WorkShop", columnList = "workShopExternalId,workShopLabel"))

public class WorkShop implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long workShopId;

  private String workShopExternalId;

  @NotBlank
  private String workShopLabel;

  private int workShopNbLigne;

  private String workShopDescription;

  private Long workShopOrgnizationId;

  private String workShopOrgnizationExternalId;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getWorkShopId() {

    return workShopId;
  }

  public void setWorkShopId(Long workShopId) {

    this.workShopId = workShopId;
  }

  public String getWorkShopExternalId() {

    return workShopExternalId;
  }

  public void setWorkShopExternalId(String workShopExternalId) {

    this.workShopExternalId = workShopExternalId;
  }

  public String getWorkShopLabel() {

    return workShopLabel;
  }

  public void setWorkShopLabel(String workShopLabel) {

    this.workShopLabel = workShopLabel;
  }

  public int getWorkShopNbLigne() {

    return workShopNbLigne;
  }

  public void setWorkShopNbLigne(int workShopNbLigne) {

    this.workShopNbLigne = workShopNbLigne;
  }

  public String getWorkShopDescription() {

    return workShopDescription;
  }

  public void setWorkShopDescription(String workShopDescription) {

    this.workShopDescription = workShopDescription;
  }

  public Long getWorkShopOrgnizationId() {

    return workShopOrgnizationId;
  }

  public void setWorkShopOrgnizationId(Long workShopOrgnizationId) {

    this.workShopOrgnizationId = workShopOrgnizationId;
  }

  public String getWorkShopOrgnizationExternalId() {

    return workShopOrgnizationExternalId;
  }

  public void setWorkShopOrgnizationExternalId(String workShopOrgnizationExternalId) {

    this.workShopOrgnizationExternalId = workShopOrgnizationExternalId;
  }

  public Date getCreatedAt() {

    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "WorkShop [workShopId=" + workShopId + ", workShopExternalId=" + workShopExternalId
        + ", workShopLabel=" + workShopLabel + ", workShopNbLigne=" + workShopNbLigne
        + ", workShopDescription=" + workShopDescription + ", workShopOrgnizationId="
        + workShopOrgnizationId + ", workShopOrgnizationExternalId=" + workShopOrgnizationExternalId
        + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
  }

}
