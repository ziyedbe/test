
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Of;
import com.addixo.smartfactory.postprocessing.enumeration.State;

import java.util.List;
import java.util.Optional;

public interface OfService {

  List<Of> findAllOf();

  Of saveOrUpdateOf(Of of);

  void deleteOf(String ofId);

  Optional<Of> findOfById(String id);

  List<Of> findOfByStatus(State ofStatus);

  List<Of> findOfByLineLabel(String ofLineLabel);

}
