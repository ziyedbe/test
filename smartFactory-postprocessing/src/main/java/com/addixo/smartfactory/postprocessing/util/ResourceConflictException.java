
package com.addixo.smartfactory.postprocessing.util;

public class ResourceConflictException extends RuntimeException {

  private static final long serialVersionUID = 1791564636123821405L;

  private Long resourceId;

  private String conflitId;

  public ResourceConflictException(final String conflitId, final String message) {

    super(message);
    this.conflitId = conflitId;
  }

  public Long getResourceId() {

    return this.resourceId;
  }

  public void setResourceId(final Long resourceId) {

    this.resourceId = resourceId;
  }

  public String getConflitId() {

    return this.conflitId;
  }

  public void setConflitId(final String conflitId) {

    this.conflitId = conflitId;
  }
}
