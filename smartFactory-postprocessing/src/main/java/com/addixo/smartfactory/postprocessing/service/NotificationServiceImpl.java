
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Notification;
import com.addixo.smartfactory.postprocessing.repository.NotificationRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class NotificationServiceImpl implements NotificationService {

  @Autowired
  private NotificationRepository notificationRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Notification> findAllNotification() {

    return this.notificationRepository.findAll();
  }

  @Override
  public Notification saveOrUpdateNotification(final Notification notification) {

    return this.notificationRepository.save(notification);
  }

  @Override
  public void deleteNotification(final Long id) {

    this.notificationRepository.delete(id);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Notification> findNotificationById(final Long id) {

    return Optional.ofNullable(this.notificationRepository.findOne(id));
  }
}
