
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.auth.service.UserService;
import com.addixo.smartfactory.postprocessing.domain.Of;
import com.addixo.smartfactory.postprocessing.service.OfService;
import com.addixo.smartfactory.postprocessing.service.ProductService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OfController {

  private static final Logger logger = LoggerFactory.getLogger(OfController.class);

  @Autowired
  private OfService ofService;

  @Autowired
  private ProductService productService;

  @Autowired
  private UserService userService;

  @PostMapping("/of")

  public Of createOf(@Valid @RequestBody final Of of) {

    logger.debug("create of : {}", of);
    this.productService.findProductByProductCode(of.getOfProductCode()).orElseThrow(
        () -> new ResourceNotFoundException(of.getOfProductCode(), "OfProductCode not found"));
    this.userService.findByUsername(of.getOfResponsableTeam())
        .orElseThrow(() -> new ResourceNotFoundException(of.getOfResponsableTeam(),
            "OfResponsableTeam not found"));
    return this.ofService.saveOrUpdateOf(of);
  }

  @GetMapping("/of/{ofId}")

  public Of findOfById(@PathVariable("ofId") final String ofId) {

    logger.debug("get Of by id {}", ofId);
    return this.ofService.findOfById(ofId)
        .orElseThrow(() -> new ResourceNotFoundException(ofId, "ofId not found"));
  }

  @GetMapping("/of")

  public List<Of> findAllOf() {

    logger.debug("get all {}", " Of ");
    return this.ofService.findAllOf();
  }

  @GetMapping("/of/byLine/{ofLineLabel}")

  public List<Of> findOfByLine(@Valid @PathVariable("ofLineLabel") final String ofLineLabel) {

    logger.debug("get of by line");
    return this.ofService.findOfByLineLabel(ofLineLabel);
  }

  @DeleteMapping("/of/{id}")
  public String deleteOf(@PathVariable("id") final String ofId) {

    logger.debug("delete of by id {}", ofId);
    final Of of = this.ofService.findOfById(ofId)
        .orElseThrow(() -> new ResourceNotFoundException(ofId, "not found"));
    this.ofService.deleteOf(of.getOfId());
    return "deleted";
  }

  @PutMapping("/of/{id}")
  public Of updateOf(@PathVariable("id") final String id, @Valid @RequestBody final Of newOf) {

    logger.debug("update of by id {}", id);
    final Of oldOf = this.ofService.findOfById(id)
        .orElseThrow(() -> new ResourceNotFoundException("of", id));
    newOf.setOfId(oldOf.getOfId());
    return this.ofService.saveOrUpdateOf(newOf);
  }
}
