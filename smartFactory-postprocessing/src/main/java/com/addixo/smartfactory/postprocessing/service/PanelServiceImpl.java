
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Panel;
import com.addixo.smartfactory.postprocessing.repository.PanelRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class PanelServiceImpl implements PanelService {

  @Autowired
  private PanelRepository panelRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Panel> findAllPanel() {

    return this.panelRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public boolean isPanelExist(final Panel panel) {

    return this.findPanelById(panel.getPanelId()).isPresent();
  }

  @Override
  public Panel saveOrUpdatePanel(final Panel panel) {

    return this.panelRepository.save(panel);
  }

  @Override
  public void deletePanel(final Long panelId) {

    this.panelRepository.delete(panelId);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Panel> findPanelById(final Long id) {

    return Optional.ofNullable(this.panelRepository.findOne(id));
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Panel> findByPanelReference(final String panelRef) {

    return this.panelRepository.findByPanelReference(panelRef);
  }
}