
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Machine;
import com.addixo.smartfactory.postprocessing.repository.MachineRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class MachineServiceImpl implements MachineService {

  @Autowired
  private MachineRepository machineRepository;

  @Autowired
  private LineService lineService;

  @Autowired
  private PostService postService;

  @Override
  @Transactional(readOnly = true)
  public List<Machine> findAllMachine() {

    return this.machineRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public boolean isMachineExist(final Machine machine) {

    return this.findMachineById(machine.getMachineId()).isPresent();
  }

  @Override
  public Machine saveOrUpdateMachine(final Machine machine) {

    // postService.findPostById(machine.getMachinePostId()).ifPresent(post -> {
    // lineService.findLineById(post.getPostLineId()).ifPresent(line -> {
    // machine.setMachineLineId(line.getLineId());
    // machine.setMachineLineLabel(line.getLineLabel());
    // });
    // });
    return this.machineRepository.save(machine);
  }

  @Override
  public void deleteMachine(final Long machinetId) {

    this.findMachineById(machinetId).ifPresent(machine -> this.machineRepository.delete(machine));
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Machine> findMachineById(final Long id) {

    return Optional.ofNullable(this.machineRepository.findOne(id));
  }

  @Override
  public List<Machine> findByMachineLineId(Long machineLineId) {

    return machineRepository.findByMachineLineId(machineLineId);
  }

  @Override
  public List<Machine> findByMachinePostId(Long machinePostId) {

    return machineRepository.findByMachinePostId(machinePostId);
  }

}
