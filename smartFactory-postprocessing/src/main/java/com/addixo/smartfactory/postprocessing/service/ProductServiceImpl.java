
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Product;
import com.addixo.smartfactory.postprocessing.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductRepository productRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Product> findAllProduct(final Pageable pageable) {

    return this.productRepository.findAll();
  }

  @Override
  public Product saveOrUpdateProduct(final Product product) {

    return this.productRepository.save(product);
  }

  @Override
  public void deleteProduct(final Long productId) {

    this.productRepository.delete(productId);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Product> findProductById(final Long id) {

    return Optional.ofNullable(this.productRepository.findOne(id));
  }

  @Override
  public Optional<Product> findProductByProductCode(final String productCode) {

    return this.productRepository.findByProductCode(productCode);
  }
}
