
package com.addixo.smartfactory.postprocessing.auth.service;

import com.addixo.smartfactory.postprocessing.auth.domain.Role;
import com.addixo.smartfactory.postprocessing.auth.repository.RoleRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleRepository roleRepository;

  @Override
  @Transactional(readOnly = true)
  public Optional<Role> findById(final Long id) {

    return Optional.of(this.roleRepository.findOne(id));
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Role> findByName(final String name) {

    return this.roleRepository.findByName(name);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Role> findAll() {

    return this.roleRepository.findAll();
  }

  @Override
  public Role saveOrUpdate(final Role role) {

    return this.roleRepository.save(role);
  }

  @Override
  public void delete(final Long roleId) {

    Role role = this.roleRepository.findOne(roleId);

    this.roleRepository.delete(role);
  }
}
