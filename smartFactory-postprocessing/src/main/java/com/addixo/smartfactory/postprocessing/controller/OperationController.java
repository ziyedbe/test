
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Operation;
import com.addixo.smartfactory.postprocessing.service.OperationService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class OperationController {

  private static final Logger logger = LoggerFactory.getLogger(OperationController.class);

  @Autowired
  private OperationService operationService;

  @PostMapping("/operation")

  public Operation createOperation(@Valid @RequestBody final Operation operation) {

    logger.debug("create operation : {}", operation);

    return this.operationService.saveOrUpdateOperation(operation);
  }

  @GetMapping("/operation/{id}")

  public Operation findOperationById(@PathVariable("id") final Long id) {

    logger.debug("get Operation");
    return this.operationService.findOperationById(id)
        .orElseThrow(() -> new ResourceNotFoundException(id, "not found"));
  }

  @GetMapping("/operation")

  public List<Operation> findAllOperation() {

    logger.debug("get all Operation ");
    return this.operationService.findAllOperation();
  }

  @DeleteMapping("/operation/{id}")

  public String deleteOperation(@PathVariable("id") final Long operationId) {

    logger.debug("delete operation by id {}", operationId);
    final Operation operation = this.operationService.findOperationById(operationId)
        .orElseThrow(() -> new ResourceNotFoundException(operationId, "not found"));
    this.operationService.deleteOperation(operation.getOperationId());
    return "deleted";
  }

  @PutMapping("/operation/{id}")

  public Operation updateOperation(@PathVariable("id") final Long id,
      @Valid @RequestBody final Operation newOperation) {

    logger.debug("update operation by id {}", id);
    final Operation oldOperation = this.operationService.findOperationById(id)
        .orElseThrow(() -> new ResourceNotFoundException("operation", id));
    newOperation.setOperationId(oldOperation.getOperationId());
    return this.operationService.saveOrUpdateOperation(newOperation);
  }
}
