
package com.addixo.smartfactory.postprocessing.auth.service;

import com.addixo.smartfactory.postprocessing.auth.domain.User;
import com.addixo.smartfactory.postprocessing.auth.repository.UserRepository;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

  private static final Log logger = LogFactory.getLog(CustomUserDetailsService.class);

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Override
  @Transactional(readOnly = true)
  public UserDetails loadUserByUsername(final String username) {

    return this.userRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException(
            String.format("No user found with username '%s'.", username)));
  }

  public void changePassword(final String oldPassword, final String newPassword) {

    Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
    String username = currentUser.getName();

    if (this.authenticationManager != null) {
      logger.debug("Re-authenticating user '" + username + "' for password change request.");

      this.authenticationManager
          .authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
    } else {
      logger.debug("No authentication manager set. can't change Password!");

      return;
    }

    logger.debug("Changing password for user '" + username + "'");

    User user = (User) this.loadUserByUsername(username);

    user.setPassword(this.passwordEncoder.encode(newPassword));
    this.userRepository.save(user);
  }
}
