
package com.addixo.smartfactory.postprocessing.auth.service;

import com.addixo.smartfactory.postprocessing.auth.domain.User;
import com.addixo.smartfactory.postprocessing.auth.domain.dto.UserRequest;

import java.util.List;
import java.util.Optional;

public interface UserService {

  void resetCredentials();

  Optional<User> findById(Long id);

  Optional<User> findByUsername(String username);

  List<User> findAll();

  User add(UserRequest user);

  User update(UserRequest user);

  void delete(Long userId);
}
