
package com.addixo.smartfactory.postprocessing.domain;

import com.addixo.smartfactory.postprocessing.enumeration.State;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Of", columnList = "ofExternalId,ofStatus,ofLineExternalId"))
public class Of implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private String ofId;

  private String ofExternalId;

  private String ofNumCommande;

  private String ofLineLabel;

  private Long ofLineId;

  private String ofLineExternalId;

  /** The of responsible team. */
  private String ofResponsableTeam;

  private String ofCustomer;

  private String ofProductCode;

  private String ofProductLabel;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date ofDateTimeStartPlanned;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date ofDateTimeFinishPlanned;

  private Long ofGlobalQuantity;

  @Enumerated(EnumType.STRING)
  private State ofStatus;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public String getOfId() {

    return ofId;
  }

  public void setOfId(String ofId) {

    this.ofId = ofId;
  }

  public String getOfExternalId() {

    return ofExternalId;
  }

  public void setOfExternalId(String ofExternalId) {

    this.ofExternalId = ofExternalId;
  }

  public String getOfNumCommande() {

    return ofNumCommande;
  }

  public void setOfNumCommande(String ofNumCommande) {

    this.ofNumCommande = ofNumCommande;
  }

  public String getOfLineLabel() {

    return ofLineLabel;
  }

  public void setOfLineLabel(String ofLineLabel) {

    this.ofLineLabel = ofLineLabel;
  }

  public Long getOfLineId() {

    return ofLineId;
  }

  public void setOfLineId(Long ofLineId) {

    this.ofLineId = ofLineId;
  }

  public String getOfLineExternalId() {

    return ofLineExternalId;
  }

  public void setOfLineExternalId(String ofLineExternalId) {

    this.ofLineExternalId = ofLineExternalId;
  }

  public String getOfResponsableTeam() {

    return ofResponsableTeam;
  }

  public void setOfResponsableTeam(String ofResponsableTeam) {

    this.ofResponsableTeam = ofResponsableTeam;
  }

  public String getOfCustomer() {

    return ofCustomer;
  }

  public void setOfCustomer(String ofCustomer) {

    this.ofCustomer = ofCustomer;
  }

  public String getOfProductCode() {

    return ofProductCode;
  }

  public void setOfProductCode(String ofProductCode) {

    this.ofProductCode = ofProductCode;
  }

  public String getOfProductLabel() {

    return ofProductLabel;
  }

  public void setOfProductLabel(String ofProductLabel) {

    this.ofProductLabel = ofProductLabel;
  }

  public Date getOfDateTimeStartPlanned() {

    return ofDateTimeStartPlanned;
  }

  public void setOfDateTimeStartPlanned(Date ofDateTimeStartPlanned) {

    this.ofDateTimeStartPlanned = ofDateTimeStartPlanned;
  }

  public Date getOfDateTimeFinishPlanned() {

    return ofDateTimeFinishPlanned;
  }

  public void setOfDateTimeFinishPlanned(Date ofDateTimeFinishPlanned) {

    this.ofDateTimeFinishPlanned = ofDateTimeFinishPlanned;
  }

  public Long getOfGlobalQuantity() {

    return ofGlobalQuantity;
  }

  public void setOfGlobalQuantity(Long ofGlobalQuantity) {

    this.ofGlobalQuantity = ofGlobalQuantity;
  }

  public State getOfStatus() {

    return ofStatus;
  }

  public void setOfStatus(State ofStatus) {

    this.ofStatus = ofStatus;
  }

  public Date getCreatedAt() {

    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Of [ofId=" + ofId + ", ofExternalId=" + ofExternalId + ", ofNumCommande="
        + ofNumCommande + ", ofLineLabel=" + ofLineLabel + ", ofLineId=" + ofLineId
        + ", ofLineExternalId=" + ofLineExternalId + ", ofResponsableTeam=" + ofResponsableTeam
        + ", ofCustomer=" + ofCustomer + ", ofProductCode=" + ofProductCode + ", ofProductLabel="
        + ofProductLabel + ", ofDateTimeStartPlanned=" + ofDateTimeStartPlanned
        + ", ofDateTimeFinishPlanned=" + ofDateTimeFinishPlanned + ", ofGlobalQuantity="
        + ofGlobalQuantity + ", ofStatus=" + ofStatus + ", createdAt=" + createdAt + ", updatedAt="
        + updatedAt + "]";
  }

}
