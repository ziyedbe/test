
package com.addixo.smartfactory.postprocessing.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;

public class ShopFloorRequest {

  @NotBlank
  private String shopFloorRequestLineLabel;

  @NotBlank
  private String shopFloorRequestPostReference;

  private String shopFloorRequestMachineReference;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date startDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date endtDate;

  public String getShopFloorRequestLineLabel() {

    return this.shopFloorRequestLineLabel;
  }

  public void setShopFloorRequestLineLabel(final String shopFloorRequestLineLabel) {

    this.shopFloorRequestLineLabel = shopFloorRequestLineLabel;
  }

  public String getShopFloorRequestPostReference() {

    return this.shopFloorRequestPostReference;
  }

  public void setShopFloorRequestPostReference(final String shopFloorRequestPostReference) {

    this.shopFloorRequestPostReference = shopFloorRequestPostReference;
  }

  public String getShopFloorRequestMachineReference() {

    return this.shopFloorRequestMachineReference;
  }

  public void setShopFloorRequestMachineReference(final String shopFloorRequestMachineReference) {

    this.shopFloorRequestMachineReference = shopFloorRequestMachineReference;
  }

  public Date getStartDate() {

    return this.startDate;
  }

  public void setStartDate(final Date startDate) {

    this.startDate = startDate;
  }

  public Date getEndtDate() {

    return this.endtDate;
  }

  public void setEndtDate(final Date endtDate) {

    this.endtDate = endtDate;
  }

  @Override
  public String toString() {

    return "ShopFloorRequest [shopFloorRequestLineLabel=" + this.shopFloorRequestLineLabel
        + ", shopFloorRequestPostReference=" + this.shopFloorRequestPostReference
        + ", shopFloorRequestMachineReference=" + this.shopFloorRequestMachineReference
        + ", startDate=" + this.startDate + ", endtDate=" + this.endtDate + "]";
  }
}
