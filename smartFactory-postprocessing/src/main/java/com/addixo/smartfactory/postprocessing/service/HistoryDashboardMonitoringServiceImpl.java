
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.HistoryDashboardMonitoring;
import com.addixo.smartfactory.postprocessing.repository.HistoryDashboardMonitoringRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service

public class HistoryDashboardMonitoringServiceImpl implements HistoryDashboardMonitoringService {

  @Autowired
  private HistoryDashboardMonitoringRepository dashboardMonitoringRepository;

  @Override
  @Transactional(readOnly = true)
  public List<HistoryDashboardMonitoring> findAllHistoryDashboardMonitoring() {

    return this.dashboardMonitoringRepository.findAll();
  }

  @Override
  public HistoryDashboardMonitoring saveOrUpdateHistoryDashboardMonitoring(
      final HistoryDashboardMonitoring historyDashboardMonitoring) {

    return this.dashboardMonitoringRepository.save(historyDashboardMonitoring);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<HistoryDashboardMonitoring> findHistoryDashboardMonitoringById(final String id) {

    return Optional.ofNullable(this.dashboardMonitoringRepository.findOne(id));
  }

  @Override
  public void deleteHistoryDashboardMonitoring(final String historyDashboardMonitoringId) {

    this.dashboardMonitoringRepository.delete(historyDashboardMonitoringId);
  }

  @Override
  public boolean isHistoryDashboardMonitoringExist(
      final HistoryDashboardMonitoring historyDashboardMonitoring) {

    return this.findHistoryDashboardMonitoringById(historyDashboardMonitoring.getDashboardOfId())
        .isPresent();
  }
}
