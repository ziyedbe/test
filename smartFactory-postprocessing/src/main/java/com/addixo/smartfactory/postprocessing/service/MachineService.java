
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Machine;

import java.util.List;
import java.util.Optional;

public interface MachineService {

  List<Machine> findAllMachine();

  Machine saveOrUpdateMachine(Machine machine);

  boolean isMachineExist(Machine machine);

  void deleteMachine(Long machineId);

  Optional<Machine> findMachineById(Long id);

  List<Machine> findByMachineLineId(Long machineLineId);

  List<Machine> findByMachinePostId(Long machinePostId);

}
