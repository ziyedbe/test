
package com.addixo.smartfactory.postprocessing.enumeration;

public enum State {

  suspended,

  inprogress,

  degraded,

  closed,

  stopPlanned,

  stopUnplanned
}
