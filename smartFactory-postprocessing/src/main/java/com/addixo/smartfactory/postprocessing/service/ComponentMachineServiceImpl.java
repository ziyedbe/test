
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.ComponentMachine;
import com.addixo.smartfactory.postprocessing.repository.ComponentMachineRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ComponentMachineServiceImpl implements ComponentMachineService {

  @Autowired
  private ComponentMachineRepository componentMachineRepository;

  @Override
  @Transactional(readOnly = true)
  public List<ComponentMachine> findAllComponentMachine() {

    return this.componentMachineRepository.findAll();
  }

  @Override
  public ComponentMachine saveOrUpdateComponentMachine(final ComponentMachine componentMachine) {

    return this.componentMachineRepository.save(componentMachine);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ComponentMachine> findComponentMachineById(final Long id) {

    return Optional.ofNullable(this.componentMachineRepository.findOne(id));
  }

  @Override
  public void deleteComponentMachine(final Long componentMachineId) {

    this.componentMachineRepository.delete(componentMachineId);
  }

  @Override
  public boolean isComponentMachineExist(final ComponentMachine componentMachine) {

    return this.findComponentMachineById(componentMachine.getComponentId()).isPresent();
  }

}
