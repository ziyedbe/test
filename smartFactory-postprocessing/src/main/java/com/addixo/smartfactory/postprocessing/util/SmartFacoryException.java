
package com.addixo.smartfactory.postprocessing.util;

public abstract class SmartFacoryException extends RuntimeException {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public SmartFacoryException() {

    super();
  }

  public SmartFacoryException(final String message, final Throwable cause) {

    super(message, cause);
  }

  public SmartFacoryException(final String message) {

    super(message);
  }

}
