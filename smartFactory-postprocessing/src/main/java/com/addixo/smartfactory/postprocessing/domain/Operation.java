
package com.addixo.smartfactory.postprocessing.domain;

import com.addixo.smartfactory.postprocessing.enumeration.OperationType;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Operation", columnList = "operationOfExternalId"))
public class Operation implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long operationId;

  private String operationExternalId;

  private String operationOfId;

  private String operationOfExternalId;

  private double operationDuration;

  private int operationStep;

  private String operationDescription;

  @Enumerated(EnumType.STRING)
  private OperationType operationType;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date operationDateTimeStartPlanned;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date operationDateTimeFinishPlanned;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getOperationId() {

    return operationId;
  }

  public void setOperationId(Long operationId) {

    this.operationId = operationId;
  }

  public String getOperationExternalId() {

    return operationExternalId;
  }

  public void setOperationExternalId(String operationExternalId) {

    this.operationExternalId = operationExternalId;
  }

  public String getOperationOfId() {

    return operationOfId;
  }

  public void setOperationOfId(String operationOfId) {

    this.operationOfId = operationOfId;
  }

  public String getOperationOfExternalId() {

    return operationOfExternalId;
  }

  public void setOperationOfExternalId(String operationOfExternalId) {

    this.operationOfExternalId = operationOfExternalId;
  }

  public double getOperationDuration() {

    return operationDuration;
  }

  public void setOperationDuration(double operationDuration) {

    this.operationDuration = operationDuration;
  }

  public int getOperationStep() {

    return operationStep;
  }

  public void setOperationStep(int operationStep) {

    this.operationStep = operationStep;
  }

  public String getOperationDescription() {

    return operationDescription;
  }

  public void setOperationDescription(String operationDescription) {

    this.operationDescription = operationDescription;
  }

  public OperationType getOperationType() {

    return operationType;
  }

  public void setOperationType(OperationType operationType) {

    this.operationType = operationType;
  }

  public Date getOperationDateTimeStartPlanned() {

    return operationDateTimeStartPlanned;
  }

  public void setOperationDateTimeStartPlanned(Date operationDateTimeStartPlanned) {

    this.operationDateTimeStartPlanned = operationDateTimeStartPlanned;
  }

  public Date getOperationDateTimeFinishPlanned() {

    return operationDateTimeFinishPlanned;
  }

  public void setOperationDateTimeFinishPlanned(Date operationDateTimeFinishPlanned) {

    this.operationDateTimeFinishPlanned = operationDateTimeFinishPlanned;
  }

  public Date getCreatedAt() {

    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Operation [operationId=" + operationId + ", operationExternalId=" + operationExternalId
        + ", operationOfId=" + operationOfId + ", operationOfExternalId=" + operationOfExternalId
        + ", operationDuration=" + operationDuration + ", operationStep=" + operationStep
        + ", operationDescription=" + operationDescription + ", operationType=" + operationType
        + ", operationDateTimeStartPlanned=" + operationDateTimeStartPlanned
        + ", operationDateTimeFinishPlanned=" + operationDateTimeFinishPlanned + ", createdAt="
        + createdAt + ", updatedAt=" + updatedAt + "]";
  }

}
