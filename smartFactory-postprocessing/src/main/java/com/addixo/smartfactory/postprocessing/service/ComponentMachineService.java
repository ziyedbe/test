
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.ComponentMachine;

import java.util.List;
import java.util.Optional;

public interface ComponentMachineService {

  List<ComponentMachine> findAllComponentMachine();

  ComponentMachine saveOrUpdateComponentMachine(ComponentMachine componentMachine);

  boolean isComponentMachineExist(ComponentMachine componentMachine);

  void deleteComponentMachine(Long id);

  Optional<ComponentMachine> findComponentMachineById(Long id);

}
