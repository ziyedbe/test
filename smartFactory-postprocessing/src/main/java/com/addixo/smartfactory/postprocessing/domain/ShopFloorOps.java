
package com.addixo.smartfactory.postprocessing.domain;

import com.addixo.smartfactory.postprocessing.enumeration.AutoManuallyType;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_ShopFloorOps", columnList = "shopFloorOpsOfId,shopFloorOpsEventDate"))
public class ShopFloorOps implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private String shopFloorOpsId;

  @NotBlank
  private String shopFloorOpsOfId;

  private String shopFloorOpsOpId;

  @NotBlank
  private String shopFloorOpsPostReference;

  @NotBlank
  private String shopFloorOpsLineLabel;

  private String shopFloorOpsMachineReference;

  @ElementCollection
  private Map<String, String> shopFloorOpsData;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date shopFloorOpsEventDate;

  @Enumerated(EnumType.STRING)
  private AutoManuallyType shopFloorOpsInsertionType;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public String getShopFloorOpsId() {

    return this.shopFloorOpsId;
  }

  public void setShopFloorOpsId(final String shopFloorOpsId) {

    this.shopFloorOpsId = shopFloorOpsId;
  }

  public String getShopFloorOpsOfId() {

    return this.shopFloorOpsOfId;
  }

  public void setShopFloorOpsOfId(final String shopFloorOpsOfId) {

    this.shopFloorOpsOfId = shopFloorOpsOfId;
  }

  public String getShopFloorOpsOpId() {

    return this.shopFloorOpsOpId;
  }

  public void setShopFloorOpsOpId(final String shopFloorOpsOpId) {

    this.shopFloorOpsOpId = shopFloorOpsOpId;
  }

  public String getShopFloorOpsPostReference() {

    return this.shopFloorOpsPostReference;
  }

  public void setShopFloorOpsPostReference(final String shopFloorOpsPostReference) {

    this.shopFloorOpsPostReference = shopFloorOpsPostReference;
  }

  public String getShopFloorOpsLineLabel() {

    return this.shopFloorOpsLineLabel;
  }

  public void setShopFloorOpsLineLabel(final String shopFloorOpsLineLabel) {

    this.shopFloorOpsLineLabel = shopFloorOpsLineLabel;
  }

  public String getShopFloorOpsMachineReference() {

    return this.shopFloorOpsMachineReference;
  }

  public void setShopFloorOpsMachineReference(final String shopFloorOpsMachineReference) {

    this.shopFloorOpsMachineReference = shopFloorOpsMachineReference;
  }

  public Map<String, String> getShopFloorOpsData() {

    return this.shopFloorOpsData;
  }

  public void setShopFloorOpsData(final Map<String, String> shopFloorOpsData) {

    this.shopFloorOpsData = shopFloorOpsData;
  }

  public Date getShopFloorOpsEventDate() {

    return this.shopFloorOpsEventDate;
  }

  public void setShopFloorOpsEventDate(final Date shopFloorOpsEventDate) {

    this.shopFloorOpsEventDate = shopFloorOpsEventDate;
  }

  public AutoManuallyType getShopFloorOpsInsertionType() {

    return this.shopFloorOpsInsertionType;
  }

  public void setShopFloorOpsInsertionType(final AutoManuallyType shopFloorOpsInsertionType) {

    this.shopFloorOpsInsertionType = shopFloorOpsInsertionType;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "ShopFloorOps [shopFloorOpsId=" + this.shopFloorOpsId + ", shopFloorOpsOfId="
        + this.shopFloorOpsOfId + ", shopFloorOpsOpId=" + this.shopFloorOpsOpId
        + ", shopFloorOpsPostReference=" + this.shopFloorOpsPostReference
        + ", shopFloorOpsLineLabel=" + this.shopFloorOpsLineLabel
        + ", shopFloorOpsMachineReference=" + this.shopFloorOpsMachineReference
        + ", shopFloorOpsData=" + this.shopFloorOpsData + ", shopFloorOpsEventDate="
        + this.shopFloorOpsEventDate + ", shopFloorOpsInsertionType="
        + this.shopFloorOpsInsertionType + ", createdAt=" + this.createdAt + ", updatedAt="
        + this.updatedAt + "]";
  }

}
