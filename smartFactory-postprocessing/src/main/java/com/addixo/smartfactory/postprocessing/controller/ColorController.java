
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Color;
import com.addixo.smartfactory.postprocessing.service.ColorService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ColorController {

  private static final Logger logger = LoggerFactory.getLogger(ColorController.class);

  @Autowired
  private ColorService colorService;

  @PostMapping("/color")
  @PreAuthorize("hasAuthority('saveColor')")
  public Color saveColor(@Valid @RequestBody final Color color) {

    logger.debug("create color : {}", color);
    return this.colorService.saveOrUpdateColor(color);
  }

  @GetMapping("/color")
  public List<Color> listAllColor() {

    logger.debug("get all {}", "color ");
    return this.colorService.findAllColor();
  }

  @PutMapping("/color/{id}")
  public Color updateColor(@PathVariable("id") final Long id,
      @Valid @RequestBody final Color newColor) {

    logger.debug("update color by id {}", id);
    final Color oldColor = this.colorService.findColorById(id)
        .orElseThrow(() -> new ResourceNotFoundException("color", id));
    newColor.setColorId(oldColor.getColorId());
    return this.colorService.saveOrUpdateColor(newColor);
  }

  @DeleteMapping("/color/{id}")
  public String deleteColor(@PathVariable("id") final Long colorId) {

    logger.debug("delete color by id {}", colorId);
    final Color color = this.colorService.findColorById(colorId)
        .orElseThrow(() -> new ResourceNotFoundException(colorId.toString(), "colorId not found"));
    this.colorService.deleteColor(color.getColorId());
    return "deleted";
  }

}