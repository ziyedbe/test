
package com.addixo.smartfactory.postprocessing.broker;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorOps;
import com.addixo.smartfactory.postprocessing.domain.dto.ShopFloorData;
import com.addixo.smartfactory.postprocessing.enumeration.AutoManuallyType;
import com.addixo.smartfactory.postprocessing.service.ShopFloorOpsService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

@Service
public class RedisMessageSubscriberOps implements MessageListener {

  private static final Logger logger = LoggerFactory.getLogger(RedisMessageSubscriberOps.class);

  private final ShopFloorOpsService shopFloorOpsService;

  private final Gson gson;

  public RedisMessageSubscriberOps(final ShopFloorOpsService shopFloorOpsService) {

    this.shopFloorOpsService = shopFloorOpsService;
    this.gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
  }

  @Override
  public void onMessage(final Message message, final byte[] pattern) {

    logger.info("start {} ", "onMessage");
    try {
      this.saveInformationMessage(message);
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    logger.info("Finsh {}", "onMessage");
  }

  private void saveInformationMessage(final Message message) {

    logger.debug("case :{} ", "information");
    final ShopFloorData shopFloorData = this.gson.fromJson(message.toString(), ShopFloorData.class);
    final ShopFloorOps shopFloorOps = new ShopFloorOps();
    shopFloorOps.setShopFloorOpsId(shopFloorData.getId());
    shopFloorOps.setShopFloorOpsOfId(shopFloorData.getOfId());
    shopFloorOps.setShopFloorOpsPostReference(shopFloorData.getShopFloorPostReference());
    shopFloorOps.setShopFloorOpsMachineReference(shopFloorData.getShopFloorMachineReference());
    shopFloorOps.setShopFloorOpsLineLabel(shopFloorData.getShopFloorLineLabel());
    shopFloorOps.setShopFloorOpsInsertionType(AutoManuallyType.automatic);
    shopFloorOps.setShopFloorOpsEventDate(shopFloorData.getEventDate());
    shopFloorOps.setShopFloorOpsData(shopFloorData.getData());
    this.shopFloorOpsService.saveOrUpdateShopFloorOps(shopFloorOps);
    logger.debug("machineInformation : {} ", shopFloorOps);
  }
}
