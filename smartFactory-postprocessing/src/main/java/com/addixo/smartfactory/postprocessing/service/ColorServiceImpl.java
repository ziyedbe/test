
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Color;
import com.addixo.smartfactory.postprocessing.repository.ColorRepository;
import com.addixo.smartfactory.postprocessing.util.ColorNotFoundException;
import com.google.common.base.Preconditions;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class ColorServiceImpl implements ColorService {

  @Autowired
  private ColorRepository colorRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Color> findAllColor() {

    return this.colorRepository.findAll();
  }

  @Override
  public Color saveOrUpdateColor(final Color color) {

    return this.colorRepository.save(color);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Color> findColorById(final Long id) {

    return Optional.ofNullable(this.colorRepository.findOne(id));
  }

  @Override
  public void deleteColor(final Long colorId) {

    Color color = this.get(colorId);
    this.colorRepository.delete(color);
  }

  @Override
  public boolean isColorExist(final Color color) {

    return this.findColorById(color.getColorId()).isPresent();
  }

  @Override
  public Color get(final Long id) {

    Preconditions.checkArgument(id != null);
    Color color = this.colorRepository.findOne(id);
    if (color == null) {
      throw new ColorNotFoundException(id, "");
    }
    return color;
  }
}
