
package com.addixo.smartfactory.postprocessing.domain;

import com.addixo.smartfactory.postprocessing.enumeration.AutoManuallyType;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_ShopFloorStop", columnList = "shopFloorStopOfId,shopFloorStopEventDate"))

public class ShopFloorStop implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private String shopFloorStopId;

  @NotBlank
  private String shopFloorStopOfId;

  private String shopFloorStopOpId;

  @NotBlank
  private String shopFloorStopPostReference;

  @NotBlank
  private String shopFloorStopLineLabel;

  private String shopFloorStopMachineReference;

  private String shopFloorStopStatus;

  @Lob
  private String shopFloorStopReason;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date shopFloorStopEventDate;

  @NotNull
  @Enumerated(EnumType.STRING)
  private AutoManuallyType shopFloorStopInsertionType;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public String getShopFloorStopId() {

    return this.shopFloorStopId;
  }

  public void setShopFloorStopId(final String shopFloorStopId) {

    this.shopFloorStopId = shopFloorStopId;
  }

  public String getShopFloorStopOfId() {

    return this.shopFloorStopOfId;
  }

  public void setShopFloorStopOfId(final String shopFloorStopOfId) {

    this.shopFloorStopOfId = shopFloorStopOfId;
  }

  public String getShopFloorStopOpId() {

    return this.shopFloorStopOpId;
  }

  public void setShopFloorStopOpId(final String shopFloorStopOpId) {

    this.shopFloorStopOpId = shopFloorStopOpId;
  }

  public String getShopFloorStopPostReference() {

    return this.shopFloorStopPostReference;
  }

  public void setShopFloorStopPostReference(final String shopFloorStopPostReference) {

    this.shopFloorStopPostReference = shopFloorStopPostReference;
  }

  public String getShopFloorStopLineLabel() {

    return this.shopFloorStopLineLabel;
  }

  public void setShopFloorStopLineLabel(final String shopFloorStopLineLabel) {

    this.shopFloorStopLineLabel = shopFloorStopLineLabel;
  }

  public String getShopFloorStopMachineReference() {

    return this.shopFloorStopMachineReference;
  }

  public void setShopFloorStopMachineReference(final String shopFloorStopMachineReference) {

    this.shopFloorStopMachineReference = shopFloorStopMachineReference;
  }

  public String getShopFloorStopStatus() {

    return this.shopFloorStopStatus;
  }

  public void setShopFloorStopStatus(final String shopFloorStopStatus) {

    this.shopFloorStopStatus = shopFloorStopStatus;
  }

  public String getShopFloorStopReason() {

    return this.shopFloorStopReason;
  }

  public void setShopFloorStopReason(final String shopFloorStopReason) {

    this.shopFloorStopReason = shopFloorStopReason;
  }

  public Date getShopFloorStopEventDate() {

    return this.shopFloorStopEventDate;
  }

  public void setShopFloorStopEventDate(final Date shopFloorStopEventDate) {

    this.shopFloorStopEventDate = shopFloorStopEventDate;
  }

  public AutoManuallyType getShopFloorStopInsertionType() {

    return this.shopFloorStopInsertionType;
  }

  public void setShopFloorStopInsertionType(final AutoManuallyType shopFloorStopInsertionType) {

    this.shopFloorStopInsertionType = shopFloorStopInsertionType;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "ShopFloorStop [shopFloorStopId=" + this.shopFloorStopId + ", shopFloorStopOfId="
        + this.shopFloorStopOfId + ", shopFloorStopOpId=" + this.shopFloorStopOpId
        + ", shopFloorStopPostReference=" + this.shopFloorStopPostReference
        + ", shopFloorStopLineLabel=" + this.shopFloorStopLineLabel
        + ", shopFloorStopMachineReference=" + this.shopFloorStopMachineReference
        + ", shopFloorStopStatus=" + this.shopFloorStopStatus + ", shopFloorStopReason="
        + this.shopFloorStopReason + ", shopFloorStopEventDate=" + this.shopFloorStopEventDate
        + ", shopFloorStopInsertionType=" + this.shopFloorStopInsertionType + ", createdAt="
        + this.createdAt + ", updatedAt=" + this.updatedAt + "]";
  }

}
