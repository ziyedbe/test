
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Machine;
import com.addixo.smartfactory.postprocessing.service.MachineService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MachineController {

  private static final Logger logger = LoggerFactory.getLogger(MachineController.class);

  @Autowired
  private MachineService machineService;

  @PostMapping("/machine")
  public Machine createMachine(@Valid @RequestBody final Machine machine) {

    logger.debug("create machine : {}", machine);
    return this.machineService.saveOrUpdateMachine(machine);
  }

  @GetMapping("/machine")
  public List<Machine> listAllMachine() {

    logger.debug("get all {}", " machine");
    return this.machineService.findAllMachine();
  }

  @GetMapping("/machine/machineByPost/{machinePostId}")
  public List<Machine> getmachineByPostId(@PathVariable("machinePostId") final Long machinePostId) {

    logger.debug("get all {}", " machine");
    return this.machineService.findByMachinePostId(machinePostId);
  }

  @DeleteMapping("/machine/{id}")
  public String deleteMachine(@PathVariable("id") final Long machineId) {

    logger.debug("delete machine by id {}", machineId);
    final Machine machine = this.machineService.findMachineById(machineId).orElseThrow(
        () -> new ResourceNotFoundException(machineId.toString(), "machineId not found"));
    this.machineService.deleteMachine(machine.getMachineId());
    return "deleted";
  }

  @GetMapping("/machine/{id}")
  public Machine getMachineById(@PathVariable("id") final Long machineId) {

    logger.debug("get machine by id {}", machineId);
    return this.machineService.findMachineById(machineId).orElseThrow(
        () -> new ResourceNotFoundException(machineId.toString(), "machineId not found"));
  }

  @PutMapping("/machine/{id}")
  public Machine updateMachine(@PathVariable("id") final Long id,
      @Valid @RequestBody final Machine newMachine) {

    logger.debug("update machine by id {}", id);
    final Machine oldMachine = this.machineService.findMachineById(id)
        .orElseThrow(() -> new ResourceNotFoundException(id, "machineId not found"));
    newMachine.setMachineId(oldMachine.getMachineId());
    return this.machineService.saveOrUpdateMachine(newMachine);
  }

}