/***************************************************************************
 *
 * Copyright [2018], ADDIXO All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains the property * of ADDIXO. The
 * intellectual and technical concepts contained here in are * proprietary to ADDIXO.
 *
 ***************************************************************************/

package com.addixo.smartfactory.postprocessing.auth.controller;

import com.addixo.smartfactory.postprocessing.auth.domain.User;
import com.addixo.smartfactory.postprocessing.auth.domain.dto.UserRequest;
import com.addixo.smartfactory.postprocessing.auth.service.UserService;
import com.addixo.smartfactory.postprocessing.util.ResourceConflictException;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

  private static final Logger logger = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserService userService;

  @GetMapping("/user/{userId}")
  @PreAuthorize("hasAuthority('getUserById')")
  public User loadById(@PathVariable final Long userId) {

    return this.userService.findById(userId)
        .orElseThrow(() -> new ResourceNotFoundException(userId, "not found"));
  }

  @GetMapping("/user")
  @PreAuthorize("hasAuthority('getAllUser')")
  public List<User> getAllUser() {

    return this.userService.findAll();
  }

  @GetMapping("/user/reset-credentials")
  public Map<String, String> resetCredentials() {

    this.userService.resetCredentials();
    Map<String, String> result = new HashMap<>();
    result.put("result", "success");
    return result;
  }

  @PostMapping("/signup")
  // @PreAuthorize("hasAuthority('saveUser')")
  public User addUser(@RequestBody final UserRequest userRequest) {

    logger.debug("userRequest {}", userRequest);

    this.userService.findByUsername(userRequest.getUsername()).ifPresent(user -> {
      throw new ResourceConflictException(user.getUsername(), "found");
    });
    return this.userService.add(userRequest);
  }

  @PutMapping("/user")
  @PreAuthorize("hasAuthority('updateUser')")
  public User updateUser(@RequestBody final UserRequest userRequest) {

    this.userService.findById(userRequest.getId())
        .orElseThrow(() -> new ResourceNotFoundException(userRequest.getId(), "not found"));
    return this.userService.update(userRequest);
  }

  @DeleteMapping("/user/{userId}")
  @PreAuthorize("hasAuthority('deleteUser')")
  public String deleteUser(@PathVariable final Long userId) {

    this.userService.findById(userId)
        .orElseThrow(() -> new ResourceNotFoundException(userId, "not found"));
    this.userService.delete(userId);

    return "deleted";
  }

  @GetMapping("/whoami")
  @PreAuthorize("hasAuthority('whoami')")
  public User whoami() {

    return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }
}
