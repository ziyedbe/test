
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_ComponentMachine", columnList = "componentExternalId"))
public class ComponentMachine implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long componentId;

  private String componentExternalId;

  private String componentLabel;

  private String componentDescription;

  private Long componentMachineId;

  private String componentMachineExternalId;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getComponentId() {

    return componentId;
  }

  public void setComponentId(Long componentId) {

    this.componentId = componentId;
  }

  public String getComponentExternalId() {

    return componentExternalId;
  }

  public void setComponentExternalId(String componentExternalId) {

    this.componentExternalId = componentExternalId;
  }

  public String getComponentLabel() {

    return componentLabel;
  }

  public void setComponentLabel(String componentLabel) {

    this.componentLabel = componentLabel;
  }

  public String getComponentDescription() {

    return componentDescription;
  }

  public void setComponentDescription(String componentDescription) {

    this.componentDescription = componentDescription;
  }

  public Long getComponentMachineId() {

    return componentMachineId;
  }

  public void setComponentMachineId(Long componentMachineId) {

    this.componentMachineId = componentMachineId;
  }

  public String getComponentMachineExternalId() {

    return componentMachineExternalId;
  }

  public void setComponentMachineExternalId(String componentMachineExternalId) {

    this.componentMachineExternalId = componentMachineExternalId;
  }

  public Date getCreatedAt() {

    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "ComponentMachine [componentId=" + componentId + ", componentExternalId="
        + componentExternalId + ", componentLabel=" + componentLabel + ", componentDescription="
        + componentDescription + ", componentMachineId=" + componentMachineId
        + ", componentMachineExternalId=" + componentMachineExternalId + ", createdAt=" + createdAt
        + ", updatedAt=" + updatedAt + "]";
  }

}
