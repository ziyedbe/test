
package com.addixo.smartfactory.postprocessing.util;

public class ResourceBadRequestException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private Long resourceId;

  private String conflitId;

  public ResourceBadRequestException(final Long resourceId, final String message) {

    super(message);
    this.setResourceId(resourceId);
  }

  public ResourceBadRequestException(final String conflitId, final String message) {

    super(message);
    this.setConflitId(conflitId);
  }

  public Long getResourceId() {

    return this.resourceId;
  }

  public void setResourceId(final Long resourceId) {

    this.resourceId = resourceId;
  }

  public String getConflitId() {

    return this.conflitId;
  }

  public void setConflitId(final String conflitId) {

    this.conflitId = conflitId;
  }
}
