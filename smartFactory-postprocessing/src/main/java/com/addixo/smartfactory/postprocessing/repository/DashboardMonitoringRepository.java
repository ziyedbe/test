
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardMonitoringRepository extends JpaRepository<DashboardMonitoring, String>,
    PagingAndSortingRepository<DashboardMonitoring, String>,
    QueryDslPredicateExecutor<DashboardMonitoring> {

  List<DashboardMonitoring> findByDashboardUserId(Long dashboardUserId);

  List<DashboardMonitoring> findByDashboardLineLabel(String dashboardMonitoringLine);

  List<DashboardMonitoring> findByDashboardOfId(String dashboardMonitoringOfId);

  List<DashboardMonitoring> findByDashboardWorkShopLabel(String dashboardMonitoringWorkShop);
}
