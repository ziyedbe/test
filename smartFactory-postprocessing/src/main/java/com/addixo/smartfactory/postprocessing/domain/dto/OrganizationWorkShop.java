
package com.addixo.smartfactory.postprocessing.domain.dto;

public interface OrganizationWorkShop {

  String getOrganizationName();

  String getWorkShopId();

  String getWorkShopLabel();

  String getWorkShopDescription();

  String getShiftStart();

  String getShiftEnd();
}
