
package com.addixo.smartfactory.postprocessing.auth.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class PublicController {

  @RequestMapping(method = GET, value = "/foo")
  public Map<String, String> getFoo() {

    Map<String, String> fooObj = new HashMap<>();
    fooObj.put("foo", "bar");
    return fooObj;
  }
}
