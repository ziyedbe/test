
package com.addixo.smartfactory.postprocessing.enumeration;

public enum OperationType {
  configuration, production
}
