
package com.addixo.smartfactory.postprocessing.config;

import com.addixo.smartfactory.postprocessing.broker.RedisMessageSubscriberOps;
import com.addixo.smartfactory.postprocessing.broker.RedisMessageSubscriberProduction;
import com.addixo.smartfactory.postprocessing.broker.RedisMessageSubscriberStop;
import com.addixo.smartfactory.postprocessing.service.ShopFloorOpsService;
import com.addixo.smartfactory.postprocessing.service.ShopFloorProductionService;
import com.addixo.smartfactory.postprocessing.service.ShopFloorStopService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

@Configuration
public class RedisConfiguration {

  @Value("${spring.redis.host}")
  private String redisHostName;

  @Value("${spring.redis.password}")
  private String redisPassword;

  @Value("${spring.redis.port}")
  private int redisPort;

  @Autowired
  private ShopFloorStopService shopFloorStopService;

  @Autowired
  private ShopFloorOpsService shopFloorOpsService;

  @Autowired
  private ShopFloorProductionService shopFloorProductionService;

  @Autowired
  private SimpMessageSendingOperations messagingTemplate;

  @Bean
  JedisConnectionFactory jedisConnectionFactory() throws Exception {

    final JedisConnectionFactory factory = new JedisConnectionFactory();
    factory.setHostName(this.redisHostName);
    factory.setPort(this.redisPort);
    factory.setPassword(this.redisPassword);
    factory.setUsePool(true);
    return factory;
  }

  @Bean
  MessageListenerAdapter messageListenerProduction() {

    return new MessageListenerAdapter(
        new RedisMessageSubscriberProduction(this.shopFloorProductionService));
  }

  @Bean
  MessageListenerAdapter messageListenerStatus() {

    return new MessageListenerAdapter(new RedisMessageSubscriberStop(this.shopFloorStopService));
  }

  @Bean
  MessageListenerAdapter messageListenerInformation() {

    return new MessageListenerAdapter(new RedisMessageSubscriberOps(this.shopFloorOpsService));
  }

  @Bean
  RedisMessageListenerContainer redisContainer() throws Exception {

    final RedisMessageListenerContainer container = new RedisMessageListenerContainer();
    container.setConnectionFactory(this.jedisConnectionFactory());
    container.addMessageListener(this.messageListenerProduction(), this.topicConsumerProduction());
    container.addMessageListener(this.messageListenerStatus(), this.topicConsumerStatus());
    container.addMessageListener(this.messageListenerInformation(),
        this.topicConsumerInformation());

    return container;
  }

  @Bean
  public RedisTemplate<String, Object> redisTemplate() throws Exception {

    final RedisTemplate<String, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(this.jedisConnectionFactory());
    template.setValueSerializer(new GenericToStringSerializer<>(Object.class));
    return template;
  }

  @Bean
  ChannelTopic topicConsumerProduction() {

    return new ChannelTopic(Environment.PRE_POST_PRODUCTION_TOPIC);
  }

  @Bean
  ChannelTopic topicConsumerStatus() {

    return new ChannelTopic(Environment.PRE_POST_STATUS_TOPIC);
  }

  @Bean
  ChannelTopic topicConsumerInformation() {

    return new ChannelTopic(Environment.PRE_POST_INFORMATION_TOPIC);
  }
}
