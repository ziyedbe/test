
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorProduction;
import com.addixo.smartfactory.postprocessing.domain.dto.ShopFloorProdDateAndOfIdRequest;
import com.addixo.smartfactory.postprocessing.domain.dto.ShopFloorRequest;
import com.addixo.smartfactory.postprocessing.enumeration.AutoManuallyType;
import com.addixo.smartfactory.postprocessing.service.ShopFloorProductionService;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ShopFloorProductionController {

  private static final Logger logger = LoggerFactory.getLogger(ShopFloorProductionController.class);

  @Autowired
  private ShopFloorProductionService shopFloorProductionService;

  @PostMapping("/shopFloorProduction")
  @ResponseBody
  public ShopFloorProduction createMachineProduction(
      @Valid @RequestBody final ShopFloorProduction shopFloorProduction) {

    logger.debug("create ShopFloorProduction  : {} , ", shopFloorProduction);

    shopFloorProduction.setShopFloorProdInsertionType(AutoManuallyType.manually);

    shopFloorProduction.setShopFloorProdId(UUID.randomUUID().toString());
    return this.shopFloorProductionService.saveOrUpdateShopFloorProduction(shopFloorProduction);
  }

  @GetMapping("/shopFloorProduction")
  @ResponseBody
  public List<ShopFloorProduction> getListAllShopFloorProduction() {

    logger.info("get list all {}", "shopFloorProduction ");
    return this.shopFloorProductionService.findAllShopFloorProduction();
  }

  @GetMapping("/shopFloorProduction/ByDate")
  @ResponseBody
  public List<ShopFloorProduction> getListShopFloorProductionByDate(
      @Valid @RequestBody final ShopFloorRequest shopFloorProductionRequest) {

    logger.info("get list {}", "ShopFloorProductionByDate");
    logger.info("shopFloorProductionRequest {}", shopFloorProductionRequest);
    return this.shopFloorProductionService.list(
        shopFloorProductionRequest.getShopFloorRequestPostReference(),
        shopFloorProductionRequest.getShopFloorRequestLineLabel(),
        shopFloorProductionRequest.getShopFloorRequestMachineReference(),
        shopFloorProductionRequest.getStartDate(), shopFloorProductionRequest.getEndtDate(), null);
  }

  @GetMapping("/shopFloorProduction/ByDateAndOfId")

  public List<ShopFloorProduction> getListShopFloorProductionByDateAndOfId(
      @Valid @RequestBody final ShopFloorProdDateAndOfIdRequest shopFloorProdDateAndOfIdRequest) {

    logger.info("get list {}", "ShopFloorProductionByDateAndOfId");
    logger.info("shopFloorProdDateAndOfIdRequest {}", shopFloorProdDateAndOfIdRequest);
    return this.shopFloorProductionService.list(null, null, null,
        shopFloorProdDateAndOfIdRequest.getStartDate(),
        shopFloorProdDateAndOfIdRequest.getEndtDate(),
        shopFloorProdDateAndOfIdRequest.getShopFloorProdOfId());
  }
}
