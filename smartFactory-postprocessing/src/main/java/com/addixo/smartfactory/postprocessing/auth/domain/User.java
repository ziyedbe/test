
package com.addixo.smartfactory.postprocessing.auth.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "USER")
public class User implements UserDetails, Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String userExternalId;

  @Column(name = "username", unique = true)
  private String username;

  @JsonIgnore
  @Column(name = "password")
  private String password;

  @Email
  private String email;

  @Column(name = "firstname")
  private String firstname;

  @Column(name = "lastname")
  private String lastname;

  @Lob
  private String picture;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = true)
  @JoinColumn(name = "role_id")
  private Role role;

  public Long getId() {

    return this.id;
  }

  public void setId(final Long id) {

    this.id = id;
  }

  public String getUserExternalId() {

    return this.userExternalId;
  }

  public void setUserExternalId(final String userExternalId) {

    this.userExternalId = userExternalId;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.security.core.userdetails.UserDetails#getUsername()
   */
  @Override
  public String getUsername() {

    return this.username;
  }

  public void setUsername(final String username) {

    this.username = username;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.security.core.userdetails.UserDetails#getPassword()
   */
  @Override
  public String getPassword() {

    return this.password;
  }

  public void setPassword(final String password) {

    this.password = password;
  }

  public String getFirstname() {

    return this.firstname;
  }

  public void setFirstname(final String firstname) {

    this.firstname = firstname;
  }

  public String getLastname() {

    return this.lastname;
  }

  public void setLastname(final String lastname) {

    this.lastname = lastname;
  }

  public String getPicture() {

    return this.picture;
  }

  public void setPicture(final String picture) {

    this.picture = picture;
  }

  public String getEmail() {

    return this.email;
  }

  public void setEmail(final String email) {

    this.email = email;
  }

  public void setRole(final Role role) {

    this.role = role;
  }

  @JsonIgnoreProperties("allowedOperations")
  public Role getRole() {

    return this.role;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.security.core.userdetails.UserDetails# isAccountNonExpired()
   */
  @JsonIgnore
  @Override
  public boolean isAccountNonExpired() {

    return true;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.security.core.userdetails.UserDetails# isAccountNonLocked()
   */
  @JsonIgnore
  @Override
  public boolean isAccountNonLocked() {

    return true;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.security.core.userdetails.UserDetails# isCredentialsNonExpired()
   */
  @JsonIgnore
  @Override
  public boolean isCredentialsNonExpired() {

    return true;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.security.core.userdetails.UserDetails#isEnabled()
   */
  @JsonIgnore
  @Override
  public boolean isEnabled() {

    return true;
  }

  /*
   * (non-Javadoc)
   *
   * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
   */
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {

    return new HashSet<>(this.role.getPermissions());
  }

  @Override
  public String toString() {

    return "User [id=" + this.id + ", userExternalId=" + this.userExternalId + ", username="
        + this.username + ", matricule=" + ", email=" + this.email + ", password=" + this.password
        + ", firstname=" + this.firstname + ", lastname=" + this.lastname + ", picture="
        + this.picture + ", cin=" + ", role=" + this.role + "]";
  }

}
