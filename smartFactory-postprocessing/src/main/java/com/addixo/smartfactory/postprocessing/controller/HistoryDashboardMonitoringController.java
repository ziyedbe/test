
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.HistoryDashboardMonitoring;
import com.addixo.smartfactory.postprocessing.service.HistoryDashboardMonitoringService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")

public class HistoryDashboardMonitoringController {

  private static final Logger logger = LoggerFactory
      .getLogger(HistoryDashboardMonitoringController.class);

  @Autowired
  private HistoryDashboardMonitoringService historyDashboardMonitoringService;

  @PostMapping("/historyDashboardMonitoring")
  public HistoryDashboardMonitoring createHistoryDashboardMonitoring(
      @Valid @RequestBody final HistoryDashboardMonitoring historyDashboardMonitoring) {

    logger.debug("create historyDashboardMonitoring : {}", historyDashboardMonitoring);
    return this.historyDashboardMonitoringService
        .saveOrUpdateHistoryDashboardMonitoring(historyDashboardMonitoring);
  }

  @GetMapping("/historyDashboardMonitoring")
  public List<HistoryDashboardMonitoring> listAllHistoryDashboardMonitoring() {

    logger.debug("get all historyDashboardMonitoring ");
    return this.historyDashboardMonitoringService.findAllHistoryDashboardMonitoring();
  }

  @DeleteMapping("/historyDashboardMonitoring/{id}")
  public String deleteHistoryDashboardMonitoring(
      @PathVariable("id") final String historyDashboardMonitoringId) {

    logger.debug("delete historyDashboardMonitoring by id {}", historyDashboardMonitoringId);
    final HistoryDashboardMonitoring historyDashboardMonitoring = this.historyDashboardMonitoringService
        .findHistoryDashboardMonitoringById(historyDashboardMonitoringId).orElseThrow(
            () -> new ResourceNotFoundException(historyDashboardMonitoringId, "not found"));
    this.historyDashboardMonitoringService
        .deleteHistoryDashboardMonitoring(historyDashboardMonitoring.getDashboardOfId());
    return "deleted";
  }

  @PutMapping("/historyDashboardMonitoring/{id}")
  public HistoryDashboardMonitoring updateHistoryDashboardMonitoring(
      @PathVariable("id") final String id,
      @Valid @RequestBody final HistoryDashboardMonitoring newHistoryDashboardMonitoring) {

    logger.debug("update historyDashboardMonitoring by id {}", id);
    Optional<HistoryDashboardMonitoring> oldHistoryDashboardMonitoring = this.historyDashboardMonitoringService
        .findHistoryDashboardMonitoringById(id);

    if (oldHistoryDashboardMonitoring.isPresent()) {
      newHistoryDashboardMonitoring
          .setDashboardOfId(oldHistoryDashboardMonitoring.get().getDashboardOfId());
    }

    return this.historyDashboardMonitoringService
        .saveOrUpdateHistoryDashboardMonitoring(newHistoryDashboardMonitoring);
  }
}