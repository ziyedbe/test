
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Settings;
import com.addixo.smartfactory.postprocessing.repository.SettingsRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class SettingsServiceImpl implements SettingsService {

  @Autowired
  private SettingsRepository settingsRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Settings> findAllSettings() {

    return this.settingsRepository.findAll();
  }

  @Override
  public Settings saveOrUpdateSettings(final Settings settings) {

    return this.settingsRepository.save(settings);
  }

  @Override
  public void deleteSettings(final Long id) {

    this.settingsRepository.delete(id);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Settings> findSettingsById(final Long id) {

    return Optional.ofNullable(this.settingsRepository.findOne(id));
  }
}
