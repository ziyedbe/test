
package com.addixo.smartfactory.postprocessing.auth.config;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenHelper {

  @Value("${app.name}")
  private String appName;

  @Value("${jwt.secret}")
  private String secret;

  @Value("${jwt.expires_in}")
  private int expiresIn;

  @Value("${jwt.header}")
  private String authHeader;

  @Value("${jwt.cookie}")
  private String authCookie;

  @Autowired
  private UserDetailsService userDetailsService;

  private static final SignatureAlgorithm signatureAlgo = SignatureAlgorithm.HS512;

  public String getUsernameFromToken(final String token) {

    String username;
    try {
      final Claims claims = this.getClaimsFromToken(token);
      username = claims.getSubject();
    } catch (Exception e) {
      username = null;
    }
    return username;
  }

  public String generateToken(final String username) {

    return Jwts.builder().setIssuer(this.appName).setSubject(username)
        .setIssuedAt(this.generateCurrentDate()).setExpiration(this.generateExpirationDate())
        .signWith(TokenHelper.signatureAlgo, this.secret).compact();
  }

  private Claims getClaimsFromToken(final String token) {

    Claims claims;
    try {
      claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
    } catch (Exception e) {
      claims = null;
    }
    return claims;
  }

  String generateToken(final Map<String, Object> claims) {

    return Jwts.builder().setClaims(claims).setExpiration(this.generateExpirationDate())
        .signWith(TokenHelper.signatureAlgo, this.secret).compact();
  }

  public Boolean canTokenBeRefreshed(final String token) {

    try {
      final Date expirationDate = this.getClaimsFromToken(token).getExpiration();
      return expirationDate.compareTo(this.generateCurrentDate()) > 0;
    } catch (Exception e) {
      return false;
    }
  }

  public String refreshToken(final String token) {

    String refreshedToken;
    try {
      final Claims claims = this.getClaimsFromToken(token);
      claims.setIssuedAt(this.generateCurrentDate());
      refreshedToken = this.generateToken(claims);
    } catch (Exception e) {
      refreshedToken = null;
    }
    return refreshedToken;
  }

  private long getCurrentTimeMillis() {

    return DateTime.now().getMillis();
  }

  private Date generateCurrentDate() {

    return new Date(this.getCurrentTimeMillis());
  }

  private Date generateExpirationDate() {

    return new Date(this.getCurrentTimeMillis() + (this.expiresIn * 1000));
  }

  public String getToken(final HttpServletRequest request) {

    /* Getting the token from Cookie store */
    Cookie authCookie = this.getCookieValueByName(request, this.authCookie);
    if (authCookie != null) {
      return authCookie.getValue();
    }
    /* Getting the token from Authentication header e.g Bearer your_token */
    String authHeader = request.getHeader(this.authHeader);
    if ((authHeader != null) && authHeader.startsWith("Bearer ")) {
      return authHeader.substring(7);
    }

    return null;
  }

  public Cookie getCookieValueByName(final HttpServletRequest request, final String name) {

    if (request.getCookies() == null) {
      return null;
    }
    for (int i = 0; i < request.getCookies().length; i++) {
      if (request.getCookies()[i].getName().equals(name)) {
        return request.getCookies()[i];
      }
    }
    return null;
  }
}
