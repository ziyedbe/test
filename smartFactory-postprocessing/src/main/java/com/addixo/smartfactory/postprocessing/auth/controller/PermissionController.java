
package com.addixo.smartfactory.postprocessing.auth.controller;

import com.addixo.smartfactory.postprocessing.auth.domain.Permission;
import com.addixo.smartfactory.postprocessing.auth.domain.User;
import com.addixo.smartfactory.postprocessing.auth.service.PermissionService;
import com.addixo.smartfactory.postprocessing.util.ResourceConflictException;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PermissionController {

  @Autowired
  private PermissionService permissionService;

  @GetMapping("/permission/{permissionId}")
  @PreAuthorize("hasAuthority('getPermissionById')")
  public Permission getPermissionById(@PathVariable final Long permissionId) {

    return this.permissionService.findById(permissionId)
        .orElseThrow(() -> new ResourceNotFoundException(permissionId, "permissionId not found"));
  }

  @GetMapping("/permission")
  @PreAuthorize("hasAuthority('getAllPermission')")
  public List<Permission> getAllPermission() {

    return this.permissionService.findAll();
  }

  @PostMapping("/permission")
  @PreAuthorize("hasAuthority('savePermission')")
  public Permission savePermission(@Valid @RequestBody final Permission permissionRequest) {

    this.permissionService.findByName(permissionRequest.getName()).ifPresent(permission -> {
      throw new ResourceConflictException(permissionRequest.getName(), "found");
    });
    return this.permissionService.saveOrUpdate(permissionRequest);
  }

  @PutMapping("/permission/{permissionId}")
  @PreAuthorize("hasAuthority('updatePermission')")
  public Permission updatePermission(@PathVariable final Long permissionId) {

    Permission permission = this.permissionService.findById(permissionId)
        .orElseThrow(() -> new ResourceNotFoundException(permissionId, "not found"));
    permission.setId(permissionId);
    return this.permissionService.saveOrUpdate(permission);
  }

  @DeleteMapping("/permission/{permissionId}")
  @PreAuthorize("hasAuthority('deletePermission')")
  public String deletePermission(@PathVariable final Long permissionId) {

    this.permissionService.findById(permissionId)
        .orElseThrow(() -> new ResourceNotFoundException(permissionId.toString(), "not found"));
    this.permissionService.delete(permissionId);

    return "deleted";
  }

  @GetMapping("/mypermissions")
  @PreAuthorize("hasAuthority('getMyPermission')")
  public User getMyPermission() {

    return (User) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
  }
}
