
package com.addixo.smartfactory.postprocessing.auth.domain.dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class UserRequest {

  private Long id;

  @NotBlank
  private String username;

  private String userExternalId;

  @NotBlank
  private String password;

  private String firstname;

  @Email
  private String email;

  private String lastname;

  @Lob
  private String picture;

  @NotNull
  @Enumerated(EnumType.STRING)
  private String role;

  public Long getId() {

    return id;
  }

  public void setId(Long id) {

    this.id = id;
  }

  public String getUsername() {

    return username;
  }

  public void setUsername(String username) {

    this.username = username;
  }

  public String getUserExternalId() {

    return userExternalId;
  }

  public void setUserExternalId(String userExternalId) {

    this.userExternalId = userExternalId;
  }

  public String getPassword() {

    return password;
  }

  public void setPassword(String password) {

    this.password = password;
  }

  public String getFirstname() {

    return firstname;
  }

  public void setFirstname(String firstname) {

    this.firstname = firstname;
  }

  public String getEmail() {

    return email;
  }

  public void setEmail(String email) {

    this.email = email;
  }

  public String getLastname() {

    return lastname;
  }

  public void setLastname(String lastname) {

    this.lastname = lastname;
  }

  public String getPicture() {

    return picture;
  }

  public void setPicture(String picture) {

    this.picture = picture;
  }

  public String getRole() {

    return role;
  }

  public void setRole(String role) {

    this.role = role;
  }

  @Override
  public String toString() {

    return "UserRequest [id=" + id + ", username=" + username + ", userExternalId=" + userExternalId
        + ", password=" + password + ", firstname=" + firstname + ", email=" + email + ", lastname="
        + lastname + ", picture=" + picture + ", role=" + role + "]";
  }

}
