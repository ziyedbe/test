
package com.addixo.smartfactory.postprocessing.util;

public class ColorNotFoundException extends SmartFacoryException {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public ColorNotFoundException(final Long id, final String message) {

    super("Color with indetifier is not found" + id + " " + message);
  }

}
