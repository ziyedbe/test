
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Organization;

import java.util.List;
import java.util.Optional;

public interface OrganizationService {

  List<Organization> findAllOrganization();

  Organization saveOrUpdateOrganization(Organization organisation);

  void deleteOrganization(Long organizationId);

  Optional<Organization> findOrganizationById(Long id);
}
