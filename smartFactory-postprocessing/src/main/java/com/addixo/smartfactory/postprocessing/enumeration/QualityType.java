/**
 *
 */

package com.addixo.smartfactory.postprocessing.enumeration;

/**
 * @author ameur.drira
 * @date 13 juil. 2018
 */
public enum QualityType {
  ok, notOk
}
