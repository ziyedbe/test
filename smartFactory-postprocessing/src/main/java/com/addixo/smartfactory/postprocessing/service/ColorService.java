
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Color;
import com.addixo.smartfactory.postprocessing.util.ColorNotFoundException;

import java.util.List;
import java.util.Optional;

public interface ColorService {

  /**
   * Get {@link Color} bu unique identifier
   * @param id the unique identifier of Color
   * @return {@link Color}
   * @throws IllegalArgumentException if <code>id<code> is null
   * @throws ColorNotFoundException if no color was found
   */
  Color get(Long id);

  List<Color> findAllColor();

  Color saveOrUpdateColor(Color color);

  boolean isColorExist(Color color);

  void deleteColor(Long id);

  Optional<Color> findColorById(Long id);
}
