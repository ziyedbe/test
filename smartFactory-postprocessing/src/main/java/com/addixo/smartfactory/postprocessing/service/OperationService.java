
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Operation;

import java.util.List;
import java.util.Optional;

public interface OperationService {

  List<Operation> findAllOperation();

  Operation saveOrUpdateOperation(Operation operation);

  void deleteOperation(Long operationId);

  Optional<Operation> findOperationById(Long id);

  List<Operation> findByOperationOfId(String operationOfId);

}
