
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Notification", columnList = "notificationSender,notificationReceiver"))
public class Notification implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long notificationId;

  @Lob
  private String notificationBody;

  private String notificationType;

  @NotNull
  private Long notificationSender;

  @NotNull
  private Long notificationReceiver;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getNotificationId() {

    return this.notificationId;
  }

  public void setNotificationId(final Long notificationId) {

    this.notificationId = notificationId;
  }

  public String getNotificationBody() {

    return this.notificationBody;
  }

  public void setNotificationBody(final String notificationBody) {

    this.notificationBody = notificationBody;
  }

  public String getNotificationType() {

    return this.notificationType;
  }

  public void setNotificationType(final String notificationType) {

    this.notificationType = notificationType;
  }

  public Long getNotificationSender() {

    return this.notificationSender;
  }

  public void setNotificationSender(final Long notificationSender) {

    this.notificationSender = notificationSender;
  }

  public Long getNotificationReceiver() {

    return this.notificationReceiver;
  }

  public void setNotificationReceiver(final Long notificationReceiver) {

    this.notificationReceiver = notificationReceiver;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Notification [notificationId=" + this.notificationId + ", notificationBody="
        + this.notificationBody + ", notificationType=" + this.notificationType
        + ", notificationSender=" + this.notificationSender + ", notificationReceiver="
        + this.notificationReceiver + ", createdAt=" + this.createdAt + ", updatedAt="
        + this.updatedAt + "]";
  }

}
