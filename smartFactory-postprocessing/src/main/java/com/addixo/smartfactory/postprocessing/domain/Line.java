
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Line", columnList = "lineExternalId,lineWorkshopId,lineLabel"))
public class Line implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long lineId;

  private String lineExternalId;

  private Long lineWorkshopId;

  private String lineWorkshopExternalId;

  private String lineLabel;

  private int lineNbPost;

  private String lineDescription;

  private String lineType;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getLineId() {

    return lineId;
  }

  public void setLineId(Long lineId) {

    this.lineId = lineId;
  }

  public String getLineExternalId() {

    return lineExternalId;
  }

  public void setLineExternalId(String lineExternalId) {

    this.lineExternalId = lineExternalId;
  }

  public Long getLineWorkshopId() {

    return lineWorkshopId;
  }

  public void setLineWorkshopId(Long lineWorkshopId) {

    this.lineWorkshopId = lineWorkshopId;
  }

  public String getLineWorkshopExternalId() {

    return lineWorkshopExternalId;
  }

  public void setLineWorkshopExternalId(String lineWorkshopExternalId) {

    this.lineWorkshopExternalId = lineWorkshopExternalId;
  }

  public String getLineLabel() {

    return lineLabel;
  }

  public void setLineLabel(String lineLabel) {

    this.lineLabel = lineLabel;
  }

  public int getLineNbPost() {

    return lineNbPost;
  }

  public void setLineNbPost(int lineNbPost) {

    this.lineNbPost = lineNbPost;
  }

  public String getLineDescription() {

    return lineDescription;
  }

  public void setLineDescription(String lineDescription) {

    this.lineDescription = lineDescription;
  }

  public String getLineType() {

    return lineType;
  }

  public void setLineType(String lineType) {

    this.lineType = lineType;
  }

  public Date getCreatedAt() {

    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Line [lineId=" + lineId + ", lineExternalId=" + lineExternalId + ", lineWorkshopId="
        + lineWorkshopId + ", lineWorkshopExternalId=" + lineWorkshopExternalId + ", lineLabel="
        + lineLabel + ", lineNbPost=" + lineNbPost + ", lineDescription=" + lineDescription
        + ", lineType=" + lineType + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
  }

}
