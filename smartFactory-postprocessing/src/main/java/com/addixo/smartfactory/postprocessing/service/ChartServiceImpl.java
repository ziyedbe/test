
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Chart;
import com.addixo.smartfactory.postprocessing.repository.ChartRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ChartServiceImpl implements ChartService {

  @Autowired
  private ChartRepository chartRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Chart> findAllChart() {

    return this.chartRepository.findAll();
  }

  @Override
  public Chart saveChart(final Chart chart) {

    return this.chartRepository.save(chart);
  }

  @Override
  @Transactional(readOnly = true)
  public Chart findById(final Long chartId) {

    return this.chartRepository.findById(chartId);
  }

}
