
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Post;

import java.util.List;
import java.util.Optional;

public interface PostService {

  List<Post> findAllPost();

  Post saveOrUpdatePost(Post post);

  boolean isPostExist(Post post);

  void deletePost(Long postId);

  Optional<Post> findPostById(Long postId);

  Optional<Post> findByPostReference(String postReference);

  List<Post> findByPostLineId(Long postLineId);
}
