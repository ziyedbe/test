
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Line;
import com.addixo.smartfactory.postprocessing.repository.LineRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class LineServiceImpl implements LineService {

  @Autowired
  private LineRepository lineRepository;

  @Autowired
  private PostService postService;

  @Autowired
  private MachineService machineService;

  @Override
  @Transactional(readOnly = true)
  public List<Line> findAllLine() {

    return this.lineRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public boolean isLineExist(final Line line) {

    return this.findLineById(line.getLineId()).isPresent();
  }

  @Override
  public void deleteLine(final Long lineId) {

    this.findLineById(lineId).ifPresent(line -> {
      postService.findByPostLineId(line.getLineId()).forEach(post -> {
        postService.deletePost(post.getPostId());
      });
      machineService.findByMachineLineId(line.getLineId()).forEach(machine -> {
        machineService.deleteMachine(machine.getMachineId());
      });

      this.lineRepository.delete(line);
    });

  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Line> findLineById(final Long id) {

    return Optional.ofNullable(this.lineRepository.findOne(id));
  }

  @Override
  @Transactional(readOnly = true)

  public List<Line> findLineByLineWorkshopId(final Long lineWorkshopId) {

    return this.lineRepository.findByLineWorkshopId(lineWorkshopId);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Line> findByLineLabel(final String lineLabel) {

    return this.lineRepository.findByLineLabel(lineLabel);
  }

  @Override
  public Line saveOrUpdateLine(final Line line) {

    return this.lineRepository.save(line);
  }

}
