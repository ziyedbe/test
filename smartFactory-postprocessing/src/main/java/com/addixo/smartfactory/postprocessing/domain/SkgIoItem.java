
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(indexes = @Index(name = "IDX_SkgIoItem", columnList = "skgIoItemEntrer"))
public class SkgIoItem implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long skgIoItemId;

  @Column(unique = true)
  private String skgIoItemEntrer;

  private String skgIoItemDescription;

  private String skgIoItemSeuil;

  private String skgIoItemType;

  public Long getSkgIoItemId() {

    return this.skgIoItemId;
  }

  public void setSkgIoItemId(final Long skgIoItemId) {

    this.skgIoItemId = skgIoItemId;
  }

  public String getSkgIoItemEntrer() {

    return this.skgIoItemEntrer;
  }

  public void setSkgIoItemEntrer(final String skgIoItemEntrer) {

    this.skgIoItemEntrer = skgIoItemEntrer;
  }

  public String getSkgIoItemDescription() {

    return this.skgIoItemDescription;
  }

  public void setSkgIoItemDescription(final String skgIoItemDescription) {

    this.skgIoItemDescription = skgIoItemDescription;
  }

  public String getSkgIoItemSeuil() {

    return this.skgIoItemSeuil;
  }

  public void setSkgIoItemSeuil(final String skgIoItemSeuil) {

    this.skgIoItemSeuil = skgIoItemSeuil;
  }

  public String getSkgIoItemType() {

    return this.skgIoItemType;
  }

  public void setSkgIoItemType(final String skgIoItemType) {

    this.skgIoItemType = skgIoItemType;
  }

  @Override
  public String toString() {

    return "SkgIoItem [skgIoItemId=" + this.skgIoItemId + ", skgIoItemEntrer="
        + this.skgIoItemEntrer + ", skgIoItemDescription=" + this.skgIoItemDescription
        + ", skgIoItemSeuil=" + this.skgIoItemSeuil + ", skgIoItemType=" + this.skgIoItemType + "]";
  }

}
