
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Customer;
import com.addixo.smartfactory.postprocessing.repository.CustomerRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Customer> findAllCustomer() {

    return this.customerRepository.findAll();
  }

  @Override
  public Customer saveOrUpdateCustomer(final Customer customer) {

    return this.customerRepository.save(customer);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Customer> findCustomerById(final Long id) {

    return Optional.ofNullable(this.customerRepository.findOne(id));
  }

  @Override
  public void deleteCustomer(final Long customerId) {

    this.customerRepository.delete(customerId);
  }

  @Override
  public boolean isCustomerExist(final Customer customer) {

    return this.findCustomerById(customer.getCustomerId()).isPresent();
  }
}
