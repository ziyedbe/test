
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorOps;
import com.addixo.smartfactory.postprocessing.domain.dto.ShopFloorRequest;
import com.addixo.smartfactory.postprocessing.service.ShopFloorOpsService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ShopFloorOpsController {

  private static final Logger logger = LoggerFactory.getLogger(ShopFloorOpsController.class);

  @Autowired
  private ShopFloorOpsService shopFloorOpsService;

  @PostMapping("/shopFloorOps")

  public ShopFloorOps createShopFloorOps(@Valid @RequestBody final ShopFloorOps shopFloorOps) {

    logger.debug("create shopFloorOps : {}", shopFloorOps);
    return this.shopFloorOpsService.saveOrUpdateShopFloorOps(shopFloorOps);
  }

  @GetMapping("/shopFloorOps")

  public List<ShopFloorOps> listAllShopFloorOps() {

    logger.debug("get all shopFloorOps ");
    return this.shopFloorOpsService.findAllShopFloorOps();
  }

  @DeleteMapping("/shopFloorOps/{id}")
  public String deleteShopFloorOps(@PathVariable("id") final String shopFloorOpsId) {

    logger.debug("delete shopFloorOps by id {}", shopFloorOpsId);
    final ShopFloorOps shopFloorOps = this.shopFloorOpsService.findShopFloorOpsById(shopFloorOpsId)
        .orElseThrow(() -> new ResourceNotFoundException(shopFloorOpsId.toString(), "not found"));
    this.shopFloorOpsService.deleteShopFloorOps(shopFloorOps.getShopFloorOpsId());
    return "deleted";
  }

  @PutMapping("/shopFloorOps/{id}")
  public ShopFloorOps updateShopFloorOps(@PathVariable("id") final String id,
      @Valid @RequestBody final ShopFloorOps newShopFloorOps) {

    logger.debug("update shopFloorOps by id {}", id);
    final ShopFloorOps oldShopFloorOps = this.shopFloorOpsService.findShopFloorOpsById(id)
        .orElseThrow(() -> new ResourceNotFoundException("shopFloorOps", id));
    newShopFloorOps.setShopFloorOpsId(oldShopFloorOps.getShopFloorOpsId());
    return this.shopFloorOpsService.saveOrUpdateShopFloorOps(newShopFloorOps);
  }

  @GetMapping("/shopFloorOps/ByDate")
  public List<ShopFloorOps> getListShopFloorOpsByDate(
      @Valid @RequestBody final ShopFloorRequest shopFloorOpsRequest) {

    logger.info("get list {}", "ShopFloorOpsByDate");
    logger.info("shopFloorOpsRequest {}", shopFloorOpsRequest);
    return this.shopFloorOpsService
        .findByShopFloorOpsPostReferenceAndShopFloorOpsLineLabelAndShopFloorOpsMachineReferenceAndShopFloorOpsEventDateBetween(
            shopFloorOpsRequest.getShopFloorRequestPostReference(),
            shopFloorOpsRequest.getShopFloorRequestLineLabel(),
            shopFloorOpsRequest.getShopFloorRequestMachineReference(),
            shopFloorOpsRequest.getStartDate(), shopFloorOpsRequest.getEndtDate());
  }

}
