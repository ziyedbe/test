
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Line;

import java.util.List;
import java.util.Optional;

public interface LineService {

  List<Line> findAllLine();

  Line saveOrUpdateLine(Line line);

  boolean isLineExist(Line line);

  void deleteLine(Long lineId);

  Optional<Line> findLineById(Long id);

  Optional<Line> findByLineLabel(String lineLabel);

  List<Line> findLineByLineWorkshopId(Long lineWorkshopId);
}
