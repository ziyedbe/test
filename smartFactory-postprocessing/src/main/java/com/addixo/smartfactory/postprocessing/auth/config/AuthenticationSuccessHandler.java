
package com.addixo.smartfactory.postprocessing.auth.config;

import com.addixo.smartfactory.postprocessing.auth.domain.User;
import com.addixo.smartfactory.postprocessing.auth.domain.dto.UserTokenState;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

  @Value("${jwt.expires_in}")
  private int expiresIn;

  @Value("${jwt.cookie}")
  private String tokenCookie;

  @Autowired
  TokenHelper tokenHelper;

  @Autowired
  ObjectMapper objectMapper;

  @Override
  public void onAuthenticationSuccess(final HttpServletRequest request,
      final HttpServletResponse response, final Authentication authentication)
      throws IOException, ServletException {

    this.clearAuthenticationAttributes(request);
    User user = (User) authentication.getPrincipal();

    String jws = this.tokenHelper.generateToken(user.getUsername());

    Cookie authCookie = new Cookie(this.tokenCookie, jws);

    authCookie.setHttpOnly(true);

    authCookie.setMaxAge(this.expiresIn);

    authCookie.setPath("/");
    response.addCookie(authCookie);

    UserTokenState userTokenState = new UserTokenState(jws, this.expiresIn);
    String jwtResponse = this.objectMapper.writeValueAsString(userTokenState);
    response.setContentType("application/json");
    response.getWriter().write(jwtResponse);
  }
}
