
package com.addixo.smartfactory.postprocessing.auth.service;

import com.addixo.smartfactory.postprocessing.auth.domain.User;
import com.addixo.smartfactory.postprocessing.auth.domain.dto.UserRequest;
import com.addixo.smartfactory.postprocessing.auth.repository.UserRepository;
import com.google.common.base.Strings;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

  private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleService roleService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  public void resetCredentials() {

    List<User> users = this.userRepository.findAll();
    for (User user : users) {
      user.setPassword(this.passwordEncoder.encode("123"));
      this.userRepository.save(user);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<User> findByUsername(final String username) {

    return this.userRepository.findByUsername(username);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<User> findById(final Long id) {

    return Optional.of(this.userRepository.findOne(id));
  }

  @Override
  @Transactional(readOnly = true)
  public List<User> findAll() {

    return this.userRepository.findAll();
  }

  @Override
  public User add(final UserRequest userRequest) {

    logger.info("add user : {}", userRequest);
    final User user = new User();
    user.setUsername(userRequest.getUsername());
    user.setEmail(userRequest.getEmail());
    user.setFirstname(userRequest.getFirstname());
    user.setLastname(userRequest.getLastname());
    user.setPicture(userRequest.getPicture());
    if (!Strings.isNullOrEmpty(userRequest.getPassword())) {
      user.setPassword(this.passwordEncoder.encode(userRequest.getPassword()));
    } else {
      this.findByUsername(userRequest.getUsername())
          .ifPresent(oldUser -> user.setPassword(oldUser.getPassword()));
    }

    this.roleService.findByName(userRequest.getRole()).ifPresent(role -> user.setRole(role));
    return this.userRepository.save(user);
  }

  @Override
  public User update(final UserRequest userRequest) {

    return this.add(userRequest);
  }

  @Override
  public void delete(final Long userId) {

    final User user = this.userRepository.findOne(userId);
    this.userRepository.delete(user);
  }
}
