
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

  List<Customer> findAllCustomer();

  Customer saveOrUpdateCustomer(Customer customer);

  boolean isCustomerExist(Customer customer);

  void deleteCustomer(Long id);

  Optional<Customer> findCustomerById(Long id);
}
