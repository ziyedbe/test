
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Customer", columnList = "customerExternalId,customerEmail"))
public class Customer implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long customerId;

  private String customerExternalId;

  private String customerFirstName;

  @NotBlank
  private String customerCode;

  private String customerLastName;

  private String customerCompanyName;

  private String customerAdresse;

  private String customerCity;

  private String customerCountry;

  private String customerPhone;

  private String customerRegistrationNumber;

  @Column(unique = true)
  @Email
  private String customerEmail;

  private String customerDescription;

  private String customerSectorActivity;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getCustomerId() {

    return this.customerId;
  }

  public void setCustomerId(final Long customerId) {

    this.customerId = customerId;
  }

  public String getCustomerExternalId() {

    return this.customerExternalId;
  }

  public void setCustomerExternalId(final String customerExternalId) {

    this.customerExternalId = customerExternalId;
  }

  public String getCustomerFirstName() {

    return this.customerFirstName;
  }

  public void setCustomerFirstName(final String customerFirstName) {

    this.customerFirstName = customerFirstName;
  }

  public String getCustomerCode() {

    return this.customerCode;
  }

  public void setCustomerCode(final String customerCode) {

    this.customerCode = customerCode;
  }

  public String getCustomerLastName() {

    return this.customerLastName;
  }

  public void setCustomerLastName(final String customerLastName) {

    this.customerLastName = customerLastName;
  }

  public String getCustomerCompanyName() {

    return this.customerCompanyName;
  }

  public void setCustomerCompanyName(final String customerCompanyName) {

    this.customerCompanyName = customerCompanyName;
  }

  public String getCustomerAdresse() {

    return this.customerAdresse;
  }

  public void setCustomerAdresse(final String customerAdresse) {

    this.customerAdresse = customerAdresse;
  }

  public String getCustomerCity() {

    return this.customerCity;
  }

  public void setCustomerCity(final String customerCity) {

    this.customerCity = customerCity;
  }

  public String getCustomerCountry() {

    return this.customerCountry;
  }

  public void setCustomerCountry(final String customerCountry) {

    this.customerCountry = customerCountry;
  }

  public String getCustomerPhone() {

    return this.customerPhone;
  }

  public void setCustomerPhone(final String customerPhone) {

    this.customerPhone = customerPhone;
  }

  public String getCustomerRegistrationNumber() {

    return this.customerRegistrationNumber;
  }

  public void setCustomerRegistrationNumber(final String customerRegistrationNumber) {

    this.customerRegistrationNumber = customerRegistrationNumber;
  }

  public String getCustomerEmail() {

    return this.customerEmail;
  }

  public void setCustomerEmail(final String customerEmail) {

    this.customerEmail = customerEmail;
  }

  public String getCustomerDescription() {

    return this.customerDescription;
  }

  public void setCustomerDescription(final String customerDescription) {

    this.customerDescription = customerDescription;
  }

  public String getCustomerSectorActivity() {

    return this.customerSectorActivity;
  }

  public void setCustomerSectorActivity(final String customerSectorActivity) {

    this.customerSectorActivity = customerSectorActivity;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Customer [customerId=" + this.customerId + ", customerExternalId="
        + this.customerExternalId + ", customerFirstName=" + this.customerFirstName
        + ", customerCode=" + this.customerCode + ", customerLastName=" + this.customerLastName
        + ", customerCompanyName=" + this.customerCompanyName + ", customerAdresse="
        + this.customerAdresse + ", customerCity=" + this.customerCity + ", customerCountry="
        + this.customerCountry + ", customerPhone=" + this.customerPhone
        + ", customerRegistrationNumber=" + this.customerRegistrationNumber + ", customerEmail="
        + this.customerEmail + ", customerDescription=" + this.customerDescription
        + ", customerSectorActivity=" + this.customerSectorActivity + ", createdAt="
        + this.createdAt + ", updatedAt=" + this.updatedAt + "]";
  }

}
