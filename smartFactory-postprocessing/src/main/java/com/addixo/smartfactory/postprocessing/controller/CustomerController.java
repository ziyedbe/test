
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Customer;
import com.addixo.smartfactory.postprocessing.service.CustomerService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CustomerController {

  private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

  @Autowired
  private CustomerService customerService;

  @PostMapping("/customer")
  public Customer createCustomer(@Valid @RequestBody final Customer customer) {

    logger.debug("create customer : {}", customer);
    return this.customerService.saveOrUpdateCustomer(customer);
  }

  @GetMapping("/customer")
  public List<Customer> listAllCustomer() {

    logger.debug("get all {}", " customer ");
    return this.customerService.findAllCustomer();
  }

  @DeleteMapping("/customer/{id}")
  public String deleteCustomer(@PathVariable("id") final Long customerId) {

    logger.debug("delete customer by id {}", customerId);
    final Customer customer = this.customerService.findCustomerById(customerId).orElseThrow(
        () -> new ResourceNotFoundException(customerId.toString(), "customerId not found"));
    this.customerService.deleteCustomer(customer.getCustomerId());
    return "deleted";
  }

  @PutMapping("/customer/{id}")
  public Customer updateCustomer(@PathVariable("id") final Long id,
      @Valid @RequestBody final Customer newCustomer) {

    logger.debug("update customer by id {}", id);
    final Customer oldCustomer = this.customerService.findCustomerById(id)
        .orElseThrow(() -> new ResourceNotFoundException(id, "customerId not found"));
    newCustomer.setCustomerId(oldCustomer.getCustomerId());
    return this.customerService.saveOrUpdateCustomer(newCustomer);
  }
}