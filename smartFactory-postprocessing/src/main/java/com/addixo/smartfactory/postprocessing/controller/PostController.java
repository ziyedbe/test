
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Post;
import com.addixo.smartfactory.postprocessing.service.PostService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PostController {

  private static final Logger logger = LoggerFactory.getLogger(PostController.class);

  @Autowired
  private PostService postService;

  @PostMapping("/post")

  public Post createPost(@Valid @RequestBody final Post post) {

    logger.debug("create post : {}", post);
    return this.postService.saveOrUpdatePost(post);
  }

  @GetMapping("/post")

  public List<Post> listAllPost() {

    logger.info("get all {}", " post");
    return this.postService.findAllPost();
  }

  @GetMapping("/post/byPostReference/{postReference}")

  public Post listPostbyPostReference(@PathVariable("postReference") final String postReference) {

    logger.info("get post by {}", "PostReference");
    return this.postService.findByPostReference(postReference)
        .orElseThrow(() -> new ResourceNotFoundException(postReference, "postReference not found"));
  }

  @GetMapping("/post/byPostLineId/{postLineId}")

  public List<Post> getByPostLineId(@PathVariable("postLineId") final Long postLineId) {

    logger.info("get post by {}", "postLineId");
    return this.postService.findByPostLineId(postLineId);

  }

  @DeleteMapping("/post/{postId}")

  public String deletePost(@PathVariable("postId") final Long postId) {

    logger.debug("delete post by id {}", postId);
    final Post post = this.postService.findPostById(postId)
        .orElseThrow(() -> new ResourceNotFoundException(postId.toString(), "postId not found"));
    this.postService.deletePost(post.getPostId());
    return "deleted";
  }

  @PutMapping("/post/{id}")

  public Post updatePost(@PathVariable("id") final Long id,
      @Valid @RequestBody final Post newPost) {

    logger.debug("update post by id {}", id);
    final Post oldPost = this.postService.findPostById(id)
        .orElseThrow(() -> new ResourceNotFoundException("post", id));
    newPost.setPostId(oldPost.getPostId());
    return this.postService.saveOrUpdatePost(newPost);
  }
}
