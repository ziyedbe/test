
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.TypeConnection;
import com.addixo.smartfactory.postprocessing.repository.TypeConnectionRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class TypeConnectionServiceImpl implements TypeConnectionService {

  @Autowired
  private TypeConnectionRepository typeConnectionRepository;

  @Override
  @Transactional(readOnly = true)
  public List<TypeConnection> findAllTypeConnection() {

    return this.typeConnectionRepository.findAll();
  }

  @Override
  public TypeConnection saveOrUpdateTypeConnection(final TypeConnection typeConnection) {

    return this.typeConnectionRepository.save(typeConnection);
  }

  @Override
  public void deleteTypeConnection(final Long typeConnectionId) {

    this.typeConnectionRepository.delete(typeConnectionId);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<TypeConnection> findTypeConnectionById(final Long id) {

    return Optional.ofNullable(this.typeConnectionRepository.findOne(id));
  }

}
