
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Settings;

import java.util.List;
import java.util.Optional;

public interface SettingsService {

  List<Settings> findAllSettings();

  Settings saveOrUpdateSettings(Settings settings);

  void deleteSettings(Long id);

  Optional<Settings> findSettingsById(Long id);
}
