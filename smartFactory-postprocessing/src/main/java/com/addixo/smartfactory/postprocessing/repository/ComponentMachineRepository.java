
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.ComponentMachine;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentMachineRepository extends JpaRepository<ComponentMachine, Long> {

}
