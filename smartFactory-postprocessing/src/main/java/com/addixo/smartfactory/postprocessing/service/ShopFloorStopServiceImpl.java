
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorStop;
import com.addixo.smartfactory.postprocessing.repository.ShopFloorStopRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class ShopFloorStopServiceImpl implements ShopFloorStopService {

  @Autowired
  private ShopFloorStopRepository shopFloorStopRepository;

  @Override
  @Transactional(readOnly = true)
  public List<ShopFloorStop> findAllShopFloorStop() {

    return this.shopFloorStopRepository.findAll();
  }

  @Override
  public ShopFloorStop saveOrUpdateShopFloorStop(final ShopFloorStop arret) {

    return this.shopFloorStopRepository.save(arret);
  }

  @Override
  public void deleteShopFloorStop(final String shopFloorStopId) {

    this.shopFloorStopRepository.delete(shopFloorStopId);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ShopFloorStop> findShopFloorStopById(final String id) {

    return Optional.ofNullable(this.shopFloorStopRepository.findOne(id));
  }

  @Override
  @Transactional(readOnly = true)

  public List<ShopFloorStop> findByShopFloorStopPostReferenceAndShopFloorStopLineLabelAndShopFloorStopMachineReferenceAndShopFloorStopEventDateBetween(
      final String shopFloorStopPostReference, final String shopFloorStopLineLabel,
      final String shopFloorStopMachineReference, final Date startDate, final Date endtDate) {

    return this.shopFloorStopRepository
        .findByShopFloorStopPostReferenceAndShopFloorStopLineLabelAndShopFloorStopMachineReferenceAndShopFloorStopEventDateBetween(
            shopFloorStopPostReference, shopFloorStopLineLabel, shopFloorStopMachineReference,
            startDate, endtDate);
  }

  @Override
  @Transactional(readOnly = true)
  public List<ShopFloorStop> findByShopFloorStopOfIdAndShopFloorStopEventDateBetween(
      final String shopFloorStopOfId, final Date startDate, final Date endtDate) {

    return this.shopFloorStopRepository.findByShopFloorStopOfIdAndShopFloorStopEventDateBetween(
        shopFloorStopOfId, startDate, endtDate);
  }
}
