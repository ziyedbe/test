
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Product;
import com.addixo.smartfactory.postprocessing.service.ProductService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProductController {

  private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

  @Autowired
  private ProductService productService;

  @PostMapping("/product")
  public Product createProduct(@Valid @RequestBody final Product product) {

    logger.debug("create product : {}", product);
    return this.productService.saveOrUpdateProduct(product);
  }

  @GetMapping("/product")
  /* request path contient ?page=0&size=3 */
  public List<Product> listAllProduct(@RequestParam("page") final int page,
      @RequestParam("size") final int size) {

    logger.debug("get all {}", "product ");

    return this.productService.findAllProduct(new PageRequest(page, size));
  }

  @DeleteMapping("/product/{id}")
  public String deleteProduct(@PathVariable("id") final Long productId) {

    logger.debug("delete product by id {}", productId);
    final Product product = this.productService.findProductById(productId)
        .orElseThrow(() -> new ResourceNotFoundException(productId.toString(), "not found"));
    this.productService.deleteProduct(product.getProductId());
    return "deleted";
  }

  @PutMapping("/product/{id}")
  public Product updateProduct(@PathVariable("id") final Long id,
      @Valid @RequestBody final Product newProduct) {

    logger.debug("update product by id {}", id);
    final Product oldProduct = this.productService.findProductById(id)
        .orElseThrow(() -> new ResourceNotFoundException("product", id));
    newProduct.setProductId(oldProduct.getProductId());
    return this.productService.saveOrUpdateProduct(newProduct);
  }
}
