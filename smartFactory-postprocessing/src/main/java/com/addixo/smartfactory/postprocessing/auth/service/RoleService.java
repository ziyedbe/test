
package com.addixo.smartfactory.postprocessing.auth.service;

import com.addixo.smartfactory.postprocessing.auth.domain.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {

  Optional<Role> findById(Long id);

  Optional<Role> findByName(String name);

  List<Role> findAll();

  Role saveOrUpdate(Role roleRequest);

  void delete(Long roleId);
}
