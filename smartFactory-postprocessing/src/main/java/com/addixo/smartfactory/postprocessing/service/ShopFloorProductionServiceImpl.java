
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.QShopFloorProduction;
import com.addixo.smartfactory.postprocessing.domain.ShopFloorProduction;
import com.addixo.smartfactory.postprocessing.enumeration.QualityType;
import com.addixo.smartfactory.postprocessing.repository.ShopFloorProductionRepository;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class ShopFloorProductionServiceImpl implements ShopFloorProductionService {

  private static final Logger logger = LoggerFactory
      .getLogger(ShopFloorProductionServiceImpl.class);

  @Autowired
  private ShopFloorProductionRepository shopFloorProductionRepository;

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  @Transactional(readOnly = true)
  public List<ShopFloorProduction> findAllShopFloorProduction() {

    return this.shopFloorProductionRepository.findAll();
  }

  @Override
  public ShopFloorProduction saveOrUpdateShopFloorProduction(
      final ShopFloorProduction shopFloorProduction) {

    return this.shopFloorProductionRepository.save(shopFloorProduction);
  }

  @Override
  public void deleteShopFloorProduction(final String id) {

    this.shopFloorProductionRepository.delete(id);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<ShopFloorProduction> findShopFloorProductionById(final String id) {

    return Optional.ofNullable(this.shopFloorProductionRepository.findOne(id));
  }

  @Override
  public List<ShopFloorProduction> list(final String shopFloorProdPostReference,
      final String shopFloorProdLineLabel, final String shopFloorProdMachineReference,
      final Date startDate, final Date endtDate, final String shopFloorProdOfId) {

    // throws IllegalArgumentException if shopFloorProdPostReference is null
    Preconditions.checkNotNull(shopFloorProdPostReference,
        "shopFloorProdPostReference must not be null");

    QShopFloorProduction qfp = QShopFloorProduction.shopFloorProduction;

    BooleanExpression be = qfp.shopFloorProdPostReference.eq(shopFloorProdPostReference);

    if (!Strings.isNullOrEmpty(shopFloorProdLineLabel)) {
      be = be.and(qfp.shopFloorProdLineLabel.eq(shopFloorProdLineLabel));
    }

    if (!Strings.isNullOrEmpty(shopFloorProdMachineReference)) {
      be = be.and(qfp.shopFloorProdMachineReference.eq(shopFloorProdMachineReference));
    }

    if ((startDate != null) && (endtDate != null)) {
      be = be.and(qfp.shopFloorProdEventDate.between(startDate, endtDate));
    }
    if (!Strings.isNullOrEmpty(shopFloorProdOfId)) {
      be.and(qfp.shopFloorProdOfId.eq(shopFloorProdOfId));
    }
    return Lists.newArrayList(this.shopFloorProductionRepository.findAll(be));
  }

  @Override
  public Long getProducedQuantity(final String shopFloorProdOfId, final Date startDate,
      final Date endtDate, final QualityType type) {

    Preconditions.checkNotNull(shopFloorProdOfId, "shopFloorProdPostReference must not be null");
    Preconditions.checkNotNull(type, "type must not be null");
    Preconditions.checkNotNull(endtDate, "endtDate must not be null");
    Preconditions.checkNotNull(startDate, "startDate must not be null");
    logger.info("start getProducedQuantity type {} , {}, {},{}", shopFloorProdOfId, type, endtDate,
        startDate);

    final JPAQuery<ShopFloorProduction> query = new JPAQuery<>(this.entityManager);
    final QShopFloorProduction qfp = QShopFloorProduction.shopFloorProduction;
    JPAQuery<Long> quantity = null;
    final BooleanExpression be = qfp.shopFloorProdOfId.eq(shopFloorProdOfId);

    if ((startDate != null) && (endtDate != null)) {
      be.and(qfp.shopFloorProdEventDate.between(startDate, endtDate));
    }
    if (QualityType.ok.equals(type)) {
      quantity = query.select(qfp.shopFloorProdQuantity.sum()).from(qfp).where(be);
      logger.info("quantity {} ", quantity.fetchOne());
    } else if (QualityType.notOk.equals(type)) {
      quantity = query.select(qfp.shopFloorProdQuantityNotOk.sum()).from(qfp).where(be);
      logger.info("quantity {} ", quantity.fetchOne());
    }
    return quantity.fetchOne();
  }

}
