
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Settings", columnList = "settingsExternalId,settingsProductCode"))

public class Settings implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long settingsId;

  private String settingsExternalId;

  private String settingsProductCode;

  private String settingsThresholdMax;

  private String settingsThresholdMin;

  @Column(unique = true)
  private String settingsType;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getSettingsId() {

    return this.settingsId;
  }

  public void setSettingsId(final Long settingsId) {

    this.settingsId = settingsId;
  }

  public String getSettingsExternalId() {

    return this.settingsExternalId;
  }

  public void setSettingsExternalId(final String settingsExternalId) {

    this.settingsExternalId = settingsExternalId;
  }

  public String getSettingsProductCode() {

    return this.settingsProductCode;
  }

  public void setSettingsProductCode(final String settingsProductCode) {

    this.settingsProductCode = settingsProductCode;
  }

  public String getSettingsThresholdMax() {

    return this.settingsThresholdMax;
  }

  public void setSettingsThresholdMax(final String settingsThresholdMax) {

    this.settingsThresholdMax = settingsThresholdMax;
  }

  public String getSettingsThresholdMin() {

    return this.settingsThresholdMin;
  }

  public void setSettingsThresholdMin(final String settingsThresholdMin) {

    this.settingsThresholdMin = settingsThresholdMin;
  }

  public String getSettingsType() {

    return this.settingsType;
  }

  public void setSettingsType(final String settingsType) {

    this.settingsType = settingsType;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Settings [settingsId=" + this.settingsId + ", settingsExternalId="
        + this.settingsExternalId + ", settingsProductCode=" + this.settingsProductCode
        + ", settingsThresholdMax=" + this.settingsThresholdMax + ", settingsThresholdMin="
        + this.settingsThresholdMin + ", settingsType=" + this.settingsType + ", createdAt="
        + this.createdAt + ", updatedAt=" + this.updatedAt + "]";
  }

}
