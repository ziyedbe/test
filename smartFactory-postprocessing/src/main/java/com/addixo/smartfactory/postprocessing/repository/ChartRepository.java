
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.Chart;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChartRepository extends JpaRepository<Chart, Long> {

  Chart findById(Long id);

  @Override
  List<Chart> findAll();

}
