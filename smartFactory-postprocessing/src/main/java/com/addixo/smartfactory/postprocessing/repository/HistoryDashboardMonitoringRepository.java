
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.HistoryDashboardMonitoring;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryDashboardMonitoringRepository
    extends JpaRepository<HistoryDashboardMonitoring, String> {
}
