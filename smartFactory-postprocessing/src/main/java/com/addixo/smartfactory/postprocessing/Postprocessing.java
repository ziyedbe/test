
package com.addixo.smartfactory.postprocessing;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAutoConfiguration
@SpringBootApplication(scanBasePackages = { "com.addixo.smartfactory.postprocessing",
    "com.addixo.smartfactory.postprocessing.batch" })
@EnableJpaAuditing
@EnableScheduling
@EnableAspectJAutoProxy
public class Postprocessing {

  @PostConstruct
  void setUtcTimezone() {

    TimeZone.setDefault(TimeZone.getTimeZone("GMT+1:00"));
  }

  public static void main(final String[] args) {

    SpringApplication.run(Postprocessing.class, args);
  }
}
