
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.WorkShop;
import com.addixo.smartfactory.postprocessing.domain.dto.OrganizationWorkShop;
import com.addixo.smartfactory.postprocessing.service.WorkShopService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class WorkShopController {

  private static final Logger logger = LoggerFactory.getLogger(WorkShopController.class);

  @Autowired
  private WorkShopService workShopService;

  @PostMapping("/workShop")
  public WorkShop createWorkShop(@Valid @RequestBody final WorkShop workShop) {

    logger.debug("create workShop : {}", workShop);
    return this.workShopService.saveOrUpdateWorkShop(workShop);
  }

  @GetMapping("/workShop")
  public List<WorkShop> listAllWorkShop() {

    logger.debug("get all {}", "workShop ");
    return this.workShopService.findAllWorkShop();
  }

  @GetMapping("/workShop/{workShopId}")
  public WorkShop getWorkShopById(@PathVariable("workShopId") final Long workShopId) {

    logger.debug("get workshop by id {}", workShopId);
    return this.workShopService.findWorkShopById(workShopId).orElseThrow(
        () -> new ResourceNotFoundException(workShopId.toString(), "workShopId not found"));
  }

  @GetMapping("/workShop/orgWorkshop")
  public List<OrganizationWorkShop> listAllWorkShopByOrganizationName() {

    logger.debug("list All WorkShop With OrganizationName");
    return this.workShopService.findOrganizationByWorkshop();
  }

  @DeleteMapping("/workShop/{id}")
  public String deleteWorkShop(@PathVariable("id") final Long workShopId) {

    logger.debug("delete workShop by id {}", workShopId);
    final WorkShop workShop = this.workShopService.findWorkShopById(workShopId).orElseThrow(
        () -> new ResourceNotFoundException(workShopId.toString(), "workShopId not found"));
    this.workShopService.deleteWorkShop(workShop.getWorkShopId());
    return "deleted";
  }

  @PutMapping("/workShop/{id}")
  public WorkShop updateWorkShop(@PathVariable("id") final Long id,
      @Valid @RequestBody final WorkShop newWorkShop) {

    logger.debug("update workShop by id {}", id);
    final WorkShop oldWorkShop = this.workShopService.findWorkShopById(id)
        .orElseThrow(() -> new ResourceNotFoundException("workShop", id));
    newWorkShop.setWorkShopId(oldWorkShop.getWorkShopId());
    return this.workShopService.saveOrUpdateWorkShop(newWorkShop);

  }

}
