
package com.addixo.smartfactory.postprocessing.auth.service;

import com.addixo.smartfactory.postprocessing.auth.domain.Permission;
import com.addixo.smartfactory.postprocessing.auth.domain.Role;
import com.addixo.smartfactory.postprocessing.auth.repository.PermissionRepository;
import com.addixo.smartfactory.postprocessing.auth.repository.RoleRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl implements PermissionService {

  @Autowired
  private PermissionRepository permissionRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Override
  public Optional<Permission> findById(final Long id) {

    return Optional.of(this.permissionRepository.findOne(id));
  }

  @Override
  public Optional<Permission> findByName(final String name) {

    return this.permissionRepository.findByName(name);
  }

  @Override
  public List<Permission> findAll() {

    return this.permissionRepository.findAll();
  }

  @Override
  public Permission saveOrUpdate(final Permission operation) {

    return this.permissionRepository.save(operation);
  }

  @Override
  public void delete(final Long operationId) {

    Permission operation = this.permissionRepository.findOne(operationId);
    for (Role r : this.roleRepository.findAll()) {
      r.getPermissions().remove(operation);
    }
    this.permissionRepository.delete(operation);
  }
}
