
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.SkgIoItem;

import java.util.List;
import java.util.Optional;

public interface SkgIoItemService {

  List<SkgIoItem> findAllSkgIoItem();

  SkgIoItem saveOrUpdateSkgIoItem(SkgIoItem skgIoItem);

  Optional<SkgIoItem> findBySkgItemId(Long skgIoItemId);

  void deleteSkgIoItem(Long skgIoItemId);

  Optional<SkgIoItem> findBySkgIoItemEntrer(String skgIoItemEntrer);

}
