
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.config.Environment;
import com.addixo.smartfactory.postprocessing.domain.WorkShop;
import com.addixo.smartfactory.postprocessing.domain.dto.OrganizationWorkShop;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkShopRepository extends JpaRepository<WorkShop, Long> {

  @Query(value = Environment.findOrganizationByWorkshop, nativeQuery = true)
  List<OrganizationWorkShop> findOrganizationByWorkshop();

}
