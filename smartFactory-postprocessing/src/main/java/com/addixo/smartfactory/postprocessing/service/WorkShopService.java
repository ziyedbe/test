
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.WorkShop;
import com.addixo.smartfactory.postprocessing.domain.dto.OrganizationWorkShop;

import java.util.List;
import java.util.Optional;

public interface WorkShopService {

  List<WorkShop> findAllWorkShop();

  List<OrganizationWorkShop> findOrganizationByWorkshop();

  WorkShop saveOrUpdateWorkShop(WorkShop workShop);

  void deleteWorkShop(Long workShopId);

  Optional<WorkShop> findWorkShopById(Long id);

}
