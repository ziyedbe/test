
package com.addixo.smartfactory.postprocessing.domain.dto;

import com.addixo.smartfactory.postprocessing.domain.Operation;
import com.addixo.smartfactory.postprocessing.enumeration.State;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class DashboardMonitoringOperation {

  private String dashboardOfId;

  private String dashboardOpId;

  private String dashboardExternalOfId;

  private String dashboardLineLabel;

  private String dashboardWorkShopLabel;

  private String dashboardMachineReference;

  private String dashboardPostReference;

  private String dashboardProductCode;

  private String dashboardProductLabel;

  private String dashboardProductCustomerCode;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dashboardDateTimeStartPlanned;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dashboardDateTimeFinishPlanned;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dashboardDateTimeRealFinished;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dashboardDateTimeRealStarted;

  private String dashboardProductDevise;

  private String dashboardProductPrice;

  private double dashboardProductTheoreticCycle;

  private double dashboardDuration;

  private double dashboardArretUnPlanned;

  private Long dashboardQuantityGlobal;

  private Long dashboardQuantityProduced;

  private Long dashboardQuantityNotOk;

  private String dashboardNumCommande;

  @Enumerated(EnumType.STRING)
  private State dashboardStatus;

  private Long dashboardUserId;

  /** The of responsible team. */
  private String dashboardUserLogin;

  private String dashboardUserRole;

  private List<Operation> operations;

  public String getDashboardOfId() {

    return this.dashboardOfId;
  }

  public void setDashboardOfId(final String dashboardOfId) {

    this.dashboardOfId = dashboardOfId;
  }

  public String getDashboardOpId() {

    return this.dashboardOpId;
  }

  public void setDashboardOpId(final String dashboardOpId) {

    this.dashboardOpId = dashboardOpId;
  }

  public String getDashboardExternalOfId() {

    return this.dashboardExternalOfId;
  }

  public void setDashboardExternalOfId(final String dashboardExternalOfId) {

    this.dashboardExternalOfId = dashboardExternalOfId;
  }

  public String getDashboardLineLabel() {

    return this.dashboardLineLabel;
  }

  public void setDashboardLineLabel(final String dashboardLineLabel) {

    this.dashboardLineLabel = dashboardLineLabel;
  }

  public String getDashboardWorkShopLabel() {

    return this.dashboardWorkShopLabel;
  }

  public void setDashboardWorkShopLabel(final String dashboardWorkShopLabel) {

    this.dashboardWorkShopLabel = dashboardWorkShopLabel;
  }

  public String getDashboardMachineReference() {

    return this.dashboardMachineReference;
  }

  public void setDashboardMachineReference(final String dashboardMachineReference) {

    this.dashboardMachineReference = dashboardMachineReference;
  }

  public String getDashboardPostReference() {

    return this.dashboardPostReference;
  }

  public void setDashboardPostReference(final String dashboardPostReference) {

    this.dashboardPostReference = dashboardPostReference;
  }

  public String getDashboardProductCode() {

    return this.dashboardProductCode;
  }

  public void setDashboardProductCode(final String dashboardProductCode) {

    this.dashboardProductCode = dashboardProductCode;
  }

  public String getDashboardProductLabel() {

    return this.dashboardProductLabel;
  }

  public void setDashboardProductLabel(final String dashboardProductLabel) {

    this.dashboardProductLabel = dashboardProductLabel;
  }

  public String getDashboardProductCustomerCode() {

    return this.dashboardProductCustomerCode;
  }

  public void setDashboardProductCustomerCode(final String dashboardProductCustomerCode) {

    this.dashboardProductCustomerCode = dashboardProductCustomerCode;
  }

  public Date getDashboardDateTimeStartPlanned() {

    return this.dashboardDateTimeStartPlanned;
  }

  public void setDashboardDateTimeStartPlanned(final Date dashboardDateTimeStartPlanned) {

    this.dashboardDateTimeStartPlanned = dashboardDateTimeStartPlanned;
  }

  public Date getDashboardDateTimeFinishPlanned() {

    return this.dashboardDateTimeFinishPlanned;
  }

  public void setDashboardDateTimeFinishPlanned(final Date dashboardDateTimeFinishPlanned) {

    this.dashboardDateTimeFinishPlanned = dashboardDateTimeFinishPlanned;
  }

  public Date getDashboardDateTimeRealFinished() {

    return this.dashboardDateTimeRealFinished;
  }

  public void setDashboardDateTimeRealFinished(final Date dashboardDateTimeRealFinished) {

    this.dashboardDateTimeRealFinished = dashboardDateTimeRealFinished;
  }

  public Date getDashboardDateTimeRealStarted() {

    return this.dashboardDateTimeRealStarted;
  }

  public void setDashboardDateTimeRealStarted(final Date dashboardDateTimeRealStarted) {

    this.dashboardDateTimeRealStarted = dashboardDateTimeRealStarted;
  }

  public String getDashboardProductDevise() {

    return this.dashboardProductDevise;
  }

  public void setDashboardProductDevise(final String dashboardProductDevise) {

    this.dashboardProductDevise = dashboardProductDevise;
  }

  public String getDashboardProductPrice() {

    return this.dashboardProductPrice;
  }

  public void setDashboardProductPrice(final String dashboardProductPrice) {

    this.dashboardProductPrice = dashboardProductPrice;
  }

  public double getDashboardProductTheoreticCycle() {

    return this.dashboardProductTheoreticCycle;
  }

  public void setDashboardProductTheoreticCycle(final double dashboardProductTheoreticCycle) {

    this.dashboardProductTheoreticCycle = dashboardProductTheoreticCycle;
  }

  public double getDashboardDuration() {

    return this.dashboardDuration;
  }

  public void setDashboardDuration(final double dashboardDuration) {

    this.dashboardDuration = dashboardDuration;
  }

  public double getDashboardArretUnPlanned() {

    return this.dashboardArretUnPlanned;
  }

  public void setDashboardArretUnPlanned(final double dashboardArretUnPlanned) {

    this.dashboardArretUnPlanned = dashboardArretUnPlanned;
  }

  public Long getDashboardQuantityGlobal() {

    return this.dashboardQuantityGlobal;
  }

  public void setDashboardQuantityGlobal(final Long dashboardQuantityGlobal) {

    this.dashboardQuantityGlobal = dashboardQuantityGlobal;
  }

  public Long getDashboardQuantityProduced() {

    return this.dashboardQuantityProduced;
  }

  public void setDashboardQuantityProduced(final Long dashboardQuantityProduced) {

    this.dashboardQuantityProduced = dashboardQuantityProduced;
  }

  public Long getDashboardQuantityNotOk() {

    return this.dashboardQuantityNotOk;
  }

  public void setDashboardQuantityNotOk(final Long dashboardQuantityNotOk) {

    this.dashboardQuantityNotOk = dashboardQuantityNotOk;
  }

  public String getDashboardNumCommande() {

    return this.dashboardNumCommande;
  }

  public void setDashboardNumCommande(final String dashboardNumCommande) {

    this.dashboardNumCommande = dashboardNumCommande;
  }

  public State getDashboardStatus() {

    return this.dashboardStatus;
  }

  public void setDashboardStatus(final State dashboardStatus) {

    this.dashboardStatus = dashboardStatus;
  }

  public Long getDashboardUserId() {

    return this.dashboardUserId;
  }

  public void setDashboardUserId(final Long dashboardUserId) {

    this.dashboardUserId = dashboardUserId;
  }

  public String getDashboardUserLogin() {

    return this.dashboardUserLogin;
  }

  public void setDashboardUserLogin(final String dashboardUserLogin) {

    this.dashboardUserLogin = dashboardUserLogin;
  }

  public String getDashboardUserRole() {

    return this.dashboardUserRole;
  }

  public void setDashboardUserRole(final String dashboardUserRole) {

    this.dashboardUserRole = dashboardUserRole;
  }

  public List<Operation> getOperations() {

    return this.operations;
  }

  public void setOperations(final List<Operation> operations) {

    this.operations = operations;
  }

  @Override
  public String toString() {

    return "DashbordMonitoringOperation [dashboardOfId=" + this.dashboardOfId + ", dashboardOpId="
        + this.dashboardOpId + ", dashboardExternalOfId=" + this.dashboardExternalOfId
        + ", dashboardLineLabel=" + this.dashboardLineLabel + ", dashboardWorkShopLabel="
        + this.dashboardWorkShopLabel + ", dashboardMachineReference="
        + this.dashboardMachineReference + ", dashboardPostReference=" + this.dashboardPostReference
        + ", dashboardProductCode=" + this.dashboardProductCode + ", dashboardProductLabel="
        + this.dashboardProductLabel + ", dashboardProductCustomerCode="
        + this.dashboardProductCustomerCode + ", dashboardDateTimeStartPlanned="
        + this.dashboardDateTimeStartPlanned + ", dashboardDateTimeFinishPlanned="
        + this.dashboardDateTimeFinishPlanned + ", dashboardDateTimeRealFinished="
        + this.dashboardDateTimeRealFinished + ", dashboardDateTimeRealStarted="
        + this.dashboardDateTimeRealStarted + ", dashboardProductDevise="
        + this.dashboardProductDevise + ", dashboardProductPrice=" + this.dashboardProductPrice
        + ", dashboardProductTheoreticCycle=" + this.dashboardProductTheoreticCycle
        + ", dashboardDuration=" + this.dashboardDuration + ", dashboardArretUnPlanned="
        + this.dashboardArretUnPlanned + ", dashboardQuantityGlobal=" + this.dashboardQuantityGlobal
        + ", dashboardQuantityProduced=" + this.dashboardQuantityProduced
        + ", dashboardQuantityNotOk=" + this.dashboardQuantityNotOk + ", dashboardNumCommande="
        + this.dashboardNumCommande + ", dashboardStatus=" + this.dashboardStatus
        + ", dashboardUserId=" + this.dashboardUserId + ", dashboardUserLogin="
        + this.dashboardUserLogin + ", dashboardUserRole=" + this.dashboardUserRole
        + ", operations=" + this.operations + "]";
  }

}
