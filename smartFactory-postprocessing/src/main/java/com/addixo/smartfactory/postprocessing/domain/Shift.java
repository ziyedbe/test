
package com.addixo.smartfactory.postprocessing.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)

public class Shift implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long shiftId;

  private String shiftExternalId;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = "GMT+1:00")
  @Temporal(TemporalType.TIME)
  private Date shiftStart;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "GMT+1:00")
  @Temporal(TemporalType.TIME)
  private Date shiftEnd;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getShiftId() {

    return this.shiftId;
  }

  public void setShiftId(final Long shiftId) {

    this.shiftId = shiftId;
  }

  public String getShiftExternalId() {

    return this.shiftExternalId;
  }

  public void setShiftExternalId(final String shiftExternalId) {

    this.shiftExternalId = shiftExternalId;
  }

  public Date getShiftStart() {

    return this.shiftStart;
  }

  public void setShiftStart(final Date shiftStart) {

    this.shiftStart = shiftStart;
  }

  public Date getShiftEnd() {

    return this.shiftEnd;
  }

  public void setShiftEnd(final Date shiftEnd) {

    this.shiftEnd = shiftEnd;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Shift [shiftId=" + this.shiftId + ", shiftExternalId=" + this.shiftExternalId
        + ", shiftStart=" + this.shiftStart + ", shiftEnd=" + this.shiftEnd + ", createdAt="
        + this.createdAt + ", updatedAt=" + this.updatedAt + "]";
  }

}