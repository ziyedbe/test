
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorOps;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ShopFloorOpsService {

  List<ShopFloorOps> findAllShopFloorOps();

  ShopFloorOps saveOrUpdateShopFloorOps(ShopFloorOps monitoringInformation);

  void deleteShopFloorOps(String id);

  Optional<ShopFloorOps> findShopFloorOpsById(String id);

  ShopFloorOps updateShopFloorOps(ShopFloorOps monitoringInformation);

  List<ShopFloorOps> findByShopFloorOpsPostReferenceAndShopFloorOpsLineLabelAndShopFloorOpsMachineReferenceAndShopFloorOpsEventDateBetween(
      String shopFloorOpsPostReference, String shopFloorOpsLineLabel,
      String shopFloorOpsMachineReference, Date startDate, Date endtDate);

  List<ShopFloorOps> findByShopFloorOpsOfIdAndShopFloorOpsEventDateBetween(String shopFloorOpsOfId,
      Date startDate, Date endtDate);
}
