
package com.addixo.smartfactory.postprocessing.batch.step;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

public class BatchSocketIoWriter implements ItemWriter<DashboardMonitoring> {

  private static final Logger logger = LoggerFactory.getLogger(BatchSocketIoWriter.class);

  private final SimpMessageSendingOperations messagingTemplate;

  public BatchSocketIoWriter(final SimpMessageSendingOperations messagingTemplate) {

    this.messagingTemplate = messagingTemplate;
  }

  @Override
  public void write(final List<? extends DashboardMonitoring> items) throws Exception {

    logger.info("start write {}", " BatchSocketIoWriter");
    logger.debug("start process items  {}", items.size());
    this.messagingTemplate.convertAndSend("/smartfactory", items);
    logger.debug("end {}", " write");
  }
}
