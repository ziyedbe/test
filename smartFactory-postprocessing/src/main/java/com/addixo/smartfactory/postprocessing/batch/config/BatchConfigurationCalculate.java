
package com.addixo.smartfactory.postprocessing.batch.config;

import com.addixo.smartfactory.postprocessing.batch.step.BatchItemProcessor;
import com.addixo.smartfactory.postprocessing.batch.step.BatchItemReader;
import com.addixo.smartfactory.postprocessing.batch.step.BatchItemWriter;
import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.service.DashboardMonitoringService;
import com.addixo.smartfactory.postprocessing.service.ShopFloorProductionService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableBatchProcessing
public class BatchConfigurationCalculate {

  private static final Logger logger = LoggerFactory.getLogger(BatchConfigurationCalculate.class);

  @Autowired
  private ShopFloorProductionService shopFloorProdService;

  @Autowired
  private StepBuilderFactory stepBuilderFactory;

  @Autowired
  private JobBuilderFactory jobBuilderFactory;

  @Autowired
  private DashboardMonitoringService dashboardMonitoringService;

  @Autowired
  private SimpleJobLauncher jobLauncher;

  @Autowired
  private JobExecutionListener listener;

  @Scheduled(cron = "${batch.cron.dashbordMonitoring}")
  public void dashbordMonitoringCalculate() {

    try {
      JobParameters jobParameters = new JobParametersBuilder()
          .addLong("time", System.currentTimeMillis()).toJobParameters();
      this.jobLauncher.run(this.processJob(), jobParameters);
      logger.info("Launcher {}", " dashbordMonitoringCalculate");
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  public Job processJob() {

    return this.jobBuilderFactory.get("processJob").preventRestart()
        .incrementer(new RunIdIncrementer()).listener(this.listener).flow(this.orderStep1()).end()
        .build();
  }

  public Step orderStep1() {

    return this.stepBuilderFactory.get("orderStep1")
        .<DashboardMonitoring, DashboardMonitoring>chunk(1).reader(this.batchItemReader())
        .processor(new BatchItemProcessor(this.shopFloorProdService))
        .writer(new BatchItemWriter(this.dashboardMonitoringService)).faultTolerant().build();
  }

  public BatchItemReader batchItemReader() {

    return new BatchItemReader(this.getListDashboardMonitoring());
  }

  public List<DashboardMonitoring> getListDashboardMonitoring() {

    logger.debug("*** BATCH RED {} ***", "Read List DashboardMonitoring");
    return this.dashboardMonitoringService.findAllDashboardMonitoring();
  }

}
