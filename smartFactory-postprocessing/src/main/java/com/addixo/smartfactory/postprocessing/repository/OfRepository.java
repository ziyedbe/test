
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.Of;
import com.addixo.smartfactory.postprocessing.enumeration.State;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfRepository extends JpaRepository<Of, String> {

  List<Of> findByOfStatus(State ofStatus);

  List<Of> findAllByOrderByOfDateTimeStartPlannedDesc();

  List<Of> findByOfLineLabel(String ofLineLabel);

}
