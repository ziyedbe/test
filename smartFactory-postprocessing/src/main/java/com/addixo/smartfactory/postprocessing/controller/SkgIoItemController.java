
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.SkgIoItem;
import com.addixo.smartfactory.postprocessing.service.SkgIoItemService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class SkgIoItemController {

  private static final Logger logger = LoggerFactory.getLogger(SkgIoItemController.class);

  @Autowired
  private SkgIoItemService skgIoItemService;

  @PostMapping("/skgIoItem")
  public SkgIoItem saveSkgIoItem(@RequestBody final SkgIoItem skgIoItem) {

    logger.debug("saveSkgIoItem {}", skgIoItem);
    return this.skgIoItemService.saveOrUpdateSkgIoItem(skgIoItem);

  }

  @GetMapping("skgIoItem")
  public List<SkgIoItem> getAllSkgIoItem() {

    logger.info("get all  {}", " skgIoItem");
    return this.skgIoItemService.findAllSkgIoItem();
  }

  @DeleteMapping("/skgIoItem/{skgIoItemId}")

  public String deleteSkgIoItem(@PathVariable("skgIoItemId") final Long skgIoItemId) {

    logger.debug("deleteSkgIoItem {}", skgIoItemId);
    final SkgIoItem skgIoItem = this.skgIoItemService.findBySkgItemId(skgIoItemId)
        .orElseThrow(() -> new ResourceNotFoundException(skgIoItemId, "skgIoItemId not found"));
    this.skgIoItemService.deleteSkgIoItem(skgIoItem.getSkgIoItemId());
    return "deleted";
  }

  @GetMapping("/skgIoItem/byskgIoItemEntrer/{skgIoItemEntrer}")

  public SkgIoItem listSkgIoItembyskgIoItemEntrer(
      @PathVariable("skgIoItemEntrer") final String skgIoItemEntrer) {

    logger.debug("get post by PostReference{}", skgIoItemEntrer);
    return this.skgIoItemService.findBySkgIoItemEntrer(skgIoItemEntrer).orElseThrow(
        () -> new ResourceNotFoundException(skgIoItemEntrer, "skgIoItemEntrer not found"));
  }

  @PutMapping("/skgIoItem/{skgIoItemId}")

  public SkgIoItem updatePost(@PathVariable("skgIoItemId") final Long skgIoItemId,
      @Valid @RequestBody final SkgIoItem newSkgIoItem) {

    logger.debug("updatePost {}", skgIoItemId);
    final SkgIoItem oldSkgIoItem = this.skgIoItemService.findBySkgItemId(skgIoItemId)
        .orElseThrow(() -> new ResourceNotFoundException(skgIoItemId, "skgIoItemId not found"));
    newSkgIoItem.setSkgIoItemId(oldSkgIoItem.getSkgIoItemId());
    return this.skgIoItemService.saveOrUpdateSkgIoItem(newSkgIoItem);
  }

}
