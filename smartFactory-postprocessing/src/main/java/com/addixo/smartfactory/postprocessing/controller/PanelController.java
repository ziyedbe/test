
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Panel;
import com.addixo.smartfactory.postprocessing.service.PanelService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PanelController {

  private static final Logger logger = LoggerFactory.getLogger(PanelController.class);

  @Autowired
  private PanelService panelService;

  @PostMapping("/panel")
  public Panel createPanel(@Valid @RequestBody final Panel panel) {

    logger.debug("create ligne : {}", panel);
    return this.panelService.saveOrUpdatePanel(panel);
  }

  @GetMapping("/panel")
  public List<Panel> listAllPanel() {

    logger.debug("get all {}", "panel ");
    return this.panelService.findAllPanel();
  }

  @DeleteMapping("/panel/{id}")
  public String deletePanel(@PathVariable("id") final Long panelId) {

    logger.debug("delete panel by id {}", panelId);
    final Panel ligne = this.panelService.findPanelById(panelId)
        .orElseThrow(() -> new ResourceNotFoundException(panelId.toString(), "panelId not found"));
    this.panelService.deletePanel(ligne.getPanelId());
    return "deleted";
  }

  @PutMapping("/panel/{id}")
  public Panel updatePanel(@PathVariable("id") final Long id,
      @Valid @RequestBody final Panel newPanel) {

    logger.debug("update panel by id {}", id);
    final Panel oldPanel = this.panelService.findPanelById(id)
        .orElseThrow(() -> new ResourceNotFoundException("panel", id));
    newPanel.setPanelId(oldPanel.getPanelId());
    return this.panelService.saveOrUpdatePanel(newPanel);
  }

  @GetMapping("/panel/byPanelReference/{panelReference}")
  public Panel listPanelbyPanelReference(
      @PathVariable("panelReference") final String panelReference) {

    logger.debug("get panel by panelReference");
    return this.panelService.findByPanelReference(panelReference).orElseThrow(
        () -> new ResourceNotFoundException(panelReference, "panelReference not found"));
  }

}
