
package com.addixo.smartfactory.postprocessing.auth.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;

@Entity
public class Permission implements GrantedAuthority {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotBlank
  @Column(unique = true)
  private String name;

  @Override
  public String getAuthority() {

    return this.name;
  }

  public void setName(final String name) {

    this.name = name;
  }

  public String getName() {

    return this.name;
  }

  public Long getId() {

    return this.id;
  }

  public void setId(final Long id) {

    this.id = id;
  }

  @Override
  public String toString() {

    return "Operation [id=" + this.id + ", name=" + this.name + "]";
  }
}
