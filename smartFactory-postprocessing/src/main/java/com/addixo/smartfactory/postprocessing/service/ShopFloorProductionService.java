
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorProduction;
import com.addixo.smartfactory.postprocessing.enumeration.QualityType;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ShopFloorProductionService {

  List<ShopFloorProduction> findAllShopFloorProduction();

  ShopFloorProduction saveOrUpdateShopFloorProduction(ShopFloorProduction shopFloorProduction);

  void deleteShopFloorProduction(String id);

  Optional<ShopFloorProduction> findShopFloorProductionById(String shopFloorProductionId);

  List<ShopFloorProduction> list(String shopFloorProdPostReference, String shopFloorProdLineLabel,
      String shopFloorProdMachineReference, Date startDate, Date endtDate,
      String shopFloorProdOfId);

  Long getProducedQuantity(String shopFloorProdOfId, Date startDate, Date endtDate,
      QualityType type);

}
