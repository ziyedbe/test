
package com.addixo.smartfactory.postprocessing.enumeration;

public enum AutoManuallyType {

  manually,

  automatic
}
