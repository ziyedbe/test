
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class TypeConnection implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /** The type id. */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long typeConnectionId;

  /** The machine reference. */
  @Column(unique = true)
  private String typeConnectionLabel;

  private String typeConnectionDescription;

  public Long getTypeConnectionId() {

    return this.typeConnectionId;
  }

  public void setTypeConnectionId(final Long typeConnectionId) {

    this.typeConnectionId = typeConnectionId;
  }

  public String getTypeConnectionLabel() {

    return this.typeConnectionLabel;
  }

  public void setTypeConnectionLabel(final String typeConnectionLabel) {

    this.typeConnectionLabel = typeConnectionLabel;
  }

  public String getTypeConnectionDescription() {

    return this.typeConnectionDescription;
  }

  public void setTypeConnectionDescription(final String typeConnectionDescription) {

    this.typeConnectionDescription = typeConnectionDescription;
  }

  public TypeConnection() {

    super();
    // TODO Auto-generated constructor stub
  }

  @Override
  public String toString() {

    return "TypeConnection [typeConnectionId=" + this.typeConnectionId + ", typeConnectionLabel="
        + this.typeConnectionLabel + ", typeConnectionDescription=" + this.typeConnectionDescription
        + "]";
  }

}
