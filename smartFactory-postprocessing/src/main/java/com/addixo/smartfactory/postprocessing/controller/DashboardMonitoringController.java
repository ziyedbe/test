
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.domain.dto.DashboardMonitoringDto;
import com.addixo.smartfactory.postprocessing.service.DashboardMonitoringService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")

public class DashboardMonitoringController {

  private static final Logger logger = LoggerFactory.getLogger(DashboardMonitoringController.class);

  @Autowired
  private DashboardMonitoringService dashboardMonitoringService;

  @GetMapping("/dashboardMonitoring/byUserId/{DashboardMonitoringUserId}")
  public List<DashboardMonitoring> getlistAllUserId(
      @PathVariable("DashboardMonitoringUserId") final Long dashboardMonitoringUserId) {

    logger.debug("get list all dashboard by user id {} ", dashboardMonitoringUserId);

    return this.dashboardMonitoringService
        .findDashboardByDashboardMonitoringUserId(dashboardMonitoringUserId);
  }

  @GetMapping("/dashboardMonitoring/{id}")
  public DashboardMonitoring findDashboardMonitoringById(@PathVariable("id") final String id) {

    logger.debug("get findDashboardMonitoringById {}", id);
    return this.dashboardMonitoringService.findDashboardMonitoringById(id)
        .orElseThrow(() -> new ResourceNotFoundException(id, "DashboardMonitoringId not found"));
  }

  @GetMapping("/dashboardMonitoring")
  public List<DashboardMonitoring> findDashboardMonitoring() {

    logger.debug("get all {}", "DashboardMonitoring");
    return this.dashboardMonitoringService.findAllDashboardMonitoring();
  }

  @GetMapping("/dashboardMonitoring/byWorkshopLabel/{dashboardWorkShopLabel}")
  public List<DashboardMonitoring> getlistAllDashboardMonitoringWorkShop(
      @PathVariable("dashboardWorkShopLabel") final String dashboardMonitoringWorkShop) {

    logger.debug("get list all dashboard by dashboardWorkShopLabel  {} ",
        dashboardMonitoringWorkShop);

    return this.dashboardMonitoringService
        .findDashboardMonitoringByDashboardMonitoringWorkShop(dashboardMonitoringWorkShop);
  }

  @GetMapping("/dashboardMonitoring/dashboardMonitoringByLineLabel/{dashboardMonitoringLine}")
  public List<DashboardMonitoring> getDashboardMonitoringByLineLabel(
      @PathVariable("dashboardMonitoringLine") final String dashboardMonitoringLine) {

    return this.dashboardMonitoringService
        .findDashboardMonitoringByDashboardMonitoringLine(dashboardMonitoringLine);
  }

  @GetMapping("/dashboardMonitoring/dashboardMonitoringByOfId/{dashboardOfId}")
  public List<DashboardMonitoring> getDashboardMonitoringByDashboardId(
      @PathVariable("dashboardOfId") final String dashboardMonitoringOf) {

    logger.debug("get DashboardMonitoring By Id  {} ", dashboardMonitoringOf);

    return this.dashboardMonitoringService
        .findDashboardMonitoringByDashboardMonitoringOf(dashboardMonitoringOf);
  }

  @PostMapping("/dashboardMonitoring/dashboardMonitoringList")
  public DashboardMonitoring getListDashboardMonitoringByDashboardId(
      @RequestBody final DashboardMonitoringDto dashboardMonitoringDto) {

    logger.debug("get DashboardMonitoring By Id  {} ", dashboardMonitoringDto);

    return this.dashboardMonitoringService
        .getDashboardMonitoringByDashboardMonitoringList(dashboardMonitoringDto)
        .orElseThrow(() -> new ResourceNotFoundException(
            dashboardMonitoringDto.getDashboardPostReference() + ""
                + dashboardMonitoringDto.getDashboardLineLabel(),
            "getDashboardPostReference and DashboardLineLabel not found"));
  }

}