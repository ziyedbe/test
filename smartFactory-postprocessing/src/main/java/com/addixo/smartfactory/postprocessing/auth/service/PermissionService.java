
package com.addixo.smartfactory.postprocessing.auth.service;

import com.addixo.smartfactory.postprocessing.auth.domain.Permission;

import java.util.List;
import java.util.Optional;

public interface PermissionService {

  Optional<Permission> findById(Long id);

  Optional<Permission> findByName(String name);

  List<Permission> findAll();

  Permission saveOrUpdate(Permission operation);

  void delete(Long operationId);
}
