
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Color", columnList = "colorReference,colorStatus"))
public class Color implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long colorId;

  private String colorExternalId;

  @NotBlank
  private String colorReference;

  @NotBlank
  private String colorStatus;

  @NotBlank
  private String colorLabel;

  private String colorDescription;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getColorId() {

    return this.colorId;
  }

  public String getColorExternalId() {

    return this.colorExternalId;
  }

  public void setColorExternalId(final String colorExternalId) {

    this.colorExternalId = colorExternalId;
  }

  public void setColorId(final Long colorId) {

    this.colorId = colorId;
  }

  public String getColorReference() {

    return this.colorReference;
  }

  public void setColorReference(final String colorReference) {

    this.colorReference = colorReference;
  }

  public String getColorStatus() {

    return this.colorStatus;
  }

  public void setColorStatus(final String colorStatus) {

    this.colorStatus = colorStatus;
  }

  public String getColorDescription() {

    return this.colorDescription;
  }

  public void setColorDescription(final String colorDescription) {

    this.colorDescription = colorDescription;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  public String getColorLabel() {

    return this.colorLabel;
  }

  public void setColorLabel(final String colorLabel) {

    this.colorLabel = colorLabel;
  }

  @Override
  public String toString() {

    return "Color [colorId=" + this.colorId + ", colorExternalId=" + this.colorExternalId
        + ", colorReference=" + this.colorReference + ", colorStatus=" + this.colorStatus
        + ", colorLabel=" + this.colorLabel + ", colorDescription=" + this.colorDescription
        + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + "]";
  }

}
