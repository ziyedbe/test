
package com.addixo.smartfactory.postprocessing.auth.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;

@Entity
public class Role implements GrantedAuthority {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(unique = true)
  @NotNull
  private String name;

  @NotNull
  @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
  @JoinTable(joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
  private Set<Permission> permissions = new HashSet<>();

  public Long getId() {

    return this.id;
  }

  public void setId(final Long id) {

    this.id = id;
  }

  public String getName() {

    return this.name;
  }

  public void setName(final String name) {

    this.name = name;
  }

  public Set<Permission> getPermissions() {

    return this.permissions;
  }

  public void setPermissions(final Set<Permission> permissions) {

    this.permissions = permissions;
  }

  @Override
  @JsonIgnore
  public String getAuthority() {

    return this.name.toString();
  }

  @Override
  public String toString() {

    return "Role [id=" + this.id + ", name=" + this.name + ", allowedPermission=" + this.permissions
        + "]";
  }

}
