
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(name = "IDX_Chart", columnList = "of,incidentId"))
public class Chart implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String of;

  private String incidentId;

  private String description;

  private String nbreOccurrence;

  public Long getId() {

    return this.id;
  }

  public void setId(final Long id) {

    this.id = id;
  }

  public String getOf() {

    return this.of;
  }

  public void setOf(final String of) {

    this.of = of;
  }

  public String getIncidentId() {

    return this.incidentId;
  }

  public void setIncidentId(final String incidentId) {

    this.incidentId = incidentId;
  }

  public String getDescription() {

    return this.description;
  }

  public void setDescription(final String description) {

    this.description = description;
  }

  public String getNbreOccurrence() {

    return this.nbreOccurrence;
  }

  public void setNbreOccurrence(final String nbreOccurrence) {

    this.nbreOccurrence = nbreOccurrence;
  }

  @Override
  public String toString() {

    return "Chart [id=" + this.id + ", of=" + this.of + ", incidentId=" + this.incidentId
        + ", description=" + this.description + ", nbreOccurrence=" + this.nbreOccurrence + "]";
  }

}
