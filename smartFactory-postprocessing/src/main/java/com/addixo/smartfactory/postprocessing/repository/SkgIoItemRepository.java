
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.SkgIoItem;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkgIoItemRepository extends JpaRepository<SkgIoItem, Long> {

  Optional<SkgIoItem> findBySkgIoItemEntrer(String skgIoItemEntrer);

}
