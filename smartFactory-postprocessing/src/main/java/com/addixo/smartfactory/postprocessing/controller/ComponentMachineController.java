
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.ComponentMachine;
import com.addixo.smartfactory.postprocessing.service.ComponentMachineService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ComponentMachineController {

  private static final Logger logger = LoggerFactory.getLogger(ComponentMachineController.class);

  @Autowired
  private ComponentMachineService componentMachineService;

  @PostMapping("/componentMachine")
  public ComponentMachine createComponentMachine(
      @Valid @RequestBody final ComponentMachine componentMachine) {

    logger.debug("create componentMachine : {}", componentMachine);
    return this.componentMachineService.saveOrUpdateComponentMachine(componentMachine);
  }

  @GetMapping("/componentMachine")
  public List<ComponentMachine> listAllComponentMachine() {

    logger.debug("get all  {}", "componentMachine");
    return this.componentMachineService.findAllComponentMachine();
  }

  @DeleteMapping("/componentMachine/{id}")
  public String deleteComponentMachine(@PathVariable("id") final Long componentMachineId) {

    logger.debug("delete componentMachine by id {}", componentMachineId);
    final ComponentMachine componentMachine = this.componentMachineService
        .findComponentMachineById(componentMachineId)
        .orElseThrow(() -> new ResourceNotFoundException(componentMachineId.toString(),
            "componentMachineId not found"));
    this.componentMachineService.deleteComponentMachine(componentMachine.getComponentId());
    return "deleted";
  }

  @PutMapping("/componentMachine/{id}")
  public ComponentMachine updateComponentMachine(@PathVariable("id") final Long id,
      @Valid @RequestBody final ComponentMachine newComponentMachine) {

    logger.debug("update componentMachine by id {}", id);
    final ComponentMachine oldComponentMachine = this.componentMachineService
        .findComponentMachineById(id)
        .orElseThrow(() -> new ResourceNotFoundException(id, "componentMachineId not found"));
    newComponentMachine.setComponentId(oldComponentMachine.getComponentId());
    return this.componentMachineService.saveOrUpdateComponentMachine(newComponentMachine);
  }
}