
package com.addixo.smartfactory.postprocessing.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)

public class Organization implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long organizationId;

  private String organizationExternalId;

  private String organizationName;

  private String organizationDescription;

  private String organizationAdresse;

  private String organizationCity;

  private String organizationCountry;

  private String organizationPhone;

  private String organizationRegistrationNumber;

  @Lob
  private String organizationLogo;

  @Column(nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Column(nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  public Long getOrganizationId() {

    return this.organizationId;
  }

  public void setOrganizationId(final Long organizationId) {

    this.organizationId = organizationId;
  }

  public String getOrganizationExternalId() {

    return this.organizationExternalId;
  }

  public void setOrganizationExternalId(final String organizationExternalId) {

    this.organizationExternalId = organizationExternalId;
  }

  public String getOrganizationName() {

    return this.organizationName;
  }

  public void setOrganizationName(final String organizationName) {

    this.organizationName = organizationName;
  }

  public String getOrganizationDescription() {

    return this.organizationDescription;
  }

  public void setOrganizationDescription(final String organizationDescription) {

    this.organizationDescription = organizationDescription;
  }

  public String getOrganizationAdresse() {

    return this.organizationAdresse;
  }

  public void setOrganizationAdresse(final String organizationAdresse) {

    this.organizationAdresse = organizationAdresse;
  }

  public String getOrganizationCity() {

    return this.organizationCity;
  }

  public void setOrganizationCity(final String organizationCity) {

    this.organizationCity = organizationCity;
  }

  public String getOrganizationCountry() {

    return this.organizationCountry;
  }

  public void setOrganizationCountry(final String organizationCountry) {

    this.organizationCountry = organizationCountry;
  }

  public String getOrganizationPhone() {

    return this.organizationPhone;
  }

  public void setOrganizationPhone(final String organizationPhone) {

    this.organizationPhone = organizationPhone;
  }

  public String getOrganizationRegistrationNumber() {

    return this.organizationRegistrationNumber;
  }

  public void setOrganizationRegistrationNumber(final String organizationRegistrationNumber) {

    this.organizationRegistrationNumber = organizationRegistrationNumber;
  }

  public String getOrganizationLogo() {

    return this.organizationLogo;
  }

  public void setOrganizationLogo(final String organizationLogo) {

    this.organizationLogo = organizationLogo;
  }

  public Date getCreatedAt() {

    return this.createdAt;
  }

  public void setCreatedAt(final Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return this.updatedAt;
  }

  public void setUpdatedAt(final Date updatedAt) {

    this.updatedAt = updatedAt;
  }

  @Override
  public String toString() {

    return "Organization [organizationId=" + this.organizationId + ", organizationExternalId="
        + this.organizationExternalId + ", organizationName=" + this.organizationName
        + ", organizationDescription=" + this.organizationDescription + ", organizationAdresse="
        + this.organizationAdresse + ", organizationCity=" + this.organizationCity
        + ", organizationCountry=" + this.organizationCountry + ", organizationPhone="
        + this.organizationPhone + ", organizationRegistrationNumber="
        + this.organizationRegistrationNumber + ", organizationLogo=" + this.organizationLogo
        + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + "]";
  }

}
