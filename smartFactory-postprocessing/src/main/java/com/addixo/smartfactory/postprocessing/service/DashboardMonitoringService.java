
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;
import com.addixo.smartfactory.postprocessing.domain.dto.DashboardMonitoringDto;
import com.addixo.smartfactory.postprocessing.domain.dto.DashboardMonitoringOperation;

import java.util.List;
import java.util.Optional;

public interface DashboardMonitoringService {

  List<DashboardMonitoring> findAllDashboardMonitoring();

  DashboardMonitoring saveOrUpdateDashboardMonitoring(
      final DashboardMonitoring dashboardMonitoring);

  List<DashboardMonitoring> saveOrUpdateDashboardMonitoringList(
      final List<DashboardMonitoring> dashboardMonitoringList);

  boolean isDashboardMonitoringExist(DashboardMonitoring dashboardMonitoring);

  void deleteDashboardMonitoring(String id);

  Optional<DashboardMonitoring> findDashboardMonitoringById(String id);

  List<DashboardMonitoring> findDashboardByDashboardMonitoringUserId(Long dashboardUserId);

  List<DashboardMonitoring> findDashboardMonitoringByDashboardMonitoringLine(
      String dashboardMonitoringLine);

  List<DashboardMonitoring> findDashboardMonitoringByDashboardMonitoringOf(
      String dashboardMonitoringOf);

  List<DashboardMonitoring> findDashboardMonitoringByDashboardMonitoringWorkShop(
      String dashboardMonitoringWorkShop);

  List<DashboardMonitoringOperation> findDashboardMonitoringWithOperations();

  Optional<DashboardMonitoring> getDashboardMonitoringByDashboardMonitoringList(
      DashboardMonitoringDto dashboardMonitoringDto);
}
