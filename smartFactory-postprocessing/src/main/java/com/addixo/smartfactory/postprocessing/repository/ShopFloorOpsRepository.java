
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorOps;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopFloorOpsRepository extends JpaRepository<ShopFloorOps, String> {

  List<ShopFloorOps> findByShopFloorOpsPostReferenceAndShopFloorOpsLineLabelAndShopFloorOpsMachineReferenceAndShopFloorOpsEventDateBetween(
      String shopFloorOpsPostReference, String shopFloorOpsLineLabel,
      String shopFloorOpsMachineReference, Date startDate, Date endtDate);

  List<ShopFloorOps> findByShopFloorOpsOfIdAndShopFloorOpsEventDateBetween(String shopFloorOpsOfId,
      Date startDate, Date endtDate);
}
