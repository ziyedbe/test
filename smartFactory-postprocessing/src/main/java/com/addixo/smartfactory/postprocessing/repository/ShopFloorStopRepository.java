
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.ShopFloorStop;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopFloorStopRepository extends JpaRepository<ShopFloorStop, String> {

  List<ShopFloorStop> findByShopFloorStopPostReferenceAndShopFloorStopLineLabelAndShopFloorStopMachineReferenceAndShopFloorStopEventDateBetween(
      String shopFloorStopPostReference, String shopFloorStopLineLabel,
      String shopFloorStopMachineReference, Date startDate, Date endtDate);

  List<ShopFloorStop> findByShopFloorStopOfIdAndShopFloorStopEventDateBetween(
      String shopFloorStopOfId, Date startDate, Date endtDate);
}
