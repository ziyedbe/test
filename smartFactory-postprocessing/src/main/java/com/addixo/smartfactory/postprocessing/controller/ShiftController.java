
package com.addixo.smartfactory.postprocessing.controller;

import com.addixo.smartfactory.postprocessing.domain.Shift;
import com.addixo.smartfactory.postprocessing.service.ShiftService;
import com.addixo.smartfactory.postprocessing.util.ResourceNotFoundException;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ShiftController {

  private static final Logger logger = LoggerFactory.getLogger(ShiftController.class);

  @Autowired
  private ShiftService shiftService;

  @PostMapping("/shift")

  public Shift createShift(@Valid @RequestBody final Shift shift) {

    logger.debug("create shift : {}", shift);
    return this.shiftService.saveOrUpdateShift(shift);
  }

  @GetMapping("/shift")

  public List<Shift> listAllShift() {

    logger.debug("get all {}", " shift ");
    return this.shiftService.findAllShift();
  }

  @DeleteMapping("/shift/{id}")

  public String deleteShift(@PathVariable("id") final Long shiftId) {

    logger.debug("delete shift by id {}", shiftId);
    final Shift shift = this.shiftService.findShiftById(shiftId)
        .orElseThrow(() -> new ResourceNotFoundException(shiftId.toString(), "shiftId not found"));
    this.shiftService.deleteShift(shift.getShiftId());
    return "deleted";
  }

  @GetMapping("/shift/{id}")

  public Shift getShiftById(@PathVariable("id") final Long shiftId) {

    logger.debug("get shift by id {}", shiftId);
    return this.shiftService.findShiftById(shiftId)
        .orElseThrow(() -> new ResourceNotFoundException(shiftId.toString(), "not found"));
  }

  @PutMapping("/shift/{id}")

  public Shift updateShift(@PathVariable("id") final Long id,
      @Valid @RequestBody final Shift newShift) {

    logger.debug("update shift by id {}", id);
    final Shift oldShift = this.shiftService.findShiftById(id)
        .orElseThrow(() -> new ResourceNotFoundException("shift", id));
    newShift.setShiftId(oldShift.getShiftId());
    return this.shiftService.saveOrUpdateShift(newShift);
  }
}
