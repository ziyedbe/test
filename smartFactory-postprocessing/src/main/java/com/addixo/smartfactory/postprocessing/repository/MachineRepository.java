
package com.addixo.smartfactory.postprocessing.repository;

import com.addixo.smartfactory.postprocessing.domain.Machine;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MachineRepository extends JpaRepository<Machine, Long> {

  List<Machine> findByMachineLineId(Long machineLineId);

  List<Machine> findByMachinePostId(Long machinePostId);

}
