
package com.addixo.smartfactory.postprocessing.auth.config;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

public class TokenBasedAuthentication extends AbstractAuthenticationToken {

  private static final long serialVersionUID = 1L;

  private String token;

  private final UserDetails principle;

  public TokenBasedAuthentication(final UserDetails principle) {

    super(principle.getAuthorities());
    this.principle = principle;
  }

  public String getToken() {

    return this.token;
  }

  public void setToken(final String token) {

    this.token = token;
  }

  @Override
  public boolean isAuthenticated() {

    return true;
  }

  @Override
  public Object getCredentials() {

    return this.token;
  }

  @Override
  public UserDetails getPrincipal() {

    return this.principle;
  }
}
