
package com.addixo.smartfactory.postprocessing.service;

import com.addixo.smartfactory.postprocessing.domain.Organization;
import com.addixo.smartfactory.postprocessing.repository.OrganizationRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
class OrganizationServiceImpl implements OrganizationService {

  @Autowired
  private OrganizationRepository organisationRepository;

  @Override
  @Transactional(readOnly = true)
  public List<Organization> findAllOrganization() {

    return this.organisationRepository.findAll();
  }

  @Override
  public Organization saveOrUpdateOrganization(final Organization organisation) {

    return this.organisationRepository.save(organisation);
  }

  @Override
  public void deleteOrganization(final Long organisationId) {

    this.organisationRepository.delete(organisationId);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Organization> findOrganizationById(final Long id) {

    return Optional.ofNullable(this.organisationRepository.findOne(id));
  }
}
