
package com.addixo.smartfactory.postprocessing.batch.step;

import com.addixo.smartfactory.postprocessing.domain.DashboardMonitoring;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;

public class BatchSocketIoReader implements ItemReader<DashboardMonitoring> {

  private static final Logger logger = LoggerFactory.getLogger(BatchSocketIoReader.class);

  private List<DashboardMonitoring> list;

  private int index;

  public BatchSocketIoReader(List<DashboardMonitoring> list) {

    this.list = list;
    this.index = 0;
  }

  @Override
  public DashboardMonitoring read() throws Exception {

    while (this.index < this.list.size()) {
      logger.debug("start read BatchSocketIoReader index {}", this.index);
      return this.list.get(this.index++);
    }
    return null;
  }

}